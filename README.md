# README #

This is pre-alpha.
Needs to get compiled. No installation, just running by the .exe file. 

### Application summary ###

- Briefly:
     - File organizing app (library) based on assigned custom tags with the goal of keeping the logic transparent enough to keep the possibility open for reproduction of everything even if the app would no longer be available or functioning.
	 - Invasive approach is chosen: files need to be present in a app library folder ("USER_DATA") and they are renamed by the app to a random identifier name. All related data such as: tags, human-readable file name etc. are kept in database (Sqlite) file which will have the possibility to be exported to .CSV too keep thing even more transparent. [This approach chosen deliberately over a checksum based solution to allow user freely modifying file and not loosing the tracking]

### Compile info ###

- .NET Framework: 4.0.
- Depends on:
	 - Demos.DemosHelpers (Oracle related class not compiled)
	 - System.Data.Sqlite (EF6 omitted)

### Author: Demo ###

* demosweb.cz / demo@demosweb.cz