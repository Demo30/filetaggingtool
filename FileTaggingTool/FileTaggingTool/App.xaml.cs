﻿using System;
using System.Windows;
using System.Threading;
using System.IO;
using Demos.FileTaggingTool.Resources;
using System.ComponentModel;
using Demos.FileTaggingTool.LocalDatabaseMigrations.ConfigurationMigrations;
using Demos.FileTaggingTool.LocalDatabaseMigrations.LoggingDatabaseMigrations;
using Demos.Logging;
using Demos.LocalStorage;

namespace Demos.FileTaggingTool
{

    public partial class App
    {
        public static FileInfo AppExecutableFile
        {
            get
            {
                var path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                var fi = new FileInfo(path);
                if (fi.Exists)
                {
                    return fi;
                }

                throw new Exception();
            }
        }
        internal Services.LibraryService.LibraryService LibraryService { get; private set; }
        internal LoggingCascade Logging { get; private set; }
        internal LocalStorageSQLiteManager LocalStorageManager { get; private set; }

        private BackgroundWorker _updateBackgroundChecker = new BackgroundWorker();

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            FileTaggingToolStatic.Initialize(this);

            LibraryService = Services.LibraryService.LibraryService.Instance;

            LocalStorageManager = new LocalStorageSQLiteManager();

            LocalStorageManager.EstablishBasicDirectoryStructureIfNeed();
            EstablishAllConfigFiles();
            EstablishLoggingMechanism();
            FileTagger.Manager.Instance.IdentifierStrategy = FileTaggingToolStatic.IdentifierStrategy;

            DispatcherUnhandledException += ExceptionHandling;

#if !DEBUG
            ShowSplash();
#endif

            Services.UpdateChecker.TryUpdatingUpdaterDestinationPath();

            _updateBackgroundChecker.DoWork += CheckForUpdatesBackground;
            _updateBackgroundChecker.RunWorkerAsync();
        }

        private void ExceptionHandling(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
#if DEBUG
            MessageBox.Show(e.Exception.ToString());
            e.Handled = true;
#endif
#if !DEBUG
            MessageBox.Show(e.Exception.Message.ToString());
#endif

            try
            {
                Services.FileTaggingToolLoggingManager.LogMessage(MessageTypes.ERROR_INTERNAL, e.Exception.Message, e.Exception.StackTrace);
            }
            catch
            {
                MessageBox.Show("Chybu se napodařilo zanést do logu.", UserMessages.ErrorCaption);
            }
        }

        private void ShowSplash()
        {
            var staticSplash = new SplashScreen(InternalResourceMapper.ImageResource + "PhotoTagger_Splash.png");
            staticSplash.Show(true);
            Thread.Sleep(1750);
            staticSplash.Close(TimeSpan.FromMilliseconds(750));
        }

        private void CheckForUpdatesBackground(object sender, DoWorkEventArgs args)
        {
            Thread.Sleep(10000);

            try
            {
                if (Services.UpdateChecker.NewVersionAvailable())
                {
                    FileTaggingToolStatic.FileTaggingToolAppInstance.Dispatcher.Invoke(() => {
                        MessageBox.Show("K dispozici je novější verze aplikace! V menu: \"Soubor\" > \"Aktualizace aplikace\" je možné aplikaci aktualizovat.", "Nová verze aplikace k dispozici", MessageBoxButton.OK, MessageBoxImage.Information);
                    });
                }
            }
            catch
            {
                if (!Services.ApplicationConfiguration.AppConfigurationManager.GetUpdaterSilentMode())
                {
                    FileTaggingToolStatic.FileTaggingToolAppInstance.Dispatcher.Invoke(() => {
                        MessageBox.Show("Selhal pokus o ověření aktuálnost verze aplikace. Pokud potíže přetrvávají, kontaktujte vývojáře.", UserMessages.ErrorCaption);
                    });
                }
            }

        }

        private void EstablishAllConfigFiles()
        {
            LocalStorageManager.EstablishLocalDatabaseIfNeed(new ConfigurationMigrationManager(), TypeOfData.AppData, new[] { "Configuration" });
            LocalStorageManager.EstablishLocalDatabaseIfNeed(new LoggingDatabaseMigrationManager(), TypeOfData.AppData, new[] { "Logging" });
        }

        private void EstablishLoggingMechanism()
        {
            Logging = Services.FileTaggingToolLoggingManager.PrepareLoggingMechanism();
        }
    }

}
