﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.IO;
using System.Reflection;

using Demos.DemosHelpers;

namespace Demos.FileTaggingTool;

internal static class FileTaggingToolStatic
{
    public static Versioning Version
    {
        get
        {
            var versionString = Assembly.GetExecutingAssembly()
                .GetName()
                .Version
                .ToString();
            var versioning = new Versioning();
            versioning.LoadVersion(versionString);
            return versioning;
        }
    }

    public static string ApplicationName => "File Tagging Tool";

    public static string GetVersion => Version.ToString();

    public static App FileTaggingToolAppInstance { get; private set; }

    public static FileTagger.IIdentifierStrategy IdentifierStrategy => Services.ApplicationConfiguration.AppConfigurationManager.Instance.GetIdentifierStrategy();

    public static void Initialize(Application PhotoTaggerAppInstance)
    {
        var app = PhotoTaggerAppInstance as App;
        FileTaggingToolAppInstance = app ?? throw new Exception("Critical internal error. Could not initialize application. Invalid application instance provided.");
    }

}