namespace Demos.FileTaggingTool
{
    public static class UserMessages
    {
        public const string ErrorCaption = "Chyba";
        public const string WarningCaption = "Upozornění";
        
        public const string CompletedByFailure = "Operace selhala";
        public const string CompletedSuccessfully = "Operace úspěšně dokončena";
        public const string InitializingFileRecords = "[2] Záznam nových souborů do databáze";
        public const string InvalidatingFileRecords = "[4] Znevalidňování identifikátorů smazaných souborů";
        public const string ProcessingStarted = "[1] Zpracování započato. Procházení souborů";
        public const string RenamingFiles = "[3] Proces přejmenovávání souborů";

        public const string FileHasNoTagsAssigned = "K souboru nejsou přiděleny žádné štítky.";

        public const string RotateBy90Degrees = "Otočit o 90°";
        public const string RotateBy270Degrees = "Otočit o 270°";
        public const string FlipVertically = "Převrácení vertikální";
        public const string FlipHorizontally = "Převrácení horizontální";

        public const string NoLibraryOpened = "Není otevřena žádná knihovna. Není možné generovat nové identifikátory.";
        public const string FileNotFound = "Soubor nenalazen: {0}";
        public const string DirectoryNotFound = "Složka nenalazena: {0}";
        
        
        public const string TagsAssignedToFile = "Štítky souboru";
        public const string TagsAssignedToFilePrimary = "Hlavní štítky souboru";
        public const string TagsAssignedToFileSecondary = "Vedlejší štítky souboru";
        public const string FilePreview = "Náhled souboru";
        public const string FilePreviewNoFileSelected = "Není zvolený žádný soubor.";
        public const string FilePreviewNotAvailable = "Náhled souboru je nedostupný.";
        public const string RootFolder = "Kořenová složka";
        public const string SearchInFilesByText = "Filtrovat soubory pomocí textu";

        public const string TagRootLevel = "Kořenová úroveň";
        public const string AssignTag = "Přiřadit štítek";
        public const string AssignTagSecondary = "Přiřadit vedlejší štítek";
        public const string RemoveTagAssociationWithFile = "Odebrat štítek souboru";
        public const string DeleteTag = "Smazat štítek";
        public const string RenameTag = "Přejmenovat štítek";
        public const string CreateNewTag = "Vytvořit nový štítek";
        public const string CreateNestedTag = "Vytvořit vnořený štítek";
        public const string AddTagToFilter = "Přidat štítek do filtru";
        public const string RemoveTagFromFilter = "Odebrat šítek z filtru";

        public const string OpenLibrary = "Otevřít knihovnu";
        
        public const string CollapseAllSubLevels = "Sbalit všechny podúrovně";
        
        public const string PrimaryTag = "Hlavní šítek";
        public const string SecondaryTag = "Vedlejší šítek";

        public const string ErrorCaptionDupliciteTag = "Duplicitní název štítku";
        public const string ErrorDupliciteTagCreation = "Štítek se shodným názvem ({0}) již existuje. Duplicitní štítky nejsou povoleny.";

    }
}