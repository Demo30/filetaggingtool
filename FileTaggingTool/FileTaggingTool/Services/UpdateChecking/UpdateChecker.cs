﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using System.Windows;
using System.Data;
using Demos.DemosHelpers;
using Demos.DesktopClient.Updater;

namespace Demos.FileTaggingTool.Services
{
    internal static class UpdateChecker
    {
        private const string UPDATER_PATH_GUI_EXE = "Demos.DesktopClient.Updater.GUI.exe";
        private const string UPDATER_PATH_DLL = "Demos.DesktopClient.Updater.Manager.dll";
        private const string UPDATER_PATH_XML_CONFIG = "UpdaterConfiguration.xml";

        private const string CONFIG_UPDATER_TABLENAME = "updaterConfiguration";
        private const string CONFIG_UPDATER_CONFIG_VALUE = "config_value";
        private const string CONFIG_UPDATER_PATHTOUPDATERGUI = "overridePathToGUI";
        private const string CONFIG_UPDATER_PATHTOUPDATERMANAGERASSEMBLY = "overridePathToManagerAssembly";
        private const string CONFIG_UPDATER_PATHTOUPDATERXMLCONFIGDIRECTORY = "overridePathToConfigurationXMLDirectory";

        private static DataTable UpdaterConfigurationData
        {
            get
            {
                using (IDbConnection conn = Services.ApplicationConfiguration.AppConfigurationManager.ConfigurationDatabase)
                {
                    conn.Open();
                    string sql = $@"
                        Select * From {CONFIG_UPDATER_TABLENAME}
                    ";
                    DataTable data = UnifiedDatabaseHelperClass.GetResultsDataTable(sql, conn);
                    return data;
                }
            }
        }

        private static string PathToUpdaterDirectory
        {
            get
            {
                string path = UpdaterConfigurationData.Rows[0][CONFIG_UPDATER_CONFIG_VALUE].ToString();

                if (String.IsNullOrEmpty(path))
                {
                    path = String.Concat(Environment.CurrentDirectory, "\\", "Updater\\");
                }
                {
                    path = UpdateChecker.PreparePathString(path);
                }
                return path;
            }
        }

        private static string PathToUpdaterGUI
        {
            get
            {
                string path = UpdateChecker.PathToUpdaterDirectory + UpdateChecker.UPDATER_PATH_GUI_EXE;
                return path;
            }
        }

        private static string PathToManagerAssembly
        {
            get
            {
                string path = UpdateChecker.PathToUpdaterDirectory + UpdateChecker.UPDATER_PATH_DLL;
                return path;
            }
        }

        private static string PathConfigurationXMLContainingDirectory
        {
            get
            {
                string path = PathToUpdaterDirectory;
                return path;
            }
        }


        private static Demos.DesktopClient.Updater.INewerVersionChecker VersionCheckerInstance
        {
            get
            {
                Assembly updateManager = System.Reflection.Assembly.LoadFile(UpdateChecker.PathToManagerAssembly);

                Type updateManagerClass = (
                    from type in updateManager.GetExportedTypes()
                    where typeof(Demos.DesktopClient.Updater.INewerVersionChecker).IsAssignableFrom(type)
                    select type)
                    .Single();

                Demos.DesktopClient.Updater.INewerVersionChecker instance = (Demos.DesktopClient.Updater.INewerVersionChecker)Activator.CreateInstance(updateManagerClass);

                return instance;
            }
        }

        internal static bool NewVersionAvailable()
        {
            Demos.DesktopClient.Updater.INewerVersionChecker instance = UpdateChecker.VersionCheckerInstance;

            bool configurationLoaded = instance.LoadConfiguration(UpdateChecker.PathConfigurationXMLContainingDirectory);
            
            if (!configurationLoaded)
            {
                throw new Exception();
            }

            bool newerVersionAvailable = instance.IsNewerVersionAvailable(FileTaggingToolStatic.Version);

            return newerVersionAvailable;
        }

        internal static Demos.DesktopClient.Updater.IInfoData LatestVersionInfo()
        {
            Demos.DesktopClient.Updater.INewerVersionChecker instance = UpdateChecker.VersionCheckerInstance;

            bool configurationLoaded = instance.LoadConfiguration(UpdateChecker.PathConfigurationXMLContainingDirectory);

            Demos.DesktopClient.Updater.IInfoData info = null;

            info = instance.GetLatestVersionInfoData();

            return info;
        }

        internal static void StartUpdater()
        {
            if (File.Exists(UpdateChecker.PathToUpdaterGUI))
            {
                MessageBoxResult res = MessageBox.Show("Aktualizaci je možné provést pouze je-li hlavní aplikace vypnuta. Chcete ji ukončit a pokračovat v aktualizaci?", "Spuštění aktualizačního nástroje", MessageBoxButton.OKCancel);
                if (res == MessageBoxResult.OK)
                {
                    FileInfo f = new FileInfo(UpdateChecker.PathToUpdaterGUI);

                    System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo(UpdateChecker.PathToUpdaterGUI)
                    {
                        WorkingDirectory = PathToUpdaterDirectory,
                    };

                    if (Services.ApplicationConfiguration.AppConfigurationManager.GetUpdaterRunAsAdmin())
                    {
                        startInfo.Verb = "runas";
                    }

                    System.Diagnostics.Process.Start(startInfo);
                    FileTaggingToolStatic.FileTaggingToolAppInstance.Shutdown();
                }
                else
                {
                    return;
                }
            }
            else
            {
                MessageBox.Show($"Není možné aktualizovat, nebyl nalezen aktualizační nástroj na adrese: {UpdateChecker.PathToUpdaterGUI}", "Problém");
                return;
            }
        }

        internal static bool TryUpdatingUpdaterDestinationPath()
        {
            string updaterDir = UpdateChecker.PathConfigurationXMLContainingDirectory;
            FileInfo configurationFile = new FileInfo(updaterDir + "\\" + UpdateChecker.UPDATER_PATH_XML_CONFIG);

            try
            {
                if (configurationFile.Exists)
                {
                    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                    doc.Load(configurationFile.FullName);
                    System.Xml.XmlNode rootPath = doc.SelectSingleNode("/Updater/ApplicationRootPath");

                    if (rootPath != null)
                    {
                        rootPath.InnerText = App.AppExecutableFile.Directory.FullName;
                        doc.Save(configurationFile.FullName);
                        return true;
                    }

                } else
                {
                    throw new FileNotFoundException(configurationFile.FullName);
                }
            }
            catch(Exception ex)
            {
                Services.FileTaggingToolLoggingManager.LogMessage(Logging.MessageTypes.ERROR_INTERNAL, ex.Message, ex.StackTrace);
            }

            return false;
        }

        private static string PreparePathString(string path)
        {
            path = path.Replace(@"/", "\\");
            path = path.Replace(@"..\", Environment.CurrentDirectory + "\\");

            if (path[path.Length - 1] != '\\')
            {
                path += "\\";
            }

            return path;
        }
    }
}
