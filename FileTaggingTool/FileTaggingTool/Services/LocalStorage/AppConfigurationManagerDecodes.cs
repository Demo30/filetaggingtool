﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.FileTaggingTool.Services
{
    internal class AppConfigurationManagerDecodes
    {
        private static Dictionary<string, Type> IdentifierStrategyDecodes = new Dictionary<string, Type>()
        {
            { "1", typeof(FileTagger.IdentifierStrategyNo1) },
            { "2", typeof(FileTagger.IdentifierStrategyNo2) },
        };

        public static string EncodeIdentifierStrategy(FileTagger.IIdentifierStrategy strategy)
        {
            foreach(string identStrgCode in AppConfigurationManagerDecodes.IdentifierStrategyDecodes.Keys)
            {
                if (strategy.GetType() == AppConfigurationManagerDecodes.IdentifierStrategyDecodes[identStrgCode])
                {
                    return identStrgCode;
                }
            }
            throw new Exception($"Missing database enumeration code for strategy of type: \"{strategy.GetType()}\"");
        }

        public static Type DecodeIdentifierStrategyCode(string code)
        {
            if (AppConfigurationManagerDecodes.IdentifierStrategyDecodes.ContainsKey(code))
            {
                return AppConfigurationManagerDecodes.IdentifierStrategyDecodes[code];
            }
            else
            {
                throw new Exception($"Missing database enumeration decode for strategy code: \"{code}\"");
            }
        }
    }
}
