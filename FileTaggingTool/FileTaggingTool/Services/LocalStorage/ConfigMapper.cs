﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.FileTaggingTool.Services.ApplicationConfiguration
{
    internal class ConfigMapper
    {
        public enum ConfigTypes
        {
            SPEC_UPDATER_PATH_TO_UPDATER_DIR,
            SPEC_UPDATER_SILENT_ERROR,
            SPEC_UPDATER_RUN_AS_ADMIN,
            GENERAL_LAST_LIBRARY_PATH,
            GENERAL_PROCESS_FILES_AUTOMATICALLY,
        }

        public class ConfigMapperInfo
        {
            public string TableName { get; }
            public string ConfigName { get; }

            public ConfigMapperInfo(string tableName, string configName)
            {
                object[] required = { tableName, configName };
                if (required.Contains(null))
                {
                    throw new ArgumentNullException();
                }
                else
                {
                    this.TableName = tableName;
                    this.ConfigName = configName;
                }
            }

            public override bool Equals(Object obj)
            {
                if (obj is ConfigMapperInfo)
                {
                    ConfigMapperInfo comparedInfo = (ConfigMapperInfo)obj;
                    bool conds =
                        comparedInfo.TableName == this.TableName &&
                        comparedInfo.ConfigName == this.ConfigName;
                    return conds;
                }
                else
                {
                    return false;
                }
            }

            public static bool operator ==(ConfigMapperInfo obj1, ConfigMapperInfo obj2)
            {
                return obj1.Equals(obj2);
            }

            public static bool operator !=(ConfigMapperInfo obj1, ConfigMapperInfo obj2)
            {
                return !obj1.Equals(obj2);
            }
        }

        public static ConfigMapperInfo GetMapperInfo(ConfigTypes configType)
        {
            switch (configType)
            {
                case ConfigTypes.GENERAL_LAST_LIBRARY_PATH:
                    return new ConfigMapperInfo(AppConfigurationManager.CONFIG_GENERAL_TABLENAME, AppConfigurationManager.CONFIG_UPDATER_CONFIGS_GENERAL_LAST_LIBRARY_PATH);
                case ConfigTypes.GENERAL_PROCESS_FILES_AUTOMATICALLY:
                    return new ConfigMapperInfo(AppConfigurationManager.CONFIG_GENERAL_TABLENAME, AppConfigurationManager.CONFIG_UPDATER_CONFIGS_GENERAL_PROCESS_FILES_AUTOMATICALLY);
                case ConfigTypes.SPEC_UPDATER_PATH_TO_UPDATER_DIR:
                    return new ConfigMapperInfo(AppConfigurationManager.CONFIG_UPDATER_TABLENAME, AppConfigurationManager.CONFIG_UPDATER_CONFIGS_PATH_TO_UPDATER_DIR);
                case ConfigTypes.SPEC_UPDATER_RUN_AS_ADMIN:
                    return new ConfigMapperInfo(AppConfigurationManager.CONFIG_UPDATER_TABLENAME, AppConfigurationManager.CONFIG_UPDATER_CONFIGS_UPDATER_RUN_AS_ADMIN);
                case ConfigTypes.SPEC_UPDATER_SILENT_ERROR:
                    return new ConfigMapperInfo(AppConfigurationManager.CONFIG_UPDATER_TABLENAME, AppConfigurationManager.CONFIG_UPDATER_CONFIGS_UPDATER_SILENT_ERROR);
                default:
                    throw new Exception($"Missing mapping for config type {configType}");
            }
        }



    }
}
