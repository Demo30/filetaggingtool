﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Demos.DemosHelpers;
using Demos.FileTaggingTool.LocalDatabaseMigrations.ConfigurationMigrations;


namespace Demos.FileTaggingTool.Services.ApplicationConfiguration
{
    public class AppConfigurationManager
    {
        public static AppConfigurationManager Instance
        {
            get
            {
                if (AppConfigurationManager._instance == null)
                {
                    AppConfigurationManager._instance = new AppConfigurationManager();
                }
                return AppConfigurationManager._instance;
            }
        }
        private static AppConfigurationManager _instance = null;

        internal event EventHandler<AppConfigurationChangedEventArgs> ConfigurationChanged;

        public static IDbConnection ConfigurationDatabase
        {
            get
            {
                IDbConnection conn = FileTaggingToolStatic.FileTaggingToolAppInstance.LocalStorageManager.GetLocalDatabase(new ConfigurationMigrationManager(), LocalStorage.TypeOfData.AppData, new[] { "Configuration" });
                return conn;
            }
        }

        internal const string CONFIG_UPDATER_TABLENAME = "updaterConfiguration";
        internal const string CONFIG_GENERAL_TABLENAME = "generalConfiguration";
        private const string CONFIG_CONFIG_NAME = "config_name";
        private const string CONFIG_CONFIG_VALUE = "config_value";
        internal const string CONFIG_UPDATER_CONFIGS_PATH_TO_UPDATER_DIR = "PATH_TO_UPDATER_DIR";
        internal const string CONFIG_UPDATER_CONFIGS_UPDATER_SILENT_ERROR = "UPDATER_SILENT_ERROR";
        internal const string CONFIG_UPDATER_CONFIGS_UPDATER_RUN_AS_ADMIN = "UPDATER_RUN_AS_ADMIN";
        internal const string CONFIG_UPDATER_CONFIGS_GENERAL_LAST_LIBRARY_PATH = "GENERAL_LAST_LIBRARY_PATH";
        internal const string CONFIG_UPDATER_CONFIGS_GENERAL_PROCESS_FILES_AUTOMATICALLY = "GENERAL_PROCESS_FILES_AUTOMATICALLY";
        internal const string CONFIG_GENERAL_CONFIGS_GENERAL_IDENTIFIER_STRATEGY = "GENERAL_IDENTIFIER_STRATEGY";

        private AppConfigurationManager()
        {

        }

        internal static string GetRawConfigValue(ConfigMapper.ConfigTypes configType)
        {
            return GetConfigValue(ConfigMapper.GetMapperInfo(configType));
        }

        public static string GetLastOpenedLibrary()
        {
            string value = GetRawConfigValue(ConfigMapper.ConfigTypes.GENERAL_LAST_LIBRARY_PATH);
            return value;
        }

        public static bool GetUpdaterSilentMode()
        {
            string value = GetRawConfigValue(ConfigMapper.ConfigTypes.SPEC_UPDATER_SILENT_ERROR);
            bool silentMode = value == "1" ? true : false;
            return silentMode;
        }

        public static bool GetUpdaterRunAsAdmin()
        {
            string value = GetRawConfigValue(ConfigMapper.ConfigTypes.SPEC_UPDATER_RUN_AS_ADMIN);
            bool runAsAdmin = value == "1" ? true : false;
            return runAsAdmin;
        }

        public static string GetPathToUpdaterDirectory()
        {
            string value = GetRawConfigValue(ConfigMapper.ConfigTypes.SPEC_UPDATER_PATH_TO_UPDATER_DIR);
            return value;
        }

        public static bool GetProcessFilesAutomatically()
        {
            string value = GetRawConfigValue(ConfigMapper.ConfigTypes.GENERAL_PROCESS_FILES_AUTOMATICALLY);
            bool processAutomatically = value == "1" ? true : false;
            return processAutomatically;
        }

        public bool SetProcessFilesAutomatically(bool processFilesAutomatically)
        {
            string value = processFilesAutomatically ? "1" : "0";
            bool result = SetConfigValue(ConfigMapper.GetMapperInfo(ConfigMapper.ConfigTypes.GENERAL_PROCESS_FILES_AUTOMATICALLY), value);
            return result;
        }

        public bool SetLastLoadedLibraryPath(string pathToLibrary)
        {
            bool result = SetConfigValue(ConfigMapper.GetMapperInfo(ConfigMapper.ConfigTypes.GENERAL_LAST_LIBRARY_PATH), pathToLibrary);
            return result;
        }

        private static string GetConfigValue(ConfigMapper.ConfigMapperInfo mapperInfo)
        {
            return GetConfigValue(mapperInfo.ConfigName, mapperInfo.TableName);
        }

        private static string GetConfigValue(string configName, string configTable)
        {
            using (IDbConnection conn = AppConfigurationManager.ConfigurationDatabase)
            {
                conn.Open();
                string sql = $@"
                    Select {CONFIG_CONFIG_VALUE} value
                    From {configTable}
                    Where {CONFIG_CONFIG_NAME} = '{configName}'
                ";
                DataTable data = UnifiedDatabaseHelperClass.GetResultsDataTable(sql, conn);
                return data.Rows[0]["value"].ToString();
            }
        }

        private bool SetConfigValue(ConfigMapper.ConfigMapperInfo mapperInfo, string configValue)
        {
            return this.SetConfigValue(mapperInfo.ConfigName, mapperInfo.TableName, configValue);
        }

        private bool SetConfigValue(string configName, string configTable, string configValue)
        {
            string oldValue = GetConfigValue(configName, configTable);

            using (IDbConnection conn = AppConfigurationManager.ConfigurationDatabase)
            {
                conn.Open();
                string sql = $@"
                    Update {configTable}
                    Set {CONFIG_CONFIG_VALUE} = '{configValue}'
                    Where {CONFIG_CONFIG_NAME} = '{configName}'
                ";
                UnifiedDatabaseHelperClass.ExecuteNonQuery(sql, conn);
            }

            if (ConfigurationChanged != null)
            {
                AppConfigurationChangedEventArgs args = new AppConfigurationChangedEventArgs
                (
                    new ConfigMapper.ConfigMapperInfo(configTable, configName), oldValue, configValue
                );
                ConfigurationChanged(nameof(AppConfigurationManager), args);
            }

            return true;
        }

        public FileTagger.IIdentifierStrategy GetIdentifierStrategy()
        {
            string value = GetConfigValue(CONFIG_GENERAL_CONFIGS_GENERAL_IDENTIFIER_STRATEGY, CONFIG_GENERAL_TABLENAME);
            Type identifierType = AppConfigurationManagerDecodes.DecodeIdentifierStrategyCode(value);
            FileTagger.IIdentifierStrategy strategy = (FileTagger.IIdentifierStrategy)System.Activator.CreateInstance(identifierType);
            return strategy;
        }

        public bool SetIdentifierStrategy(FileTagger.IIdentifierStrategy strategy)
        {
            string identifierStrategyCode = AppConfigurationManagerDecodes.EncodeIdentifierStrategy(strategy);
            bool result = this.SetConfigValue(CONFIG_GENERAL_CONFIGS_GENERAL_IDENTIFIER_STRATEGY, CONFIG_GENERAL_TABLENAME, identifierStrategyCode);
            return result;
        }


    }
}
