﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.FileTaggingTool.Services.ApplicationConfiguration
{
    internal class AppConfigurationChangedEventArgs : EventArgs
    {
        public ConfigMapper.ConfigMapperInfo ConfigInfo { get; }
        public string OldValue { get; }
        public string NewValue { get; }

        public AppConfigurationChangedEventArgs(ConfigMapper.ConfigMapperInfo configInfo, string oldValue, string newValue)
        {
            if (
                configInfo == null ||
                String.IsNullOrEmpty(oldValue) ||
                String.IsNullOrEmpty(newValue))
            {
                throw new ArgumentNullException();
            }

            this.ConfigInfo = configInfo;
            this.OldValue = oldValue;
            this.NewValue = newValue;
        }

    }
}
