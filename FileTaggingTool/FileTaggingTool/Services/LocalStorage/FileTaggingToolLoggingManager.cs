﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Demos.FileTaggingTool;
using Demos.FileTaggingTool.LocalDatabaseMigrations;
using Demos.FileTaggingTool.LocalDatabaseMigrations.LoggingDatabaseMigrations;
using Demos.Logging;

namespace Demos.FileTaggingTool.Services
{
    public class FileTaggingToolLoggingManager
    {
        internal static LoggingCascade PrepareLoggingMechanism()
        {
            string loggingDirName = "Logging";

            string loggingXMLFilePath = FileTaggingToolStatic.FileTaggingToolAppInstance.LocalStorageManager.GetPathToFile(Demos.LocalStorage.TypeOfData.AppData, "LoggingXML.xml", new string[] { loggingDirName }, false, true);
            LoggingStrategyFile fileStrategy = new LoggingStrategyFile(loggingXMLFilePath, FileTaggingToolStatic.ApplicationName);

            System.Data.IDbConnection loggingDatabase = FileTaggingToolStatic.FileTaggingToolAppInstance.LocalStorageManager.GetLocalDatabase(new LoggingDatabaseMigrationManager(), LocalStorage.TypeOfData.AppData, new string[] { "Logging" });
            loggingDatabase.Open();
            LoggingStrategyDatabase databaseStrategy = new LoggingStrategyDatabase(loggingDatabase, FileTaggingToolStatic.ApplicationName);
            databaseStrategy.LoggingTableName = "log";

            LoggingStrategy[] strategies = new LoggingStrategy[]
            {
                databaseStrategy,
                fileStrategy
            };

            LoggingCascade cascade = new LoggingCascade(strategies);

            return cascade;
        }

        internal static void LogMessage(Demos.Logging.MessageTypes type, string message, string stackTrace = null)
        {
            FileTaggingToolStatic.FileTaggingToolAppInstance.Logging.LogMessage(type, message, stackTrace, FileTaggingToolStatic.GetVersion.ToString());
        }

        internal static void LogCustomMessage(string customType, string message)
        {
            FileTaggingToolStatic.FileTaggingToolAppInstance.Logging.LogCustomMessage(customType, message, FileTaggingToolStatic.GetVersion.ToString());
        }
    }
}
