﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.ComponentModel;
using System.Threading;
using Demos.DemosHelpers;
using Demos.FileTagger;
using Demos.FileTaggingTool.Services.ApplicationConfiguration;
using ORM = Demos.FileTagger.ORM;

namespace Demos.FileTaggingTool.Services.LibraryService
{
    internal class LibraryService
    {
        public static LibraryService Instance
        {
            get
            {
                if (LibraryService._instance == null)
                {
                    LibraryService._instance = new LibraryService();
                }

                return LibraryService._instance;
            }
            private set
            {
                LibraryService._instance = value;
            }
        }
        private static LibraryService _instance = null;

        public Manager LibraryManager { get { return Manager.Instance; } }
        public AppConfigurationManager ApplicationConfigurationManager { get { return AppConfigurationManager.Instance; } }
        public bool FilesProcessedAutomatically
        {
            get
            {
                return AppConfigurationManager.GetProcessFilesAutomatically();
            }
            set
            {
                AppConfigurationManager.Instance.SetProcessFilesAutomatically(value);
            }
        }
        public event EventHandler<FileSystemStructureChangeArgs> FileSystemStructureChange;

        private LibraryFileWatcher FileSystemWatcher { get; set; }
        private Timer ProcessingTimer { get; set; } = null;
        private Queue<DirectoryInfo> DirectoriesToProcess { get; set; } = new Queue<DirectoryInfo>();

        private LibraryService()
        {
            this.BindToEvents();
        }

        private void BindToEvents()
        {
            this.LibraryManager.LibraryOpened += this.OnLibraryOpened;
            this.ApplicationConfigurationManager.ConfigurationChanged += this.OnApplicationConfigurationChanged;            
        }

        private void UpdateTimer()
        {
            int dueTime = 1000;
            if (this.ProcessingTimer == null)
            {
                TimerCallback timerProcess = (state) =>
                {
                    this.ProcessEnqueuedDirectory();
                };
                Timer timer = new Timer(timerProcess, null, dueTime, Timeout.Infinite);
                this.ProcessingTimer = timer;
            }
            this.ProcessingTimer.Change(dueTime, Timeout.Infinite);
        }

        private void OnLibraryOpened(object sender, EventArgs e)
        {
            if (FilesProcessedAutomatically)
            {
                this.StartAutoProcessingFiles();
            }
        }

        private void OnApplicationConfigurationChanged(object sender, AppConfigurationChangedEventArgs args)
        {
            if (args.ConfigInfo == ConfigMapper.GetMapperInfo(ConfigMapper.ConfigTypes.GENERAL_PROCESS_FILES_AUTOMATICALLY))
            {
                if (this.FilesProcessedAutomatically)
                {
                    this.StartAutoProcessingFiles();
                }
                else
                {
                    this.StopAutoprocessingFiles();
                }
            }
        }

        private void ProcessEnqueuedDirectory()
        {
            DirectoryInfo[] dirs = this.DirectoriesToProcess.ToArray();
            this.DirectoriesToProcess.Clear();
            this.ProcessDirectories(dirs);
        }

        public FileInfo GetFileInfo(ORM.File file)
        {
            string fullPath = file.GetFullFinalFilepath(this.LibraryManager.OpenedLibraryUserDataDirPath);
            FileInfo fi = new FileInfo(fullPath);
            return fi;
        }

        public void RenameFile(ORM.File file, string newName)
        {
            this.LibraryManager.SetUserFilename(file.Id, newName);
            if (!this.FilesProcessedAutomatically) // when auto-proc is enabled, event gets raised from the listener
            {
                this.RaiseDirectoryFileSystemEvent(new System.IO.DirectoryInfo[] { this.GetFileInfo(file).Directory });
            }
        }

        public void CreateDirectory(string dirPath)
        {
            Directory.CreateDirectory(dirPath);
            if (!this.FilesProcessedAutomatically)
            {
                this.RaiseDirectoryFileSystemEvent(new DirectoryInfo[] { new DirectoryInfo(dirPath) });
            }
        }

        public void DeleteDir(DirectoryInfo dir)
        {
            Directory.Delete(dir.FullName, true);
            if (!this.FilesProcessedAutomatically)
            {
                if (dir.Parent != null)
                {
                    this.RaiseDirectoryFileSystemEvent(new DirectoryInfo[] { dir.Parent });
                }
            }
        }

        public void DeleteFiles(ORM.File[] filesToDelete)
        {
            List<string> dirsToProcess = new List<string>();

            foreach (FileTagger.ORM.File curFile in filesToDelete)
            {
                string filePath = curFile.GetFullFinalFilepath(this.LibraryManager.OpenedLibraryUserDataDirPath);
                if (File.Exists(filePath))
                {
                    FileInfo fi = new FileInfo(filePath);
                    File.Delete(fi.FullName);
                    if (!dirsToProcess.Contains(fi.Directory.FullName))
                    {
                        dirsToProcess.Add(fi.Directory.FullName);
                    }
                } 
                else
                {
                    Services.FileTaggingToolLoggingManager.LogMessage(Logging.MessageTypes.NOTICE, $"File ({filePath}) not found while trying to delete it.");
                    continue;
                }
            }

            List<DirectoryInfo> dirInfosToProcess = new List<DirectoryInfo>();
            foreach (string curDirToProcess in dirsToProcess)
            {
                dirInfosToProcess.Add(new DirectoryInfo(curDirToProcess));
            }

            this.RaiseDirectoryFileSystemEvent(dirInfosToProcess.ToArray());
        }

        public void MoveContentToDirectory(DirectoryInfo destinationDirectory, string[] fullPaths, bool copy)
        {
            List<string> dirPathsToProcess = new List<string>();

            foreach (string path in fullPaths)
            {
                if (File.Exists(path))
                {
                    FileInfo fi = new FileInfo(path);
                    string newFilePath = $"{destinationDirectory.FullName}\\{fi.Name}";
                    if (copy)
                    {
                        File.Copy(fi.FullName, newFilePath);
                    }
                    else
                    {
                        File.Move(fi.FullName, newFilePath);
                    }

                    FileInfo newFileInfo = new FileInfo(newFilePath);
                    if (!dirPathsToProcess.Contains(newFileInfo.Directory.FullName))
                    {
                        dirPathsToProcess.Add(newFileInfo.Directory.FullName);
                    }
                }
                else if (Directory.Exists(path))
                {
                    DirectoryInfo di = new DirectoryInfo(path);
                    string newDirPath = $"{destinationDirectory.FullName}\\{di.Name}";
                    DirectoryInfo diTarget = new DirectoryInfo(newDirPath);
                    if (copy)
                    {
                        DemosHelpers.GeneralHelperClass.CopyDirectoriesWithContent(di, diTarget);
                    }
                    else
                    {
                        Directory.Move(di.FullName, diTarget.FullName);
                    }

                    if (!dirPathsToProcess.Contains(di.FullName))
                    {
                        dirPathsToProcess.Add(di.FullName);
                    }
                }
                else
                {
                    throw new Exception("File transfer failed. File does not exist on specified path.");
                }
            }

            if (dirPathsToProcess.Count > 0)
            {
                List<DirectoryInfo> dirs = new List<DirectoryInfo>();
                foreach (string dirPath in dirPathsToProcess)
                {
                    dirs.Add(new DirectoryInfo(dirPath));
                }
                this.RaiseDirectoryFileSystemEvent(dirs.ToArray());
            }
        }

        public void ProcessAllFilesInLibrary()
        {
            this.LibraryManager.ProcessFilesInOpenedLibrary();
        }

        public void ProcessDirectories(DirectoryInfo[] dirs)
        {
            DirectoryInfo parent = this.FindCommonDirectoryAncestor(dirs);
            if (parent == null || !Manager.IsPathWithinOpenedLibPath(parent.FullName))
            {
                parent = new DirectoryInfo(Manager.Instance.OpenedLibraryUserDataDirPath);
            }
            FileEvaluationSettings settings = new FileEvaluationSettings();
            settings.FileSelector = new FileDirectorySelector(parent);
            this.LibraryManager.ProcessFiles(settings);
        }

        private void RaiseDirectoryFileSystemEvent(DirectoryInfo[] directories)
        {
            if (this.FileSystemStructureChange != null)
            {
                FileSystemStructureChangeArgs args = new FileSystemStructureChangeArgs(directories);
                this.FileSystemStructureChange(this, args);
            }
        }

        private void StartAutoProcessingFiles()
        {
            if (this.FileSystemWatcher != null)
            {
                this.StopAutoprocessingFiles();
            }
            this.FileSystemWatcher = new LibraryFileWatcher();
            this.FileSystemWatcher.ProcessingRequestSent += (obj, args) =>
            {
                if (args.ChangesInDirectories.Length > 0)
                {
                    foreach (DirectoryInfo dir in args.ChangesInDirectories)
                    {
                        if (!this.IsDirectoryEnqueuedForProcessing(dir))
                        {
                            this.DirectoriesToProcess.Enqueue(dir);
                        }
                    }
                    this.UpdateTimer();
                }
            };
            //this.ProcessAllFilesInLibrary();
            this.FileSystemWatcher.StartWatching();
        }

        private void StopAutoprocessingFiles()
        {
            if (this.FileSystemWatcher != null)
            {
                this.FileSystemWatcher.EnableRaisingEvents = false;
                this.FileSystemWatcher.Dispose();
                this.FileSystemWatcher = null;
            }
        }

        #region Supportive methods

        private bool IsDirectoryEnqueuedForProcessing(DirectoryInfo dir)
        {
            foreach(DirectoryInfo currentDir in this.DirectoriesToProcess)
            {
                if (currentDir != null && currentDir.FullName == dir.FullName)
                {
                    return true;
                }
            }
            return false;
        }

        private DirectoryInfo FindCommonDirectoryAncestor(DirectoryInfo[] dirs)
        {
            if (dirs.Length == 1) { return dirs[0]; }
            else if (dirs.Length == 2)
            {
                List<string> firstPathTree = new List<string>();
                DirectoryInfo first = dirs[0];
                while (first.Parent != null)
                {
                    firstPathTree.Add(first.FullName.TrimEnd(new char[] { '\\' }));
                    first = first.Parent;
                }
                DirectoryInfo second = dirs[1];
                while (second.Parent != null)
                {
                    if (firstPathTree.Contains(second.FullName.TrimEnd(new char[] { '\\' })))
                    {
                        return second;
                    }
                    else
                    {
                        second = second.Parent;
                    }
                }
                return null;
            }
            else if (dirs.Length > 2)
            {
                int mid = (int)(dirs.Length / 2);
                List<DirectoryInfo> left = new List<DirectoryInfo>();
                List<DirectoryInfo> right = new List<DirectoryInfo>();
                for (int i = 0; i < dirs.Length; i++)
                {
                    if (i < mid)
                    {
                        left.Add(dirs[i]);
                    }
                    else
                    {
                        right.Add(dirs[i]);
                    }
                }

                DirectoryInfo leftCommonAncestor = this.FindCommonDirectoryAncestor(left.ToArray());
                DirectoryInfo rightCommonAncestor = this.FindCommonDirectoryAncestor(right.ToArray());

                if (leftCommonAncestor == null || rightCommonAncestor == null)
                {
                    return null;
                }
                else
                {
                    return this.FindCommonDirectoryAncestor(new DirectoryInfo[] { leftCommonAncestor, rightCommonAncestor });
                }
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}
