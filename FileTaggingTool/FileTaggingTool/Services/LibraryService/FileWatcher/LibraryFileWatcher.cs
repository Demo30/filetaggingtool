﻿using System;
using System.IO;
using ORM = Demos.FileTagger.ORM;
using Demos.FileTagger;

namespace Demos.FileTaggingTool.Services.LibraryService
{
    internal class LibraryFileWatcher : FileSystemWatcher
    {
        public Manager LibraryManager { get { return Manager.Instance; } }

        internal event EventHandler<FileSystemStructureChangeArgs> ProcessingRequestSent;

        public LibraryFileWatcher()
        {

        }

        public void StopWatching()
        {
            this.EnableRaisingEvents = false;
        }

        public void StartWatching()
        {
            this.SetupWatcher();
            this.EnableRaisingEvents = true;
        }

        public void ResetWatching()
        {
            this.SetupWatcher();
            this.StartWatching();
        }

        protected void SetupWatcher()
        {
            this.IncludeSubdirectories = true;
            this.Path = this.LibraryManager.OpenedLibraryUserDataDirPath;

            this.Renamed += this.OnFileSystemChange;
            this.Changed += this.OnFileSystemChange;
            this.Created += this.OnFileSystemChange;
            this.Deleted += this.OnFileSystemChange;
            this.Error += this.OnFileSystemError;
        }

        protected void OnFileSystemChange(object sender, FileSystemEventArgs args)
        {
            this.CommonUpdateAction(args);
        }

        protected void CommonUpdateAction(FileSystemEventArgs args)
        {
            Action<FileSystemEventArgs> tryCorrectingRename = (arguments) =>
            {
                try
                {
                    if (arguments is RenamedEventArgs)
                    {
                        RenamedEventArgs renamedArgs = (RenamedEventArgs)arguments;

                        FileInfo oldFileName = new FileInfo(renamedArgs.OldFullPath);
                        FileInfo newFileName = new FileInfo(renamedArgs.FullPath);

                        string oldPathSupposedIdentifier = Manager.Instance.IdentifierStrategy.ExtractIdentifierFromFileInfo(new FileInfo(oldFileName.Name), false, false);
                        string newPathSupposedIdentifier = Manager.Instance.IdentifierStrategy.ExtractIdentifierFromFileInfo(new FileInfo(newFileName.Name), false, false);
                        ORM.File file = LibraryManager.GetFileByIdentifier(oldPathSupposedIdentifier, false);
                        if (file != null && oldPathSupposedIdentifier != newPathSupposedIdentifier)
                        {
                            if (newFileName.Exists)
                            {
                                file.UserFilename = Manager.Instance.ExtractFileName(newFileName, false, false);
                                string correctedName = file.GetFullFinalFilepath(Manager.Instance.OpenedLibraryUserDataDirPath);
                                File.Move(newFileName.FullName, correctedName);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Services.FileTaggingToolLoggingManager.LogMessage(Logging.MessageTypes.WARNING, ex.Message, ex.StackTrace.ToString());
                }
            };

            tryCorrectingRename(args);

            DirectoryInfo directoryToProcess = null;
            if (Directory.Exists(args.FullPath))
            {
                directoryToProcess = new DirectoryInfo(args.FullPath);
            } else
            {
                FileInfo fi = new FileInfo(args.FullPath);
                if (fi.Directory.Exists)
                {
                    directoryToProcess = fi.Directory;
                }
            }

            if (directoryToProcess != null)
            {
                if (this.ProcessingRequestSent != null)
                {
                    FileSystemStructureChangeArgs processingArgs = new FileSystemStructureChangeArgs(new DirectoryInfo[] { directoryToProcess });
                    processingArgs.ChangeType = args.ChangeType;
                    this.ProcessingRequestSent(this, processingArgs);
                }
            }
        }

        protected void OnFileSystemError(object sender, ErrorEventArgs args)
        {
            Exception ex = args.GetException();
            Services.FileTaggingToolLoggingManager.LogMessage(Logging.MessageTypes.ERROR_INTERNAL, ex.Message, ex.StackTrace);

            this.ResetWatching();
        }

    }
}