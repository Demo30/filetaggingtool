﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Demos.FileTaggingTool.Services.LibraryService
{
    internal class FileSystemStructureChangeArgs : EventArgs
    {
        public WatcherChangeTypes ChangeType { get; set; } = WatcherChangeTypes.All;
        public DirectoryInfo[] ChangesInDirectories { get; }

        public FileSystemStructureChangeArgs(System.IO.DirectoryInfo[] affectedDirectories)
        {
            this.ChangesInDirectories = affectedDirectories;
        }
    }
}
