﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using Demos.DemosHelpers;

namespace Demos.FileTaggingTool.UserInterface
{
    class AppGenericWindow : Window
    {
        public AppGenericWindow()
        {
            base.Icon = GeneralHelperClass.GetImageSourceFromString("/PhotoTagger;component/Resources/Images/demoicon_apps.ico");
            base.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#EBEFF0"));
            base.Width = 800;
            base.Height = 600;
        }
    }
}
