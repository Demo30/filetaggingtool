﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows;
using System.Reflection;
using Demos.FileTagger.ORM;
using Demos.FileTaggingTool;

namespace Demos.FileTaggingTool.UserInterface
{
    internal static class GUICommons
    {
        public static SolidColorBrush CommonBrushes_Blue { get; } = new SolidColorBrush(new Color() { R = 0x2F, G = 0xB9, B = 0xE8, A = 0xff });
        public static SolidColorBrush CommonBrushes_DarkBlue { get; } = new SolidColorBrush(new Color() { R = 0x04, G = 0x1f, B = 0x28, A = 0xff });
        public static SolidColorBrush CommonBrushes_Gray { get; } = new SolidColorBrush(new Color() { R = 0xEB, G = 0xEF, B = 0xF0, A = 0xff });
        public static SolidColorBrush CommonBrushes_Red { get; } = new SolidColorBrush(new Color() { R = 0xc9, G = 0x17, B = 0x00, A = 0xff });
        public static SolidColorBrush CommonBrushes_Green { get; } = new SolidColorBrush(new Color() { R = 0xb9, G = 0xf0, B = 0x38, A = 0xff });

        public static LinearGradientBrush CommonBrushes_Button_Active { get; } = new()
        {
            StartPoint = new Point(0, 0),
            EndPoint = new Point(0, 1),
            GradientStops = new GradientStopCollection()
            {
                new((Color)ColorConverter.ConvertFromString("#FFF3F3F3"), 0.0),
                new((Color)ColorConverter.ConvertFromString("#FFEBEBEB"), 0.5),
                new((Color)ColorConverter.ConvertFromString("#FFDDDDDD"), 0.5),
                new((Color)ColorConverter.ConvertFromString("#FFCDCDCD"), 1)
            }
        };

        public static LinearGradientBrush CommonBrushes_Button_TogglePressed { get; } = new()
        {
            StartPoint = new Point(0, 0),
            EndPoint = new Point(0, 1),
            GradientStops = new GradientStopCollection()
            {
                new((Color)ColorConverter.ConvertFromString("#FFdce4f2"), 0.4),
                new((Color)ColorConverter.ConvertFromString("#FFdce4f2"), 0.55),
                new((Color)ColorConverter.ConvertFromString("#bf2FB9E8"), 1)
            }
        };

        public static Thickness MainWindow_BorderThicknessr_smallWindow { get; } = new(3);
        public static Thickness MainWindow_BorderThicknessr_fullscreen { get; } = new(15, 20, 15, 0);

        private static SolidColorBrush editableBG_disabled { get; } = new(Colors.LightGray);
        private static SolidColorBrush editableBG_enabled { get; } = new(Colors.White);


        public static void ToggleActive(object WardenGUIObject, bool isEnabled)
        {
            if (WardenGUIObject == null)
                return;

            Type GUIobjectType = WardenGUIObject.GetType();

            if (GUIobjectType == typeof(TextBox))
            {
                switch (isEnabled)
                {
                    case true:
                        {
                            TextBox textbox = (TextBox)WardenGUIObject;
                            textbox.Background = editableBG_enabled;
                            textbox.IsReadOnly = false;
                            break;
                        }
                    case false:
                        {
                            TextBox textbox = (TextBox)WardenGUIObject;
                            textbox.Background = editableBG_disabled;
                            textbox.IsReadOnly = true;
                            textbox.IsReadOnlyCaretVisible = false;
                            break;
                        }
                }
            }
            else if (GUIobjectType == typeof(ToggleButton))
            {
                switch (isEnabled)
                {
                    case true:
                        {
                            ToggleButton button = (ToggleButton)WardenGUIObject;
                            button.IsEnabled = true;
                            break;
                        }
                    case false:
                        {
                            ToggleButton button = (ToggleButton)WardenGUIObject;
                            button.IsEnabled = false;
                            break;
                        }
                }
            }
            else if (GUIobjectType == typeof(ComboBox))
            {
                switch (isEnabled)
                {
                    case true:
                        {
                            ComboBox combobox = (ComboBox)WardenGUIObject;
                            combobox.IsEditable = false;
                            combobox.IsHitTestVisible = true;
                            combobox.Focusable = true;
                            combobox.Background = editableBG_enabled;
                            break;
                        }
                    case false:
                        {
                            ComboBox combobox = (ComboBox)WardenGUIObject;
                            combobox.IsEditable = false;
                            combobox.IsHitTestVisible = false;
                            combobox.Focusable = false;
                            combobox.Background = editableBG_disabled;
                            break;
                        }
                }
            }
            else if (GUIobjectType == typeof(ListBox))
            {
                switch (isEnabled)
                {
                    case true:
                        {
                            ListBox combobox = (ListBox)WardenGUIObject;
                            combobox.IsHitTestVisible = true;
                            combobox.Focusable = true;
                            combobox.Background = editableBG_enabled;
                            break;
                        }
                    case false:
                        {
                            ListBox combobox = (ListBox)WardenGUIObject;
                            combobox.IsHitTestVisible = false;
                            combobox.Focusable = false;
                            combobox.Background = editableBG_disabled;
                            break;
                        }
                }
            }
            else if (WardenGUIObject is Control)
            {
                switch (isEnabled)
                {
                    case true:
                        {
                            Control control = (Control)WardenGUIObject;
                            control.IsEnabled = true;
                            break;
                        }
                    case false:
                        {
                            Control control = (Control)WardenGUIObject;
                            control.IsEnabled = false;
                            break;
                        }
                }
            }
            else
                throw new NotImplementedException("There is no implementation for supplied object type.");
        }

        public static void ToggleEditingBatch(object[] objects2Enable, object[] objects2Disable)
        {
            if (objects2Enable != null)
            {
                foreach (object currentGUIObject in objects2Enable)
                {
                    ToggleActive(currentGUIObject, true);
                }
            }

            if (objects2Disable != null)
            {
                foreach (object currentGUIObject in objects2Disable)
                {
                    ToggleActive(currentGUIObject, false);
                }
            }
        }

        public static void ToggleIsPressedButton(Button button, bool isPressed)
        {
            switch (isPressed)
            {
                case true:
                    button.Background = CommonBrushes_Button_TogglePressed;
                    button.IsHitTestVisible = false;
                    break;
                case false:
                    button.Background = CommonBrushes_Button_Active;
                    button.IsHitTestVisible = true;
                    break;
            }
        }

        public static void ToggleIsPressedButtonBatch(Button[] buttonsActive, Button[] buttonsPressed)
        {
            if (buttonsActive != null)
            {
                foreach (Button currentButton in buttonsActive)
                {
                    ToggleIsPressedButton(currentButton, false);
                }
            }

            if (buttonsPressed != null)
            {
                foreach (Button currentButton in buttonsPressed)
                {
                    ToggleIsPressedButton(currentButton, true);
                }
            }
        }

        public static object TryGetAppResourceByKey(string ResourceKey) => FileTaggingToolStatic.FileTaggingToolAppInstance.TryFindResource(ResourceKey);

        public static string LocalizeAssignmentType(TagAssignmentType tagAssignmentType)
        {
            return tagAssignmentType switch
            {
                TagAssignmentType.PrimaryTag => UserMessages.PrimaryTag,
                TagAssignmentType.SecondaryTag => UserMessages.SecondaryTag,
                _ => throw new InvalidOperationException()
            };
        }
    }
}
