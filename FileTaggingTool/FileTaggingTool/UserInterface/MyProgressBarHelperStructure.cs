﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace Demos.FileTaggingTool.UserInterface
{
    static class MyProgressBarHelperStructure
    {
        public static Rectangle actualBar { get; internal set; }
        public static Label comment { get; internal set; }

        //public static void SetWidthByPercentage(int percentage)
        //{
        //    int maxWidth = (int)(AppWindowHandler.GetAppWindowByName("MainWindow")).Width;
        //    MyProgressBarHelperStructure.actualBar.Width = (maxWidth / 100) * percentage;
        //}

        public static void SetText(string text)
        {
            MyProgressBarHelperStructure.comment.Content = text;
        }

        //public static void SetTextAndPercentage(int percentage, string text)
        //{
        //    MyProgressBarHelperStructure.comment.Content = text;
        //    int maxWidth = (int)(AppWindowHandler.GetAppWindowByName("MainWindow")).Width;
        //    MyProgressBarHelperStructure.actualBar.Width = (maxWidth / 100) * percentage;
        //}

        public static void ResetBar()
        {
            MyProgressBarHelperStructure.comment.Content = null;
            MyProgressBarHelperStructure.actualBar.Width = 0;
        }
    }
}
