namespace Demos.FileTaggingTool.UserInterface;

public class Constants
{
    public const int NumberOfFilesLimitForConfirmation = 15;
}