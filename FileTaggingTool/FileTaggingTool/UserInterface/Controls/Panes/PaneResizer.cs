﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Demos.FileTaggingTool.UserInterface.Controls.Panes
{
    // The logic of this control should be general, but is currently very dependent on the particular circumstances:
    // - Main window is the frame check
    // - It only resizes ColumnDefinition
    // - safety margin is pretty much random

    public class PaneResizer : GridSplitter
    {
        private SolidColorBrush defaultColor = GUICommons.CommonBrushes_Blue;

        public ColumnDefinition ResizableColumn { get; set; } = null;
        public RowDefinition ResizableRow { get; set; } = null;

        private FrameworkElement VisibilityFrameCheck { get; set; } = FileTaggingToolStatic.FileTaggingToolAppInstance.MainWindow;

        private double _safetyMargin = 50;
        public int SafetyMarginMultiplication { get; set; } = 4;


        public PaneResizer()
        {
            this.Background = this.defaultColor;
            this.MouseUp += StretchingActionEnd;
            this.MouseEnter += this.OnMouseEnter;
            this.MouseLeave += this.OnMouseLeave;
            this.Margin = new Thickness(5);
            this.VerticalAlignment = VerticalAlignment.Stretch;
            this.BorderThickness = new Thickness(0);

            this.DragDelta += PaneResizer_DragDelta;
        }

        private void PaneResizer_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            if ((this.ResizableColumn != null && this.ResizableRow != null) || (this.ResizableColumn == null && this.ResizableRow == null))
            {
                return;
            }

            if (this.ResizableColumn != null)
            {
                this.ResizeColumn(e);
            }
            else if (this.ResizableRow != null)
            {
                this.ResizeRow(e);
            }
        }

        private void ResizeColumn(System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            if (this.VisibilityFrameCheck == null)
            {
                return;
            }

            this._safetyMargin = this.Width * this.SafetyMarginMultiplication;

            if (this.ResizableColumn.MinWidth == 0) { this.ResizableColumn.MinWidth = _safetyMargin; }

            this.Background = GUICommons.CommonBrushes_Green;

            double xAdjust = e.HorizontalChange / 15d;
            double finalWidth = (this.ResizableColumn.ActualWidth + xAdjust) > this.ResizableColumn.MinWidth ? (this.ResizableColumn.ActualWidth + xAdjust) : this.ResizableColumn.MinWidth;

            this.ResizableColumn.Width = new GridLength(finalWidth);

            if (!this.IsUserVisible(this, this.VisibilityFrameCheck, _safetyMargin))
            {
                this.ResizableColumn.Width = new GridLength(this.ResizableColumn.Width.Value - _safetyMargin);
            }
        }

        private void ResizeRow(System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            if (this.VisibilityFrameCheck == null)
            {
                return;
            }

            this._safetyMargin = this.Height * this.SafetyMarginMultiplication;

            if (this.ResizableRow.MinHeight == 0) { this.ResizableRow.MinHeight = _safetyMargin; }

            this.Background = GUICommons.CommonBrushes_Green;

            double yAdjust = (e.VerticalChange) / 15d;
            double finalHeight = (this.ResizableRow.ActualHeight + yAdjust) > this.ResizableRow.MinHeight ? (this.ResizableRow.ActualHeight + yAdjust) : this.ResizableRow.ActualHeight;

            this.ResizableRow.Height = new GridLength(finalHeight);

            if (!this.IsUserVisible(this, this.VisibilityFrameCheck, _safetyMargin))
            {
                this.ResizableRow.Height = new GridLength(this.ResizableRow.Height.Value - _safetyMargin);
            }
        }

        private void StretchingActionEnd(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.Background = this.defaultColor;
        }

        private void OnMouseEnter(object sender, System.Windows.Input.MouseEventArgs args)
        {
            this.Background = GUICommons.CommonBrushes_DarkBlue;
        }

        private void OnMouseLeave(object sender, System.Windows.Input.MouseEventArgs args)
        {
            this.Background = this.defaultColor;
        }


        /// <summary>
        /// copycat from stack overflow https://stackoverflow.com/questions/3685566
        /// </summary>
        private bool IsUserVisible(FrameworkElement element, FrameworkElement container, double safetyMargin = 0)
        {
            if (!element.IsVisible)
                return false;

            Rect bounds = element.TransformToAncestor(container).TransformBounds(new Rect(0.0, 0.0, element.ActualWidth, element.ActualHeight));
            Rect rect = new Rect(0.0, 0.0, container.ActualWidth - safetyMargin, container.ActualHeight - safetyMargin);
            return rect.Contains(bounds.TopLeft) || rect.Contains(bounds.BottomRight);
        }
    }
}
