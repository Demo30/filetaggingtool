﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Demos.FileTagger.ORM;

namespace Demos.FileTaggingTool.UserInterface.Controls.TaggingRelated.AvailableTagsListView
{
    public class TagStandaloneControl : UserControl
    {
        public event EventHandler<TagEventArgs> Request = null;
        public SolidColorBrush BackgroundOnMouseOver { get; set; } = GUICommons.CommonBrushes_Blue;

        protected readonly Tag _tag = null;

        public TagStandaloneControl(Tag tag)
        {
            _tag = tag ?? throw new ArgumentNullException();
            Initialize();
        }

        private void Initialize()
        {
            AllowDrop = false;

            var grid = new Grid();

            var txt = new TextBlock()
            {
                Text = _tag.Header,
                Margin = new Thickness(5, 5, 5, 5)
            };

            grid.Children.Add(txt);

            Content = grid;

            MouseMove += OnMouseOverRow;
            MouseLeave += (sender, args) =>
            {
                Background = new SolidColorBrush(Colors.Transparent);
            };

            ContextMenu = GetRootContextMenu();
        }

        private ContextMenu GetRootContextMenu()
        {
            var cm = new ContextMenu();
            GetContextMenuItems(cm).Select(i => i.Value).ToList().ForEach(i => cm.Items.Add(i));
            return cm;
        }

        private IDictionary<TagEventArgs.RequestTypes, MenuItem> GetContextMenuItems(ContextMenu cm)
        {
            return new Dictionary<TagEventArgs.RequestTypes, MenuItem>
            {
                { TagEventArgs.RequestTypes.DISSOCIATE_TAG_GENERAL, CreateContextMenuItem(cm, TagEventArgs.RequestTypes.DISSOCIATE_TAG_GENERAL, UserMessages.RemoveTagAssociationWithFile) },
            };
        }

        private MenuItem CreateContextMenuItem(ContextMenu cMenu, TagEventArgs.RequestTypes type, string title)
        {
            var item = new MenuItem { Header = title };
            item.Click += (sender, args) =>
            {
                var itemArgs = new TagEventArgs(null)
                {
                    Request = type,
                    PassedTag = _tag
                };
                SendRequest(itemArgs);
                cMenu.IsOpen = false;
            };
            return item;
        }

        protected virtual void SendRequest(TagEventArgs args)
        {
            Request?.Invoke(this, args);
        }

        private void OnMouseOverRow(object sender, MouseEventArgs args)
        {
            if (!(sender is TagStandaloneControl tagControl))
            {
                return;
            }

            Background = BackgroundOnMouseOver;

            if (args.LeftButton == MouseButtonState.Pressed)
            {
                OnTagListItemMouseDrag();
            }
        }

        private void OnTagListItemMouseDrag()
        {
            var dat = new TagDragDropData
            {
                Sender = this,
                Tag = _tag
            };

            DragDrop.DoDragDrop(this, dat, DragDropEffects.Move);
        }
    }
}
