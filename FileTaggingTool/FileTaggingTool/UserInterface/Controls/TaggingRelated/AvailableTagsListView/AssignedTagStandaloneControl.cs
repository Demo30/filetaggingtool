﻿using System;
using Demos.FileTagger.ORM;

namespace Demos.FileTaggingTool.UserInterface.Controls.TaggingRelated.AvailableTagsListView
{
    public class AssignedTagStandaloneControl : TagStandaloneControl
    {
        private readonly TagAssignmentType _assignmentType;
        
        public AssignedTagStandaloneControl(Tag tag, TagAssignmentType assignmentType) :  base(tag)
        {
            _assignmentType = assignmentType;
        }
        
        protected override void SendRequest(TagEventArgs args)
        {
            if (args.Request == TagEventArgs.RequestTypes.DISSOCIATE_TAG_GENERAL)
            {
                args.Request = ResolveDissociateRequestType();
            }
            base.SendRequest(args);
        }

        private TagEventArgs.RequestTypes ResolveDissociateRequestType()
        {
            switch (_assignmentType)
            {
                case TagAssignmentType.PrimaryTag: return TagEventArgs.RequestTypes.DISSOCIATE_TAG_PRIMARY;
                case TagAssignmentType.SecondaryTag: return TagEventArgs.RequestTypes.DISSOCIATE_TAG_SECONDARY;
                default: throw new NotImplementedException("No implementation for assignment type: " + _assignmentType);
            }
        }
    }
}
