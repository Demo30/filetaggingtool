﻿using System.Linq;
using System.Windows;
using Demos.FileTagger.ORM;

namespace Demos.FileTaggingTool.UserInterface.Controls.TaggingRelated.AvailableTagsListView
{
    public class TagDragDropData
    {
        public object Sender { get; set; }
        public Tag Tag { get; set; }

        public static TagDragDropData GetTagDropDataFromDragEventArgs(DragEventArgs dragEventArgs)
        {
            var type = typeof(TagDragDropData).ToString();
            var validInput =
                dragEventArgs.Data.GetFormats().Contains(type) &&
                dragEventArgs.Data.GetDataPresent(type);

            if (!validInput)
            {
                return null;
            }

            var passedData = (TagDragDropData)dragEventArgs.Data.GetData(type);

            return passedData;
        }
    }
}
