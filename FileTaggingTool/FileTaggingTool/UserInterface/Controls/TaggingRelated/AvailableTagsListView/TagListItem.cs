﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Demos.FileTagger.ORM;

namespace Demos.FileTaggingTool.UserInterface.Controls.TaggingRelated.AvailableTagsListView
{
    public class TagListItem : ListViewItem, ITagItemDroppable
    {
        public event EventHandler<EventArgs> ExpansionStateChanged;
        public event EventHandler<ParentDropTagEventArgs> TagTreeStructureChangeRequested;
        public event EventHandler<TagEventArgs> TagRequest;

        public bool IsRootItem { get; private set; }

        public bool IsExpanded => _expander.IsExpanded;
        public Tag TagOrmObject { get; private set; }

        private CheckBox _checkBox;
        private StackPanel _stackPanel;
        private readonly TagListItemExpanderElement _expander;

        public TagListItem(bool expanded = true)
        {
            _expander = new TagListItemExpanderElement(expanded);
        }

        public void Initialize(Tag tag)
        {
            TagOrmObject = tag ?? throw new ArgumentNullException();
            InitializeWithTag();
        }

        public void InitializeAsRoot()
        {
            InitializeCommon();

            var sp = new StackPanel { Orientation = Orientation.Horizontal };
            sp.Children.Add(_expander);
            sp.Children.Add(new TextBlock { Text = UserMessages.TagRootLevel , Margin = new Thickness(7, 0, 0, 0) });
            
            Content = sp;

            PreviewMouseRightButtonDown += (sender, args) =>
            {
                var subMenu = new ContextMenu();
                subMenu.Items.Add(CreateMenuItem(subMenu, this, TagEventArgs.RequestTypes.CREATE_ROOT_TAG, UserMessages.CreateNewTag));
                subMenu.IsOpen = true;
            };

            Drop += (sender, e) =>
            {
                ResetVisual();

                var type = typeof(TagDragDropData).ToString();
                var validInput =
                    (e.Data.GetFormats().Contains(type)) &&
                    (e.Data.GetDataPresent(type));

                if (!validInput)
                {
                    return;
                }

                var passedTag = (TagDragDropData)e.Data.GetData(type);

                if (TagTreeStructureChangeRequested == null) return;
                try { TagTreeStructureChangeRequested(this, new ParentDropTagEventArgs(passedTag.Tag)); } catch (Exception) { }
            };

            IsRootItem = true;
        }

        public void UpdateByTagAssociation(bool isAssociated)
        {
            _checkBox.IsChecked = isAssociated;
        }

        public void HideExpansionButton()
        {
            _expander.Hide();
            ResetVisual();
        }

        public TagDragDropData GetDataFromDragEventArgs(DragEventArgs dragEventArgs)
        {
            var data = TagDragDropData.GetTagDropDataFromDragEventArgs(dragEventArgs);
            return data;
        }

        public void SendRequest(TagEventArgs.RequestTypes type)
        {
            if (TagRequest == null) return;
            var args = new TagEventArgs(TagOrmObject)
            {
                Request = type
            };
            TagRequest(this, args);
        }

        private void InitializeCommon()
        {
            AllowDrop = true;

            _expander.ExpansionStateChanged += (sender, args) =>
            {
                if (ExpansionStateChanged == null) return;
                try { ExpansionStateChanged(this, EventArgs.Empty); } catch { }
            };

            DragEnter += (obj, args) =>
            {
                Background = GUICommons.CommonBrushes_Blue;
            };
            DragLeave += (obj, args) => { ResetVisual(); };
        }

        private void InitializeWithTag()
        {
            InitializeCommon();

            var grid = new Grid();
            _stackPanel = new StackPanel();
            _stackPanel.Children.Add(grid);

            _expander.HorizontalAlignment = HorizontalAlignment.Left;
            _expander.VerticalAlignment = VerticalAlignment.Center;
            _expander.Margin = new Thickness(0);
            _expander.ExpansionStateChanged += (sender, args) =>
            {
                if (ExpansionStateChanged == null) return;
                try { ExpansionStateChanged(this, EventArgs.Empty); } catch { }
            };

            _checkBox = new CheckBox()
            {
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Center,
                Margin = new Thickness(25, 0, 0, 0)
            };
            _checkBox.Checked += OnCheckBoxStateChange;
            _checkBox.Unchecked += OnCheckBoxStateChange;

            var txt = new TextBlock()
            {
                Text = TagOrmObject.Header,
                Margin = new Thickness(45, 0, 0, 0)
            };

            grid.Children.Add(_expander);
            grid.Children.Add(_checkBox);
            grid.Children.Add(txt);

            Content = _stackPanel;

            MouseMove += OnMouseOverRow;
            PreviewMouseRightButtonDown += (sender, args) =>
            {
                var subMenu = GetContextMenuForTagListItem(this);

                subMenu.IsOpen = true;
            };
            Drop += TagListItem_Drop;
        }

        private void ResetVisual(TagListItem item = null)
        {
            if (item == null)
            {
                Background = new SolidColorBrush(Colors.Transparent);
            }
            else
            {
                item.Background = new SolidColorBrush(Colors.Transparent);
            }
        }

        private void OnCheckBoxStateChange(object sender, RoutedEventArgs args)
        {
            if (!(sender is CheckBox cb))
            {
                throw new InvalidOperationException();
            }

            SendRequest(cb.IsChecked == true
                ? TagEventArgs.RequestTypes.ASSIGN_TAG
                : TagEventArgs.RequestTypes.DISSOCIATE_TAG_GENERAL);
        }

        private void OnMouseOverRow(object sender, MouseEventArgs args)
        {
            if (sender is TagListItem row && args.LeftButton == MouseButtonState.Pressed)
            {
                OnTagListItemMouseDrag();
            }
        }

        private void OnTagListItemMouseDrag()
        {
            var dat = new TagDragDropData
            {
                Sender = this,
                Tag = TagOrmObject
            };

            DragDrop.DoDragDrop(this, dat, DragDropEffects.Move);
        }

        private void TagListItem_Drop(object sender, DragEventArgs e)
        {
            ResetVisual();

            var passedData = GetDataFromDragEventArgs(e);

            if (passedData == null) { return; }

            var newParentTag = TagOrmObject;

            if (passedData.Tag.Id == newParentTag.Id) { return; }

            if (TagTreeStructureChangeRequested == null) return;
            try { TagTreeStructureChangeRequested(this, new ParentDropTagEventArgs(passedData.Tag, newParentTag)); } catch(Exception) { }
        }

        private ContextMenu GetContextMenuForTagListItem(TagListItem tagListItem)
        {
            if (tagListItem == null) { throw new ArgumentNullException(); }

            var subMenu = new ContextMenu
            {
                PlacementTarget = tagListItem
            };

            subMenu.Items.Add(CreateMenuItem(subMenu, tagListItem, TagEventArgs.RequestTypes.RENAME, UserMessages.RenameTag));
            subMenu.Items.Add(CreateMenuItem(subMenu, tagListItem, TagEventArgs.RequestTypes.ADD_TAG_TO_FILTER, UserMessages.AddTagToFilter));
            subMenu.Items.Add(CreateMenuItem(subMenu, tagListItem, TagEventArgs.RequestTypes.CREATE_NESTED_TAG, UserMessages.CreateNestedTag));
            subMenu.Items.Add(CreateMenuItem(subMenu, tagListItem, TagEventArgs.RequestTypes.DELETE, UserMessages.DeleteTag));
            subMenu.Items.Add(CreateMenuItem(subMenu, tagListItem, TagEventArgs.RequestTypes.ASSIGN_TAG, UserMessages.AssignTag));
            subMenu.Items.Add(CreateMenuItem(subMenu, tagListItem, TagEventArgs.RequestTypes.ASSIGN_TAG_SECONDARY, UserMessages.AssignTagSecondary));
            subMenu.Items.Add(CreateMenuItem(subMenu, tagListItem, TagEventArgs.RequestTypes.DISSOCIATE_TAG_GENERAL, UserMessages.RemoveTagAssociationWithFile));
            subMenu.Items.Add(CreateMenuItem(subMenu, tagListItem, TagEventArgs.RequestTypes.COLLAPSE_NESTED_NODES, UserMessages.CollapseAllSubLevels));

            return subMenu;
        }

        private MenuItem CreateMenuItem(ContextMenu cMenu, TagListItem tagListItem, TagEventArgs.RequestTypes type, string title)
        {
            var item = new MenuItem() { Header = title };
            item.Click += (sender, args) =>
            {
                tagListItem.SendRequest(type);
                cMenu.IsOpen = false;
            };

            return item;
        }
    }
}
