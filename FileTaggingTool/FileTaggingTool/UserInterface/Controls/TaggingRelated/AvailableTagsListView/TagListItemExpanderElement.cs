﻿using System;
using System.Windows.Controls;

namespace Demos.FileTaggingTool.UserInterface.Controls.TaggingRelated.AvailableTagsListView
{
    public class TagListItemExpanderElement : UserControl
    {
        public bool IsExpanded { get; private set; } = true;
        public event EventHandler<EventArgs> ExpansionStateChanged = null;

        public TagListItemExpanderElement(bool expanded = true)
        {
            this.MouseDown += TagListItemExpanderElement_MouseDown;
            this.Width = 20;
            this.Height = 20;
            this.IsExpanded = expanded;
            this.AdjustVisual();
        }

        public void Hide()
        {
            this.Content = null;
        }

        private void TagListItemExpanderElement_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.ExpansionToggle();
        }

        private void ExpansionToggle()
        {
            this.IsExpanded = this.IsExpanded ? false : true;
            this.AdjustVisual();

            if (this.ExpansionStateChanged != null) { try { this.ExpansionStateChanged(this, EventArgs.Empty); } catch { } }
        }

        private void AdjustVisual()
        {
            if (this.IsExpanded)
            {
                this.Content = new Image() { Source = DemosHelpers.GeneralHelperClass.GetImageSourceFromString(Demos.FileTaggingTool.Resources.InternalResourceMapper.ImageResource + "expander_expanded.png") };
            }
            else
            {
                this.Content = new Image() { Source = DemosHelpers.GeneralHelperClass.GetImageSourceFromString(Demos.FileTaggingTool.Resources.InternalResourceMapper.ImageResource + "expander_collapsed.png") };
            }
        }
    }
}
