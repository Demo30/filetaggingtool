﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Demos.FileTagger;
using Demos.FileTagger.ORM;

namespace Demos.FileTaggingTool.UserInterface.Controls.TaggingRelated.AvailableTagsListView
{
    public class TagListView : ListView
    {
        public event EventHandler<TagEventArgs> TagRequest = null;
        public event EventHandler<EventArgs> StructureChanged = null;
        public event EventHandler<EventArgs> TagListItemChanged = null;
        public bool IsInValidState => _manager != null && _getCurrentlySelectedFile != null;

        private Manager _manager { get; set; }
        private Func<File[]> _getCurrentlySelectedFile;
        private Tag[] _assignedTags = Array.Empty<Tag>();
        private Tag[] _validTags = Array.Empty<Tag>();
        private bool _isRootCollapsed = false;
        private List<int> _collapsedTagIDs = new List<int>();
        private string _filteringMask = null;

        public TagListView() { }

        public void Instantiate(Manager manager, Func<File[]> GetCurrentlySelectedFile)
        {
            if (manager == null || GetCurrentlySelectedFile == null) { throw new ArgumentNullException(); }

            _manager = manager;
            _getCurrentlySelectedFile = GetCurrentlySelectedFile;
            PreviewDragOver += OnDragOver;
            ContextMenu = GetRootContextMenu();
        }

        private ContextMenu GetRootContextMenu()
        {
            var cm = new ContextMenu();
            cm.Items.Add(CreateContextMenuItem(cm, TagEventArgs.RequestTypes.CREATE_ROOT_TAG, UserMessages.CreateNewTag));
            return cm;
        }

        private MenuItem CreateContextMenuItem(ContextMenu cMenu, TagEventArgs.RequestTypes type, string title)
        {
            var item = new MenuItem { Header = title };
            item.Click += (sender, args) =>
            {
                var itemArgs = new TagEventArgs(null)
                {
                    Request = type
                };
                ForwardTagRequest(itemArgs);
                cMenu.IsOpen = false;
            };
            return item;
        }

        public void UpdateTagPresentation(Tag[] assignedTags, Tag[] validTags)
        {
            UpdateTagPresentation(assignedTags, validTags, String.Empty);
        }

        public void UpdateTagPresentation(Tag[] assignedTags, Tag[] validTags, string filterMask)
        {
            if (!IsInValidState)
            {
                throw new InvalidOperationException("Cannot update since the control is not in valid state.");
            }
            if (assignedTags == null || validTags == null)
            {
                throw new ArgumentNullException("Arrays of tags cannot be null. They can be empty.");
            }

            _assignedTags = assignedTags;
            _validTags = validTags;
            _filteringMask = filterMask;

            ItemsSource = null;
            Items.Clear();

            AddRootLevelRow();

            if (string.IsNullOrEmpty(filterMask))
            {
                PopulateView_NoMask();
            }
            else
            {
                PopulateView_Mask(filterMask);
            }
        }

        internal void Tag_Assign(Tag curTag, File[] currentlySelectedFiles, TagAssignmentType assignmentType = TagAssignmentType.PrimaryTag)
        {
            if (curTag == null || currentlySelectedFiles == null) { return; }

            var numOfFile = currentlySelectedFiles.Length;

            if (numOfFile > Constants.NumberOfFilesLimitForConfirmation)
            {
                var res = MessageBox.Show($"Skutečně chcete přidělit tag většímu množství souborů ({numOfFile})?", UserMessages.WarningCaption, MessageBoxButton.OKCancel);
                if (res != MessageBoxResult.OK)
                {
                    return;
                }
            }

            var assignedTags = _manager.GetAssignedTags(currentlySelectedFiles.Select(f => f.Id), true);
            var filesWithoutCurrentTag = currentlySelectedFiles.Where(f => !assignedTags.Any(at =>
                    at.File.Id == f.Id && at.Tag.Id == curTag.Id && at.TagAssignmentType == assignmentType));
            _manager.AssignTag(filesWithoutCurrentTag.Select(file => (curTag.Id, ID: file.Id)), assignmentType);
        }

        internal void Tag_Dissociate(Tag curTag, File[] currentlySelectedFiles)
        {
            if (curTag == null || currentlySelectedFiles == null) { return; }

            var numOfFile = currentlySelectedFiles.Length;

            if (numOfFile > Constants.NumberOfFilesLimitForConfirmation)
            {
                var res = MessageBox.Show($"Skutečně chcete odebrat štítek většímu množství souborů ({numOfFile})?", UserMessages.WarningCaption, MessageBoxButton.OKCancel);
                if (res != MessageBoxResult.OK)
                {
                    return;
                }
            }
            
            _manager.DissociateTags(curTag.Id, currentlySelectedFiles.Select(f => f.Id), null);
        }

        private void AddRootLevelRow()
        {
            var rootItem = new TagListItem(!_isRootCollapsed);
            rootItem.InitializeAsRoot();

            rootItem.ExpansionStateChanged += (sender, args) =>
            {
                SwitchExpansionStateOfAllRootLevelTags(!rootItem.IsExpanded);
            };

            rootItem.TagRequest += OnTagRequest;
            rootItem.TagTreeStructureChangeRequested += OnTagTreeStructureChangeRequested;

            Items.Add(rootItem);
        }

        private void OnTagTreeStructureChangeRequested(object sender, ParentDropTagEventArgs args)
        {
            if (args == null) { throw new ArgumentNullException(); }

            if (args.ParentNull)
            {
                _manager.MoveInTagTreeStructure(args.PassedTag.Id, null);
            }
            else
            {
                if (IsIdAncestor(args.ParentTag.Id, args.PassedTag.Id, _validTags))
                {
                    MessageBox.Show("Nelze provést. Podřazení tagu by vyvolalo zacyklení.", UserMessages.WarningCaption);
                    return;
                }

                _manager.MoveInTagTreeStructure(args.PassedTag.Id, args.ParentTag.Id);
            }

            if (StructureChanged == null) return;
            try { StructureChanged(this, EventArgs.Empty); } catch (Exception) { }
        }

        private void OnCollapseNestedNodesRequest(object sender, TagEventArgs args)
        {
            if (sender is TagListItem currentItem)
            {
                var allTags = _validTags;
                var idsToCollapse = allTags
                    .Where(curTag => curTag.Id != currentItem.TagOrmObject.Id)
                    .Where(curTag => IsIdAncestor(curTag.Id, currentItem.TagOrmObject.Id, allTags))
                    .Select(curTag => curTag.Id)
                    .ToList();

                _collapsedTagIDs.AddRange(idsToCollapse);

                ForwardTagRequest(args);
            }
            else
            {
                throw new ArgumentException($"Expected request from a {nameof(TagListItem)} type.");
            }
        }

        private void AddNestedLevelsRecursive(TagListItem currentLevel, int level, IReadOnlyCollection<int> restrictedIDs = null)
        {
            var restrict = restrictedIDs is {Count: > 0};

            var nestedLevelTagsOrdered = _validTags
                    .Where(tag => tag.ParentTagID == currentLevel.TagOrmObject.Id)
                    .OrderBy(tag => tag.Header)
                    .ToArray();

            if (!nestedLevelTagsOrdered.Any()) { return; }

            foreach (var curNestedTag in nestedLevelTagsOrdered)
            {
                if (restrict && !restrictedIDs.Contains(curNestedTag.Id)) continue;
                
                var nestedLevel = CreateItem(curNestedTag);
                    
                nestedLevel.Margin = new Thickness(20 * level, 0, 0, 0);
                Items.Add(nestedLevel);

                if (!_collapsedTagIDs.Contains(curNestedTag.Id))
                {
                    AddNestedLevelsRecursive(nestedLevel, level + 1, restrictedIDs);
                }
            }
        }

        private TagListItem CreateItem(Tag tag)
        {
            var expanded = !_collapsedTagIDs.Contains(tag.Id);
            var it = new TagListItem(expanded);

            it.Initialize(tag);

            if (IsTagLeaf(tag, _validTags))
            {
                it.HideExpansionButton();
            }
            
            it.ExpansionStateChanged += OnExpansionStateChanged;

            if (_assignedTags.Contains(tag))
            {
                it.UpdateByTagAssociation(true);
            }

            it.TagRequest += OnTagRequest;
            it.TagTreeStructureChangeRequested += OnTagTreeStructureChangeRequested;

            return it;
        }

        private void OnTagRequest(object sender, TagEventArgs args)
        {
            switch (args.Request)
            {
                case TagEventArgs.RequestTypes.ASSIGN_TAG: OnTagAssignRequest(sender); break;
                case TagEventArgs.RequestTypes.ASSIGN_TAG_SECONDARY: OnTagAssignRequestSecondary(sender); break;
                case TagEventArgs.RequestTypes.DISSOCIATE_TAG_GENERAL: OnTagDissociationRequest(sender); break;
                case TagEventArgs.RequestTypes.RENAME: OnTagRenameRequest(args); break;
                case TagEventArgs.RequestTypes.TAGTREESTRUCTUREUPDATE: OnTagTreeStructureChangeRequested(sender, null); break;
                case TagEventArgs.RequestTypes.COLLAPSE_NESTED_NODES: OnCollapseNestedNodesRequest(sender, args); break;
                case TagEventArgs.RequestTypes.CREATE_NESTED_TAG:
                case TagEventArgs.RequestTypes.CREATE_ROOT_TAG:
                case TagEventArgs.RequestTypes.ADD_TAG_TO_FILTER:
                case TagEventArgs.RequestTypes.DELETE:
                default:
                    ForwardTagRequest(args); break;
            }
        }

        private void PopulateView_NoMask()
        {
            var rootLevelTagsOrdered = from tag in _validTags where tag.ParentTagID == null orderby tag.Header select tag;

            foreach (var curTag in rootLevelTagsOrdered)
            {
                var it = CreateItem(curTag);

                Items.Add(it);

                if (!_collapsedTagIDs.Contains(curTag.Id))
                {
                    AddNestedLevelsRecursive(it, 1);
                }
            }
        }

        private void PopulateView_Mask(string mask, bool includeSubTags = true)
        {
            var matchingTags = _validTags
                .Where(tag => DoesTagMatchMask(tag, mask, true))
                .OrderBy(tag => tag.Header)
                .ToList();

            var restrictedIDs = new List<int>();
            
            var matchingIDs = matchingTags.Select(id => id.Id).ToList();
            restrictedIDs.AddRange(matchingIDs);

            if (includeSubTags)
            {
                Tag[] FindNew() => _validTags
                    .Where(subTag => 
                        subTag.ParentTagID != null
                        && restrictedIDs.Contains((int)subTag.ParentTagID)
                        && !restrictedIDs.Contains(subTag.Id))
                    .ToArray();

                var currentSubTags = FindNew();
                while (currentSubTags.Any())
                {
                    restrictedIDs.AddRange(currentSubTags.Select(t => t.Id));
                    currentSubTags = FindNew();
                }
            }

            var ancestorIDs = GatherAncestorIDsRecursive(matchingTags).ToList();
            restrictedIDs.AddRange(ancestorIDs);

            var rootLevelTagsOrdered = _validTags
                .Where(tag => tag.ParentTagID == null)
                .OrderBy(tag => tag.Header);
            
            foreach (var curTag in rootLevelTagsOrdered)
            {
                if (!restrictedIDs.Contains(curTag.Id)) { continue; }

                var it = CreateItem(curTag);

                Items.Add(it);

                if (!_collapsedTagIDs.Contains(curTag.Id))
                {
                    AddNestedLevelsRecursive(it, 1, restrictedIDs.ToArray());
                }
            }
        }

        private void ForwardTagRequest(TagEventArgs args)
        {
            if (TagRequest == null) return;
            try { TagRequest(this, args); } catch { }
        }

        private void OnTagAssignRequest(object sender)
        {
            if (sender is TagListItem item) { Tag_Assign(item.TagOrmObject, _getCurrentlySelectedFile(), TagAssignmentType.PrimaryTag); }

            if (TagListItemChanged == null) return;
            try { TagListItemChanged(this, EventArgs.Empty); } catch { }
        }
        
        private void OnTagAssignRequestSecondary(object sender)
        {
            if (sender is TagListItem item) { Tag_Assign(item.TagOrmObject, _getCurrentlySelectedFile(), TagAssignmentType.SecondaryTag); }

            if (TagListItemChanged == null) return;
            try { TagListItemChanged(this, EventArgs.Empty); } catch { }
        }

        private void OnTagDissociationRequest(object sender)
        {
            if (sender is TagListItem item) { Tag_Dissociate(item.TagOrmObject, _getCurrentlySelectedFile()); }

            if (TagListItemChanged == null) return;
            try { TagListItemChanged(this, EventArgs.Empty); } catch { }
        }

        private void OnTagRenameRequest(TagEventArgs args)
        {
            var tag = args.PassedTag;
            if (tag == null) { return; }

            void Action()
            {
                if (TagListItemChanged == null) return;
                try
                {
                    TagListItemChanged(this, EventArgs.Empty);
                }
                catch { }
            }

            CommonGUILogic.RenameTag(tag, Action);
        }

        private void OnDragOver(object sender, DragEventArgs args)
        {
            ScrollListViewWhileDraggingIfNeeded(sender, args);
        }

        private void ScrollListViewWhileDraggingIfNeeded(object sender, DragEventArgs args)
        {
            // implementation stolen from: https://stackoverflow.com/questions/10733581

            if (!(sender is FrameworkElement container))
            {
                return;
            }

            var scrollViewer = GetFirstVisualChild<ScrollViewer>(container);

            if (scrollViewer == null)
            {
                return;
            }

            const int tolerance = 30;
            const int offset = 3;
            var verticalPos = args.GetPosition(container).Y;

            if (verticalPos < tolerance) // Top of visible list? 
            {
                scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset - offset); //Scroll up. 
            }

            else if (verticalPos > container.ActualHeight - tolerance) //Bottom of visible list? 
            {
                scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset + offset); //Scroll down.     
            }
        }


        /// <summary>
        /// Gets the first matching tag from the bank. If none is present, return null.
        /// </summary>
        private Tag GetTagByID(int tagId, IEnumerable<Tag> bank)
        {
            var childTag = bank.Where(tag => tag.Id == tagId).ToArray();

            if (!childTag.Any())
            {
                return null;
            }

            var desiredTag = childTag.First();
            return desiredTag;
        }

        private bool IsIdAncestor(int childId, int parentId, IEnumerable<Tag> bank)
        {
            var childTag = GetTagByID(childId, bank);
            SearchForAncestorRecursive(childTag, parentId, out var result);
            return result;
        }

        private void SearchForAncestorRecursive(Tag childTag, int searchedId, out bool result)
        {
            if (childTag.Id == searchedId)
            {
                result = true;
                return;
            }

            if (childTag.ParentTagID == null)
            {
                result = false;
                return;
            }

            var tag = GetTagByID((int)childTag.ParentTagID, _validTags);
            SearchForAncestorRecursive(tag, searchedId, out result);
        }

        private void OnExpansionStateChanged(object sender, EventArgs args)
        {
            var item = (TagListItem)sender;
            var tagId = item.TagOrmObject.Id;

            if (item.IsExpanded == false && !_collapsedTagIDs.Contains(tagId))
            {
                _collapsedTagIDs.Add(tagId);
            }
            else if (item.IsExpanded && _collapsedTagIDs.Contains(tagId))
            {
                _collapsedTagIDs.Remove(tagId);
            }

            UpdateTagPresentation(_assignedTags, _validTags, _filteringMask);
        }

        private static bool IsTagLeaf(Tag tag, IEnumerable<Tag> tagStack)
        {
            var hasForParent = tagStack
                .Where(curTag => curTag.ParentTagID == tag.Id)
                .Select(curTag => curTag.Id);

            return !hasForParent.Any();
        }

        private void SwitchExpansionStateOfAllRootLevelTags(bool collapse)
        {
            if (Items.Count == 0)
            {
                return;
            }

            foreach(TagListItem it in Items)
            {
                if (it.IsRootItem || it.TagOrmObject.ParentTagID != null) { continue; }

                if (collapse && !_collapsedTagIDs.Contains(it.TagOrmObject.Id))
                {
                    _collapsedTagIDs.Add(it.TagOrmObject.Id);
                }
                else if (!collapse && _collapsedTagIDs.Contains(it.TagOrmObject.Id))
                {
                    _collapsedTagIDs.Remove(it.TagOrmObject.Id);
                }
            }

            _isRootCollapsed = collapse;
            UpdateTagPresentation(_assignedTags, _validTags, _filteringMask);
        }

        private bool DoesTagMatchMask(Tag tag, string mask, bool errorAsPass)
        {
            if (tag == null) { throw new ArgumentNullException(); }

            if (string.IsNullOrEmpty(mask)) { return true; }
            
            try
            {
                var result = System.Text.RegularExpressions.Regex.IsMatch(tag.Header, mask, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                return result;
            }
            catch
            {
                if (errorAsPass)
                {
                    return true;
                }

                throw;
            }
        }

        /// <summary>
        /// Clears the collection of tags, that are also ancestors of any of the other tags
        /// </summary>
        /// <returns></returns>
        private Tag[] GetTagsClearedOfItsOwnAncestors(Tag[] bank)
        {
            return bank
                .Select(curTag => new
                {
                    curTag,
                    isAncestor = bank
                        .Where(curTag2 => curTag2.Id != curTag.Id)
                        .Any(curTag2 => IsIdAncestor(curTag2.Id, curTag.Id, bank))
                })
                .Where(t => !t.isAncestor)
                .Select(t => t.curTag).ToArray();
        }

        private IEnumerable<int> GatherAncestorIDsRecursive(IEnumerable<Tag> tags)
        {
            var ids = new List<int>();

            foreach (var curTag in tags)
            {
                if (curTag.ParentTagID == null) continue;
                var parentTag = GetTagByID((int)curTag.ParentTagID, _validTags);
                if (parentTag == null) { throw new InvalidOperationException("Parent tag not found among available tag objects."); }

                ids.Add(parentTag.Id);

                var tempIds = GatherAncestorIDsRecursive(new[] { parentTag });

                ids.AddRange(tempIds);
            }

            return ids.Distinct();
        }

        #region Supportive methods

        private static T GetFirstVisualChild<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj == null) return null;
            
            for (var i = 0; i < System.Windows.Media.VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                var child = System.Windows.Media.VisualTreeHelper.GetChild(depObj, i);

                if (child is T dependencyObject)
                {
                    return dependencyObject;
                }

                var childItem = GetFirstVisualChild<T>(child);

                if (childItem != null)
                {
                    return childItem;
                }
            }

            return null;
        }

        #endregion
    }
}
