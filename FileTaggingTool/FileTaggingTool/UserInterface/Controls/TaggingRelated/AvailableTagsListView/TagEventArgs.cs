﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Demos.FileTagger.ORM;

namespace Demos.FileTaggingTool.UserInterface
{
    public class TagEventArgs : EventArgs
    {
        public enum RequestTypes
        {
            NO_REQUEST,
            RENAME,
            DELETE,
            ADD_TAG_TO_FILTER,
            REMOVEFROMFILTER,
            TAGTREESTRUCTUREUPDATE,
            ASSIGN_TAG,
            ASSIGN_TAG_SECONDARY,
            DISSOCIATE_TAG_GENERAL,
            DISSOCIATE_TAG_PRIMARY,
            DISSOCIATE_TAG_SECONDARY,
            CREATE_NESTED_TAG,
            CREATE_ROOT_TAG,
            COLLAPSE_NESTED_NODES,
        }

        public RequestTypes Request { get; set; } = RequestTypes.NO_REQUEST;
        public Tag PassedTag { get; set; }

        public TagEventArgs(Tag passedTag)
        {
            PassedTag = passedTag;
        }
    }
}
