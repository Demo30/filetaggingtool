﻿using System;
using Demos.FileTagger.ORM;

namespace Demos.FileTaggingTool.UserInterface.Controls.TaggingRelated.AvailableTagsListView
{
    public class ParentDropTagEventArgs : EventArgs
    {
        public Tag ParentTag { get; private set; }
        public Tag PassedTag { get; private set; }
        public bool ParentNull { get; private set; } = false;

        public ParentDropTagEventArgs(Tag passedTag, Tag parentTag)
        {
            if (passedTag == null || parentTag == null)
            {
                throw new ArgumentNullException();
            }

            this.PassedTag = passedTag;
            this.ParentTag = parentTag;
        }

        public ParentDropTagEventArgs(Tag passedTag)
        {
            if (passedTag == null)
            {
                throw new ArgumentNullException();
            }

            this.PassedTag = passedTag;
            this.ParentNull = true;
        }
    }
}
