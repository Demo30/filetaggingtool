﻿using System.Windows;

namespace Demos.FileTaggingTool.UserInterface.Controls.TaggingRelated.AvailableTagsListView
{
    public interface ITagItemDroppable
    {
        TagDragDropData GetDataFromDragEventArgs(DragEventArgs droppedObject);
    }
}
