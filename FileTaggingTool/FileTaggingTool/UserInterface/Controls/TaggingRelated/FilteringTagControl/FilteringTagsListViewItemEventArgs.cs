﻿using System;

namespace Demos.FileTaggingTool.UserInterface.Controls.TaggingRelated.FilteringTagControl;

public class FilteringTagsListViewItemEventArgs : EventArgs
{
    public FilteredTag FilteringTag { get; }

    public FilteringTagsListViewItemEventArgs(FilteredTag filteringTag)
    {
        FilteringTag = filteringTag;
    }
}