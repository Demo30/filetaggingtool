﻿using System.Windows.Controls;
using Demos.DemosHelpers;

namespace Demos.FileTaggingTool.UserInterface.Controls.TaggingRelated.FilteringTagControl;

internal class TagRelationGuiSettings
{
    public Image Icon { get; set; }
    public string Description { get; set; }
    public SQLConditionRelationships Relationship {get;}

    public TagRelationGuiSettings(SQLConditionRelationships conditionRelationship)
    {
        Relationship = conditionRelationship;
    }
}