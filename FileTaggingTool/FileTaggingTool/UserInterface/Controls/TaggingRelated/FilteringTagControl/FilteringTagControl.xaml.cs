﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Demos.DemosHelpers;
using Demos.FileTaggingTool.UserInterface.Controls.TaggingRelated.AvailableTagsListView;

namespace Demos.FileTaggingTool.UserInterface.Controls.TaggingRelated.FilteringTagControl;

public partial class FilteringTagControl
{
    public event EventHandler<EventArgs> FilteredTagsChanged;
    public event EventHandler<EventArgs> FilteringRelationshipChanged;
    public IEnumerable<FilteredTag> FilteringTags => _filteredTags;
    public FilteredTag CurrentlySelectedFilteringTag => lstv_filteringTags.SelectedTags.Length != 1 ? null : lstv_filteringTags.SelectedTags[0];
    public bool WithTagSubLevels => Convert.ToBoolean(chkbx_filterWithSubTags.IsChecked);

    public SQLConditionRelationships CurrentFilteringRelationship => filteredTagRelationSwitcher.CurrentState;

    private readonly List<FilteredTag> _filteredTags = new();

    public FilteringTagControl()
    {
        InitializeComponent();
        BindToEvents();
        PrepareFilteringTagsListView();
    }

    private void BindToEvents()
    {
        lstv_filteringTags.RemoveTagRequest += (_, args) => RemoveFilteringTag(args.FilteringTag);
        lstv_filteringTags.UpdateFilteredFilesListRequest += (_, _) => RaiseFilteringTagsChangedEvent();
    }

    public void AddFilteringTag(FilteredTag tag)
    {
        _filteredTags.Add(tag);
        RaiseFilteringTagsChangedEvent();
    }

    public void RemoveFilteringTag(FilteredTag tag)
    {
        _filteredTags.Remove(tag);
        RaiseFilteringTagsChangedEvent();
    }

    public void PopulateFilteringTagListView()
    {
        PopulateFilteringTagListViewCore();
    }

    private void PrepareFilteringTagsListView()
    {
        lstv_filteringTags.AllowDrop = true;
        lstv_filteringTags.Drop += (_, args) =>
        {
            var data = TagDragDropData.GetTagDropDataFromDragEventArgs(args);
            if (data is null)
            {
                return;
            }

            if (_filteredTags.All(t => t.TagId != data.Tag.Id))
            {
                _filteredTags.Add(FilteredTag.ToFilteredTag(data.Tag, CommonGUILogic.GetDefaultAssignmentTypesForFilter()));
            }

            RaiseFilteringTagsChangedEvent();
        };

        filteredTagRelationSwitcher.StateChanged += FilteredTagRelationSwitcher_StateChanged;
    }

    private void PopulateFilteringTagListViewCore()
    {
        var ordered = (_filteredTags.OrderBy(tag => tag.TagName)).ToArray();
        lstv_filteringTags.ResetTags(ordered);
    }

    private void FilteredTagRelationSwitcher_StateChanged(object sender, EventArgs args)
    {
        if (FilteringRelationshipChanged == null) return;
        try { FilteringRelationshipChanged(this, EventArgs.Empty); } catch { }
    }

    private void btn_clearFilter_Click(object sender, RoutedEventArgs args)
    {
        _filteredTags.Clear();
        RaiseFilteringTagsChangedEvent();
    }

    private void chkbx_withSubTags_Toggle(object sender, RoutedEventArgs routedEventArgs)
    {
        RaiseFilteringTagsChangedEvent();
    }

    private void RaiseFilteringTagsChangedEvent()
    {
        if (FilteredTagsChanged == null) return;
        try { FilteredTagsChanged(this, EventArgs.Empty); } catch { }
    }

}