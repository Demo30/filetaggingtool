﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Demos.FileTaggingTool.UserInterface.Controls.TaggingRelated.FilteringTagControl;

public partial class FilteringTagsListView
{
    public event EventHandler<FilteringTagsListViewItemEventArgs> RemoveTagRequest;
    public event EventHandler<EventArgs> UpdateFilteredFilesListRequest;

    public FilteredTag[] SelectedTags => lstvw.SelectedItems.Cast<object>().Select(item => item as FilteredTag).ToArray();

    public FilteringTagsListView()
    {
        InitializeComponent();
    }

    public void ResetTags(IEnumerable<FilteredTag> tags)
    {
        lstvw.ItemsSource = null;
        lstvw.Items.Clear();
        foreach(var currentTag in tags)
        {
            var li = new FilteringTagsListViewItem(currentTag, this);
            li.UpdateFilteredFilesListRequest += (_, _) =>
                UpdateFilteredFilesListRequest?.Invoke(this, EventArgs.Empty);
            lstvw.Items.Add(li);
        }
        lstvw.DisplayMemberPath = nameof(FileTagger.ORM.Tag.Header);
    }

    public void RequestFilteringTagRemoval(FilteredTag filteringTag)
    {
        RemoveTagRequest?.Invoke(this, new FilteringTagsListViewItemEventArgs(filteringTag));
    }
}