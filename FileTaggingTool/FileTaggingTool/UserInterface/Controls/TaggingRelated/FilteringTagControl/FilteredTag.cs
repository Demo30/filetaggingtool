using System.Collections.Generic;
using Demos.FileTagger.ORM;

namespace Demos.FileTaggingTool.UserInterface.Controls.TaggingRelated.FilteringTagControl;

public class FilteredTag
{
    public int TagId { get; set; }
    
    public string TagName { get; set; }

    public IEnumerable<TagAssignmentType> TagAssignmentType { get; set; } = new List<TagAssignmentType>();

    public static FilteredTag ToFilteredTag(Tag tag, IEnumerable<TagAssignmentType> assignmentTypes)
    {
        return new FilteredTag
        {
            TagId = tag.Id,
            TagName = tag.Header,
            TagAssignmentType = assignmentTypes ?? new List<TagAssignmentType>(),
        };
    }
}