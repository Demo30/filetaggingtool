namespace Demos.FileTaggingTool.UserInterface.Controls.TaggingRelated.FilteringTagControl;

public static class UserMessages
{
    public const string RELATION_AND = "Vztah A ZÁROVEŇ";
    public const string RELATION_OR = "Vztah NEBO";

    public const string REMOVE_TAG_FROM_FILTER = "Odebrat štítek z filtrování.";
    public const string CLEAR_FILTER = "Vyčistit filtr";
}