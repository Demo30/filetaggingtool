﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Demos.FileTagger.ORM;

namespace Demos.FileTaggingTool.UserInterface.Controls.TaggingRelated.FilteringTagControl;

public class FilteringTagsListViewItem : ListViewItem
{
    public event EventHandler<EventArgs> UpdateFilteredFilesListRequest;
    public FilteredTag FilteringTag { get; }
    private FilteringTagsListView OwningFilteringTagListView {get; }

    public FilteringTagsListViewItem(FilteredTag tag, FilteringTagsListView owningListView)
    {
        FilteringTag = tag;
        OwningFilteringTagListView = owningListView;
        Initialize();
    }

    private void Initialize()
    {
        UpdateVisuals();
    }

    private void UpdateVisuals()
    {
        HorizontalContentAlignment = HorizontalAlignment.Stretch; // stretch across the listview

        var grd = new Grid
        {
            HorizontalAlignment = HorizontalAlignment.Stretch
        };

        var clrFilterBtnWidth = 20;
        var clrFilterBtnPadding = 5;

        var col1 = new ColumnDefinition();
        var col2 = new ColumnDefinition
        {
            Width = new GridLength(clrFilterBtnWidth + (2 * clrFilterBtnPadding))
        };

        grd.ColumnDefinitions.Add(col1);
        grd.ColumnDefinitions.Add(col2);

        var rowContent = GetBasicRowContent();
        
        Grid.SetColumn(rowContent, 0);

        var clearFilterButton = new Button
        {
            Width = clrFilterBtnWidth,
            Padding = new Thickness(clrFilterBtnPadding, 0, clrFilterBtnPadding, 0),
            HorizontalAlignment = HorizontalAlignment.Right,
            Content = "X",
            ToolTip = UserMessages.REMOVE_TAG_FROM_FILTER
        };
        clearFilterButton.Click += OnRemoveFilteringTagEvent;
        Grid.SetColumn(clearFilterButton, 1);
            
        grd.Children.Add(rowContent);
        grd.Children.Add(clearFilterButton);
        Content = grd;
    }

    private UIElement GetBasicRowContent()
    {
        var rowContent = new Grid();
        rowContent.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star)});
        rowContent.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star)});
        
        var text = new TextBlock
        {
            Text = $"{FilteringTag.TagName}"
        };
        rowContent.Children.Add(text);
        Grid.SetColumn(text, 0);

        var button = new Button { Content = string.Join(", ", FilteringTag.TagAssignmentType.Select(GUICommons.LocalizeAssignmentType)) };
        button.Click += (_, _) => SwitchAssignmentType();
        rowContent.Children.Add(button);
        Grid.SetColumn(button, 1);

        return rowContent;
    }

    private void SwitchAssignmentType()
    {
        void UpdateVisualsAndRequestFileSearch()
        {
            UpdateVisuals();
            UpdateFilteredFilesListRequest?.Invoke(this, EventArgs.Empty);
        }
        
        if (FilteringTag.TagAssignmentType.Count() > 1)
        {
            FilteringTag.TagAssignmentType = new[] {TagAssignmentType.PrimaryTag};
            UpdateVisualsAndRequestFileSearch();
            return;
        }
            
        if (FilteringTag.TagAssignmentType.ToList()[0] == TagAssignmentType.PrimaryTag)
        {
            FilteringTag.TagAssignmentType = new[] {TagAssignmentType.SecondaryTag};
            UpdateVisualsAndRequestFileSearch();
            return;
        }

        FilteringTag.TagAssignmentType = new[] {TagAssignmentType.PrimaryTag, TagAssignmentType.SecondaryTag};
        UpdateVisualsAndRequestFileSearch();
    }

    private void OnRemoveFilteringTagEvent(object sender, RoutedEventArgs args)
    {
        OwningFilteringTagListView.RequestFilteringTagRemoval(FilteringTag);
    }
}