﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Demos.DemosHelpers;

namespace Demos.FileTaggingTool.UserInterface.Controls.TaggingRelated.FilteringTagControl;

public class FilteredTagRelationSwitcher : UserControl
{
    public event EventHandler<EventArgs> StateChanged = null;
    public SQLConditionRelationships CurrentState { get; private set; }
    private SolidColorBrush defaultColor = GUICommons.CommonBrushes_Blue;
    private Dictionary<SQLConditionRelationships, TagRelationGuiSettings> RelationshipGUISettings = new()
    {
        { SQLConditionRelationships.AND, new TagRelationGuiSettings(SQLConditionRelationships.AND)
            {
                Description = UserMessages.RELATION_AND,
                Icon = new Image {Source = GeneralHelperClass.GetImageSourceFromString(Demos.FileTaggingTool.Resources.InternalResourceMapper.ImageResource + "relation_AND.png")}
            }
        },
        { SQLConditionRelationships.OR, new TagRelationGuiSettings(SQLConditionRelationships.OR)
            {
                Description = UserMessages.RELATION_OR,
                Icon = new Image { Source = GeneralHelperClass.GetImageSourceFromString(Demos.FileTaggingTool.Resources.InternalResourceMapper.ImageResource + "relation_OR.png") }
            }
        }
    };


    public FilteredTagRelationSwitcher()
    {
        Background = defaultColor;
        MouseDown += MouseClick;
        MouseEnter += OnMouseEnter;
        MouseLeave += OnMouseLeave;
        BorderThickness = new Thickness(0);

        AdjustAccordingToState();
    }

    private void SwitchState(SQLConditionRelationships? relation = null)
    {
        if (relation == null)
        {
            CurrentState = (CurrentState == SQLConditionRelationships.AND) ?
                SQLConditionRelationships.OR : 
                SQLConditionRelationships.AND;

        }
        else
        {
            CurrentState = (SQLConditionRelationships)relation;
        }

        if (StateChanged == null) return;
        try { StateChanged(this, EventArgs.Empty); } catch { }
    }

    private void AdjustAccordingToState()
    {
        if (RelationshipGUISettings.ContainsKey(CurrentState))
        {
            var tagRelation = RelationshipGUISettings[CurrentState];
            Content = tagRelation.Icon;
            ToolTip = tagRelation.Description;
        } else
        {
            throw new Exception($"GUI setting definition missing for relationship: {CurrentState}");
        }
    }

    private void MouseClick(object sender, System.Windows.Input.MouseButtonEventArgs args)
    {
        SwitchState();
        AdjustAccordingToState();
    }

    private void OnMouseEnter(object sender, System.Windows.Input.MouseEventArgs args)
    {
        Background = new SolidColorBrush(Colors.White);
    }

    private void OnMouseLeave(object sender, System.Windows.Input.MouseEventArgs args)
    {
        Background = defaultColor;
    }
}