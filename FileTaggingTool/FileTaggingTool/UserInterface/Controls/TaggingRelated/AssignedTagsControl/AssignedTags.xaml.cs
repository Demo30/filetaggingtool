using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Demos.FileTagger.ORM;
using Demos.FileTaggingTool.UserInterface.Controls.TaggingRelated.AvailableTagsListView;

namespace Demos.FileTaggingTool.UserInterface.Controls.TaggingRelated.AssignedTagsControl
{
    public partial class AssignedTags
    {
        public event EventHandler<TagEventArgs> Request = null;

        public static DependencyProperty AssignedTagsLabelProperty =
            DependencyProperty.Register("AssignedTagsLabel", typeof(string), typeof(UserControl));
        
        public string AssignedTagsLabel
        {
            get => (string)GetValue(AssignedTagsLabelProperty);
            set => SetValue(AssignedTagsLabelProperty, value);
        }
        
        public static DependencyProperty AssignmentTypeProperty =
            DependencyProperty.Register("AssignmentType", typeof(TagAssignmentType), typeof(UserControl));
        
        public TagAssignmentType AssignmentType
        {
            get => (TagAssignmentType)GetValue(AssignmentTypeProperty);
            set => SetValue(AssignmentTypeProperty, value);
        }

        
        public AssignedTags()
        {
            InitializeComponent();
            PrepareDragAndDrop();
        }

        private void PrepareDragAndDrop()
        {
            AllowDrop = true;
            Drop += (sender, args) =>
            {
                var data = TagDragDropData.GetTagDropDataFromDragEventArgs(args);
                if (data is null)
                {
                    return;
                }
                
                Request?.Invoke(this, new TagEventArgs(data.Tag)
                {
                    Request = ResolveRequestType()
                });
            };
        }

        private TagEventArgs.RequestTypes ResolveRequestType()
        {
            switch (AssignmentType)
            {
                case TagAssignmentType.PrimaryTag: return TagEventArgs.RequestTypes.ASSIGN_TAG;
                case TagAssignmentType.SecondaryTag: return TagEventArgs.RequestTypes.ASSIGN_TAG_SECONDARY;
                default: throw new NotImplementedException("No implementation for assignment type: " + AssignmentType);
            }
        }

        public void SetAssignedTags(IEnumerable<Tag> tags, EventHandler<TagEventArgs> onTagRequestCallback)
        {
            var tagsArray = tags?.ToArray() ?? Array.Empty<Tag>();
            
            if (!tagsArray.Any())
            {
                cc_assignedTags.Content = string.Empty;
                return;
            }
            
            cc_assignedTags.Content = CreateWrapControl(tagsArray, onTagRequestCallback);
        }
        
        private WrapPanel CreateWrapControl(IEnumerable<Tag> tags, EventHandler<TagEventArgs> onTagRequestCallback)
        {
            var wrappedContent = new WrapPanel
            {
                Orientation = Orientation.Horizontal
            };
            foreach (var tag in tags)
            {
                var tagControl = new AssignedTagStandaloneControl(tag, AssignmentType);
                tagControl.Request += onTagRequestCallback;
                wrappedContent.Children.Add(tagControl);
            }
            return wrappedContent;
        }
    }
}