﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Demos.FileTaggingTool.UserInterface.Controls.TextSearchControl;

public partial class TextSearchControl
{
    public event EventHandler<TextChangedEventArgs> TextChanged;
    public string Text => txtbx_main.Text;

    public TextSearchControl()
    {
        InitializeComponent();
        Initialize();
    }

    private void Initialize()
    {
        UpdateClearTextButtonVisibility();
    }

    private void txt_changed(object sender, TextChangedEventArgs args)
    {
        UpdateClearTextButtonVisibility();
        
        try
        {
            if (TextChanged == null) return;
            TextChanged(this, args);
        }
        catch { }
    }

    private void btn_clear_Click(object sender, RoutedEventArgs args)
    {
        txtbx_main.Text = string.Empty;
    }

    private void UpdateClearTextButtonVisibility()
    {
        btn_clearText.Visibility = !string.IsNullOrEmpty(txtbx_main.Text)
            ? Visibility.Visible
            : Visibility.Hidden;
    }
}