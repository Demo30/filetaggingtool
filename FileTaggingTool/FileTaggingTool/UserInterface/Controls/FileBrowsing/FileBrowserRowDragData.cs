﻿using System;
using System.Linq;
using System.Windows;

namespace Demos.FileTaggingTool.UserInterface.Controls.FileBrowsing
{
    internal class FileBrowserRowDragData
    {
        public FilesBrowserRow Sender { get; }

        public FileBrowserRowDragData(FilesBrowserRow sender)
        {
            if (sender != null)
            {
                this.Sender = sender;
            }
            else
            {
                throw new ArgumentNullException();
            }
        }

        public static FileBrowserRowDragData GetDropDataFromDragEventArgs(DragEventArgs dragEventArgs)
        {
            string type = typeof(FileBrowserRowDragData).ToString();
            bool validInput =
                (dragEventArgs.Data.GetFormats().Contains(type)) &&
                (dragEventArgs.Data.GetDataPresent(type));

            if (!validInput)
            {
                return null;
            }

            FileBrowserRowDragData passedData = (FileBrowserRowDragData)dragEventArgs.Data.GetData(type);

            return passedData;
        }
    }
}
