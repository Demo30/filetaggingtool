﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using File = Demos.FileTagger.ORM.File;
using IO = System.IO;

namespace Demos.FileTaggingTool.UserInterface.Controls.FileBrowsing
{
    internal class FilesBrowserDirRow : FilesBrowserRow
    {
        public IO.DirectoryInfo DirectoryInfo { get; protected set; }
        protected Action<IO.DirectoryInfo> _directoryButtonClickMethod = null;

        public FilesBrowserDirRow(IO.DirectoryInfo dir, Action<IO.DirectoryInfo> buttonClickMethod) : base()
        {
            if (dir == null) { throw new ArgumentNullException(); }
            this.RowType = RowTypes.DirectoryRow;

            this.DirectoryInfo = dir;
            this._directoryButtonClickMethod = buttonClickMethod;

            this.PreviewMouseRightButtonDown += (sender, args) => { this.ContextMenu = GetContextMenu(this); };

            this.InitializeVisual();
        }

        protected override void InitializeVisual()
        {
            FileBrowserDirButton dirBtn = new FileBrowserDirButton();
            dirBtn.Click += (object sender, EventArgs args) =>
            {
                if (this._directoryButtonClickMethod != null)
                {
                    this._directoryButtonClickMethod(this.DirectoryInfo);
                }
            };

            this._mainContent.Children.Add(dirBtn);
            this._mainContent.Children.Add(new Label() { Content = this.DirectoryInfo.Name, VerticalContentAlignment = VerticalAlignment.Center });
        }

        protected override string GetCustomName()
        {
            return this.DirectoryInfo.Name;
        }

        protected override FileBrowserEventArgs GetRequestArgs(FileBrowserEventArgs.RequestTypes type)
        {
            return new FileBrowserEventArgs(this.DirectoryInfo, true, type);
        }

        protected override void OnMouseOverRow(object sender, MouseEventArgs args)
        {
            return;
        }

        protected override void OnFileBrowserRowDrop(FileBrowserRowDragData passedData, FilesBrowserRow targetRow)
        {
            string pathToMove = string.Empty;

            switch (passedData.Sender.RowType)
            {
                case RowTypes.FileRow:
                    FilesBrowserFileRow fileRow = (FilesBrowserFileRow)passedData.Sender;
                    File movedFile = fileRow.FileModel;
                    pathToMove = movedFile.GetFullFinalFilepath(FileTagger.Manager.Instance.OpenedLibraryUserDataDirPath);
                    break;
                case RowTypes.DirectoryRow:
                    FilesBrowserDirRow dirRow = (FilesBrowserDirRow)passedData.Sender;
                    IO.DirectoryInfo movedDirectory = dirRow.DirectoryInfo;
                    pathToMove = movedDirectory.FullName;
                    break;
            }

            if (targetRow is FilesBrowserDirRow) {
                FilesBrowserDirRow dirRow = (FilesBrowserDirRow)targetRow;
                CommonGUILogic.MoveContentToLibrary(dirRow.DirectoryInfo, new string[] { pathToMove }, false);
                this.SendRequest(FileBrowserEventArgs.RequestTypes.PROCESS_FILES_IN_FOLDER);
            }
        }

        private ContextMenu GetContextMenu(FilesBrowserRow fileBrowserRow)
        {
            if (fileBrowserRow == null) { throw new ArgumentNullException(); }

            ContextMenu subMenu = new ContextMenu();
            subMenu.PlacementTarget = fileBrowserRow;

            subMenu.Items.Add(this.CreateMenuItem(subMenu, fileBrowserRow, FileBrowserEventArgs.RequestTypes.PROCESS_FILES_IN_FOLDER, "Zpracovat soubory ve složce", this.GetConfirmationMethod("Opravdu chcete zpracovat soubory ve složce. Akce může být časově náročná", "Zpracovat soubory ve složce?"), true));
            subMenu.Items.Add(this.CreateMenuItem(subMenu, fileBrowserRow, FileBrowserEventArgs.RequestTypes.OPEN_DIR_IN_EXPLORER, "Otevřít složku ve Windows", null, true));
            subMenu.Items.Add(this.CreateMenuItem(subMenu, fileBrowserRow, FileBrowserEventArgs.RequestTypes.DELETE_DIR, "Smazat složku a její obsah", this.GetConfirmationMethod("Opravdu chcete smazat vybranou složku a veškerý její obsah? Akci není možné vrátit.", "Smazat vybranou složku?"), true));
            subMenu.Items.Add(this.CreateMenuItem(subMenu, fileBrowserRow, FileBrowserEventArgs.RequestTypes.RENAME_DIR, "Přejmenovat složku", null, true));

            return subMenu;
        }
    }
}
 