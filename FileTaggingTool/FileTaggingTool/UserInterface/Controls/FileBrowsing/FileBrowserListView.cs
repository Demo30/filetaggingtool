﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Demos.DemosHelpers;
using Demos.FileTagger;

namespace Demos.FileTaggingTool.UserInterface.Controls.FileBrowsing
{
    public class FileBrowserListView : ListView
    {
        public event EventHandler<FileBrowserEventArgs> FileBrowserRequest = null;
        public event EventHandler<EventArgs> UpdateVisualsRequest = null;
        public bool IncludeSubdirectories { get; set; } = false;
        public bool OnlyTaglessFiles { get; set; } = false;
        public string CurrentFilesFilterRelativePath
        {
            get => FileTagger.ORM.File.CleanPath(_currentFilesFilterRelativePath);
            set => _currentFilesFilterRelativePath = value;
        }
        public DirectoryInfo CurrentFilterDirectory
        {
            get
            {
                var path = $"{_managerInstance.OpenedLibraryUserDataDirPath}\\{_currentFilesFilterRelativePath}";
                return new DirectoryInfo(path);
            }
        }
        public Action ChangeDirectoryAction { get; set; } = null;

        private readonly Manager _managerInstance = Manager.Instance;
        private string _currentFilesFilterRelativePath = string.Empty;
        private IEnumerable<FileTagger.ORM.File> _filesToShow = Array.Empty<FileTagger.ORM.File>();

        private FileTagger.ORM.File[] SelectedFiles
        {
            get
            {
                var list = new List<FileTagger.ORM.File>();
                foreach (var row in SelectedItems)
                {
                    if (row is not FilesBrowserFileRow fileRow)
                    {
                        return Array.Empty<FileTagger.ORM.File>();
                    }

                    list.Add(fileRow.FileModel);
                }
                return list.ToArray();
            }
        }
        private readonly FilesBrowserAsyncThumbnailRenderer _thumbnailRenderer = new();

        public FileBrowserListView()
        {
            Padding = new Thickness(0, 12, 0, 0);
            BindEvents();
        }

        public ScrollViewer GetScrollViewer()
        {
            var border = System.Windows.Media.VisualTreeHelper.GetChild(this, 0);
            if (border is Border)
            {
                return (ScrollViewer)System.Windows.Media.VisualTreeHelper.GetChild(border, 0);
            }

            return null;
        }

        private void BindEvents()
        {
            SetupDropEvents();
            SetupScrollEvents();
            PreviewMouseRightButtonDown += (sender, args) => { ContextMenu = GetFileBrowserListViewContextMenu(); };
        }

        private void SetupScrollEvents()
        {
            Loaded += (sender, e) =>
            {
                var scrollViewer = GetScrollViewer();
                if (scrollViewer == null) return;
                if (scrollViewer.Template.FindName("PART_VerticalScrollBar", scrollViewer) is ScrollBar scrollBar)
                {
                    scrollBar.ValueChanged += (scrollbar, args) =>
                    {
                        UpdateThumbnails();
                    };
                }
                UpdateThumbnails();
            };
        }

        private void UpdateThumbnails()
        {
            const int flex = 5;
            var scrollViewer = GetScrollViewer();
            var start = (int)scrollViewer.VerticalOffset - flex;
            var count = (int)scrollViewer.ViewportHeight + flex;

            if (Items.Count <= 0) return;
            _thumbnailRenderer.ClearQueue();

            start = (int)GeneralHelperClass.Clamp(start, 0, Items.Count);
            count = (int)GeneralHelperClass.Clamp(start + count, 0, Items.Count - start);

            for (var i = start; i < start + count; i++)
            {
                var item = Items.GetItemAt(i);
                if (item is FilesBrowserFileRow fr)
                {
                    _thumbnailRenderer.AddFileBrowserRowToRender(fr);
                }
            }
        }

        private ContextMenu GetFileBrowserListViewContextMenu()
        {
            var subMenu = new ContextMenu
            {
                PlacementTarget = this
            };

            subMenu.Items.Add(CreateContextMenuItem("Vytvořit složku", new FileBrowserEventArgs(CurrentFilterDirectory, false, FileBrowserEventArgs.RequestTypes.CREATE_DIR)));
            subMenu.Items.Add(CreateContextMenuItem("Otevřít složku ve Windows", new FileBrowserEventArgs(CurrentFilterDirectory, false, FileBrowserEventArgs.RequestTypes.OPEN_DIR_IN_EXPLORER)));
            subMenu.Items.Add(CreateContextMenuItem("Zpracovat soubory ve složce", new FileBrowserEventArgs(CurrentFilterDirectory, false, FileBrowserEventArgs.RequestTypes.PROCESS_FILES_IN_FOLDER)));

            return subMenu;
        }

        private MenuItem CreateContextMenuItem(string header, FileBrowserEventArgs args)
        {
            var item = new MenuItem { Header = header };
            item.Click += (sender, arguments) =>
            {
                OnFileRequest(this, args);
                RaiseUpdateVisualsRequestEvent();
            };
            return item;
        }

        public int UpdateFileTree(IEnumerable<FileTagger.ORM.File> filesToShow)
        {
            _filesToShow = filesToShow ?? Array.Empty<FileTagger.ORM.File>();

            var items = new List<FilesBrowserRow>();

            var pathToCurrentFolder = string.Concat(_managerInstance.OpenedLibraryUserDataDirPath, '\\', _currentFilesFilterRelativePath);
            var curDir = Directory.Exists(pathToCurrentFolder) ? new DirectoryInfo(pathToCurrentFolder) : new DirectoryInfo(_managerInstance.OpenedLibraryUserDataDirPath);

            var subDirs = curDir.GetDirectories();

            void DirButtonClickMethod(DirectoryInfo dir)
            {
                _currentFilesFilterRelativePath = GetRelativePath(dir.FullName);
                ChangeDirectoryAction?.Invoke();
            }
            
            foreach (var t in subDirs)
            {
                FilesBrowserRow dirRow = new FilesBrowserDirRow(t, DirButtonClickMethod);
                dirRow.FileRequest += OnFileRequest;
                items.Add(dirRow);
            }

            var files = OnlyTaglessFiles
                ? _managerInstance.GetFilesWithoutTags(true, _currentFilesFilterRelativePath, IncludeSubdirectories).ToList()
                : _filesToShow.ToList();
            
            // TODO TW: This should probably get optimized - multiple filtering is happening here and possibly its evaluation could be moved up the pipeline chain
            foreach (var currentFile in files)
            {
                string CustomNameMethod(FileTagger.ORM.File file) => file.GetFileName(false, true);

                FilesBrowserRow viewModel = new FilesBrowserFileRow(currentFile, CustomNameMethod);
                viewModel.FileRequest += OnFileRequest;

                items.Add(viewModel);
            }

            var ordered = items.OrderBy(GetOrderbyString).ToList();
            
            ItemsSource = ordered;
            try { UpdateThumbnails(); }
            catch
            {
                // ignored
            }

            return ordered.Count(r => r.RowType == FilesBrowserRow.RowTypes.FileRow);
        }

        private void OnFileRequest(object sender, FileBrowserEventArgs args)
        {
            if (args == null || args.Request == FileBrowserEventArgs.RequestTypes.NO_REQUEST) { return; }

            switch (args.Request)
            {
                case FileBrowserEventArgs.RequestTypes.DELETE_FILE: DeleteFile(args.File); break;
                case FileBrowserEventArgs.RequestTypes.DELETE_ALL_SELECTED_FILES: DeleteFile(); break;
                case FileBrowserEventArgs.RequestTypes.OPEN_FILE_IN_EXPLORER: OpenFilesInDirectory(SelectedFiles); break;
                case FileBrowserEventArgs.RequestTypes.OPEN_FILE: RunFileUsingWindowsAttachedApplication(args.File); break;
                case FileBrowserEventArgs.RequestTypes.PROCESS_FILES_IN_FOLDER: ProcessFilesInFolder(args.Directory); break;
                case FileBrowserEventArgs.RequestTypes.RENAME_FILE: if (FileBrowserRequest != null) { try { FileBrowserRequest(this, args); } catch { } } break;
                case FileBrowserEventArgs.RequestTypes.OPEN_DIR_IN_EXPLORER: OpenDirectoryInWindows(args.Directory); break;
                case FileBrowserEventArgs.RequestTypes.DELETE_DIR: DeleteDirectory(args.Directory); break;
                case FileBrowserEventArgs.RequestTypes.CREATE_DIR: if (FileBrowserRequest != null) { try { FileBrowserRequest(this, args); } catch { } } break;
                case FileBrowserEventArgs.RequestTypes.RENAME_DIR: if (FileBrowserRequest != null) { try { FileBrowserRequest(this, args); } catch { } } break;
                
                case FileBrowserEventArgs.RequestTypes.NO_REQUEST:
                default:
                    throw new InvalidOperationException($"No implementation for this type of Request: {args.Request.ToString()}");
            }
        }

        private void ProcessFilesInFolder(DirectoryInfo directory)
        {
            var set = new FileEvaluationSettings
            {
                FileSelector = new FileDirectorySelector(directory)
            };
            _managerInstance.ProcessFiles(set);

            RaiseUpdateVisualsRequestEvent();
        }

        private void DeleteFile(FileTagger.ORM.File file = null)
        {
            var filesToDelete = file != null ? new [] { file } : SelectedFiles;

            var fileInfosToDelete = new List<FileInfo>();
            foreach (var curFile in filesToDelete)
            {
                var filePath = curFile.GetFullFinalFilepath(_managerInstance.OpenedLibraryUserDataDirPath);
                if (!File.Exists(filePath))
                {
                    MessageBox.Show(string.Format(UserMessages.FileNotFound, filePath));
                    return;
                }

                fileInfosToDelete.Add(new FileInfo(filePath));
            }

            foreach(var curFi in fileInfosToDelete)
            {
                File.Delete(curFi.FullName);
            }
            
            _managerInstance.DeleteFiles(filesToDelete);

            RaiseUpdateVisualsRequestEvent();
        }

        private void RunFileUsingWindowsAttachedApplication(FileTagger.ORM.File file)
        {
            var filePath = file.GetFullFinalFilepath(_managerInstance.OpenedLibraryUserDataDirPath);

            if (!File.Exists(filePath))
            {
                MessageBox.Show(string.Format(UserMessages.FileNotFound, filePath));
                return;
            }

            System.Diagnostics.Process.Start(filePath);
        }


        private void OpenFilesInDirectory(FileTagger.ORM.File[] files)
        {
            var filePaths = new List<string>();

            foreach (var t in files)
            {
                var fp = t.GetFullFinalFilepath(_managerInstance.OpenedLibraryUserDataDirPath);

                if (!File.Exists(fp))
                {
                    MessageBox.Show(string.Format(UserMessages.FileNotFound, fp));
                    return;
                }

                filePaths.Add(fp);
            }

            var success = false;

            try // try opening folder with selected files using the Shell API method
            {
                GeneralHelperClass.OpenFolderAndSelectFile(filePaths.ToArray());
                success = true;
            }
            catch
            {
                // ignored
            }

            if (success == false) // if the previous failed, try opening it with the wrapper methods (often does not select files on first try...)
            {
                try
                {
                    var explorerFullPath = GeneralHelperClass.GetFullPath("explorer.exe");
                    var argument = "/select, \"" + filePaths[0] + "\"";
                    System.Diagnostics.Process.Start(explorerFullPath, argument);

                    success = true;
                }
                catch
                {
                    // ignored
                }
            }

            if (success) return;
            
            var firstFile = files[0];
            var firstFilePath = firstFile.GetFullFinalFilepath(_managerInstance.OpenedLibraryUserDataDirPath);
            var folderPath = firstFilePath.Replace(firstFile.GetFileName(true, true), "");
            System.Diagnostics.Process.Start(folderPath);
        }

        private void OpenDirectoryInWindows(DirectoryInfo directory)
        {
            var dirPath = directory.FullName;

            if (!Directory.Exists(dirPath))
            {
                MessageBox.Show(string.Format(UserMessages.DirectoryNotFound, dirPath));
                return;
            }

            System.Diagnostics.Process.Start(dirPath);
        }

        private void DeleteDirectory(DirectoryInfo directory)
        {
            var dirPath = directory.FullName;

            if (!Directory.Exists(dirPath))
            {
                MessageBox.Show(string.Format(UserMessages.DirectoryNotFound, dirPath));
                return;
            }

            try
            {
                Directory.Delete(dirPath, true);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Došlo k chybě při pokusu o smazání složky ({dirPath}). Ujistěte se, že nemáte otevřený některý ze souborů ve složce, kterou se pokoušíte smazat.", "Chyba při pokusu o smazání složky");
                Services.FileTaggingToolLoggingManager.LogMessage(Logging.MessageTypes.ERROR_INTERNAL, ex.Message, ex.StackTrace);
            }

            RaiseUpdateVisualsRequestEvent();
        }

        private string GetOrderbyString(FilesBrowserRow row)
        {
            var orderNum = 0;
            switch (row.RowType)
            {
                case FilesBrowserRow.RowTypes.DirectoryRow:
                    orderNum = 1;
                    break;
                case FilesBrowserRow.RowTypes.FileRow:
                    orderNum = 2;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return string.Concat(orderNum, row.CustomName);
        }

        private string GetRelativePath(string fullPath)
        {
            var userDataPath = _managerInstance.OpenedLibraryUserDataDirPath;

            var userDataPathIndex = fullPath.IndexOf(userDataPath);
            var relativePath = fullPath.Remove(0, userDataPathIndex + userDataPath.Length + 1);

            return relativePath;
        }

        private void SetupDropEvents()
        {
            AllowDrop = true;
            Drop += (sender, e) =>
            {
                try
                {
                    var formats = e.Data.GetFormats();
                    if (formats.Contains(DataFormats.FileDrop))
                    {
                        var data = e.Data.GetData(DataFormats.FileDrop);
                        var currFolder = CurrentFilterDirectory;
                        if (data is string[] paths && currFolder.Exists)
                        {
                            CommonGUILogic.MoveContentToLibrary(currFolder, paths, true);
                        }
                        else
                        {
                            throw new Exception("File drop failed. Unexpected data or destination folder does not exist.");
                        }

                        // Process newly added files
                        var sett = new FileEvaluationSettings()
                        {
                            FileSelector = new FileDirectorySelector(CurrentFilterDirectory)
                        };
                        _managerInstance.ProcessFiles(sett);
                    }
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Při zpracování vkládaného obsahu došlo k chybě. Pokud problémy přetrvávají, obraťte se na vývojáře aplikace.", "Chyba při zpracování vkládaného obsahu.");
                    Services.FileTaggingToolLoggingManager.LogMessage(Logging.MessageTypes.ERROR_INTERNAL, ex.Message, ex.StackTrace);
                }

                RaiseUpdateVisualsRequestEvent();
            };
        }

        private void RaiseUpdateVisualsRequestEvent()
        {
            if (UpdateVisualsRequest == null) return;
            try {
                UpdateVisualsRequest(this, EventArgs.Empty);
            }
            catch
            {
                // ignored
            }
        } 
    }
}
