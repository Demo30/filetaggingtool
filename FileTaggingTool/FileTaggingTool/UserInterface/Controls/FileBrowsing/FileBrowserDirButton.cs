﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Demos.FileTaggingTool.UserInterface.Controls.FileBrowsing
{
    public class FileBrowserDirButton : UserControl
    {
        public event EventHandler<EventArgs> Click = null;

        public FileBrowserDirButton()
        {
            this.MouseDown += FileBrowserDirButton_MouseDown;
            this.MouseEnter += this.OnMouseEnter;
            this.MouseLeave += this.OnMouseLeave;
            this.VerticalContentAlignment = VerticalAlignment.Center;
            this.HorizontalContentAlignment = HorizontalAlignment.Center;
            this.Margin = new Thickness(2);
            this.Padding = new Thickness(5, 3, 5, 3);
            this.Width = 42;
            this.Height = 37;
            this.Content = new Image() { Source = DemosHelpers.GeneralHelperClass.GetImageSourceFromString(Demos.FileTaggingTool.Resources.InternalResourceMapper.ImageResource + "folderIcon.png") };
        }

        private void FileBrowserDirButton_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (this.Click != null) { try { this.Click(this, EventArgs.Empty); } catch { } }
        }

        private void OnMouseEnter(object sender, System.Windows.Input.MouseEventArgs args)
        {
            this.Background = GUICommons.CommonBrushes_Blue;
            //System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
        }

        private void OnMouseLeave(object sender, System.Windows.Input.MouseEventArgs args)
        {
            this.Background = new SolidColorBrush(Colors.Transparent);
            //System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;

        }
    }
}
