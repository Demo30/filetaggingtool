﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using File = Demos.FileTagger.ORM.File;

namespace Demos.FileTaggingTool.UserInterface.Controls.FileBrowsing
{
    internal class FilesBrowserFileRow : FilesBrowserRow
    {
        public File FileModel { get; protected set; }
        public string FileExtensionLowercase
        {
            get { return RowType == RowTypes.FileRow ? FileModel.FileExtension.ToLower() : String.Empty; }
        }
        public bool ThumbnailRendered { get; private set; }
        private System.Drawing.Image _thumbImage = null;
        private const int ThumbnailSize = 80;

        public FilesBrowserFileRow(File file, GetName customMethod = null) : base()
        {
            if (file == null) { throw new ArgumentNullException(); }
            RowType = RowTypes.FileRow;

            GetNameMethod = customMethod;
            FileModel = file;

            PreviewMouseRightButtonDown += (sender, args) => { ContextMenu = GetContextMenu(this); };

            InitializeVisual();
        }

        protected override void InitializeVisual()
        {
            Background = null;
            ImageSource imageSource = null;
            try
            {
                imageSource = Demos.DemosHelpers.GeneralHelperClass.GetImageSourceFromString(Demos.FileTaggingTool.Resources.InternalResourceMapper.ImageResource + "placeholder.png");
            }
            catch { }

            if (imageSource != null)
            {
                Image img = new Image()
                {
                    Source = imageSource,
                    Height = FilesBrowserFileRow.ThumbnailSize,
                    Width = FilesBrowserFileRow.ThumbnailSize * 1.2,
                    Margin = new Thickness(0,0,10,0)
                };
                _mainContent.Children.Add(img);
            }

            _mainContent.Children.Add(new TextBlock() { Text = CustomName });
        }

        public void PrepareThumbnail()
        {
            if (!ThumbnailRendered)
            {
                try
                {
                    string imageFullPath = FileModel.GetFullFinalFilepath(FileTagger.Manager.Instance.OpenedLibraryUserDataDirPath);

                    Func<bool> someCallback = new Func<bool>(() => { return false; });
                    System.Drawing.Image.GetThumbnailImageAbort myCallback = new System.Drawing.Image.GetThumbnailImageAbort(someCallback);
                    System.Drawing.Image imgDr = System.Drawing.Image.FromFile(imageFullPath);
                    imgDr = FixedSize(imgDr, (int)(FilesBrowserFileRow.ThumbnailSize * 1.2), FilesBrowserFileRow.ThumbnailSize);
                    System.Drawing.Image thumb = imgDr.GetThumbnailImage((int)(FilesBrowserFileRow.ThumbnailSize * 1.2), FilesBrowserFileRow.ThumbnailSize, myCallback, IntPtr.Zero);
                    _thumbImage = thumb;
                }
                catch { }
            } else
            {
                _thumbImage = null; // cleanup, not really the correct place to do this
            }
        }

        public void RenderThumbnail()
        {
            if (!ThumbnailRendered && _thumbImage != null)
            {
                Image imageElement = null;
                foreach (var element in _mainContent.Children)
                {
                    if (element is Image)
                    {
                        imageElement = (Image)element;
                        imageElement.Source = ToWpfImage(_thumbImage);
                        ThumbnailRendered = true;
                        break;
                    }
                }
            }
        }

        // probably move from here...to DemosHelpers maybe?
        private static System.Windows.Media.Imaging.BitmapImage ToWpfImage(System.Drawing.Image img)
        {
            System.IO.MemoryStream ms = new System.IO.MemoryStream();  // no using here! BitmapImage will dispose the stream after loading
            img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);

            System.Windows.Media.Imaging.BitmapImage ix = new System.Windows.Media.Imaging.BitmapImage();
            ix.BeginInit();
            ix.CacheOption = System.Windows.Media.Imaging.BitmapCacheOption.OnLoad;
            ix.StreamSource = ms;
            ix.EndInit();
            return ix;
        }

        private static System.Drawing.Image FixedSize(System.Drawing.Image imgPhoto, int Width, int Height)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)Width / (float)sourceWidth);
            nPercentH = ((float)Height / (float)sourceHeight);
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = System.Convert.ToInt16((Width -
                              (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = System.Convert.ToInt16((Height -
                              (sourceHeight * nPercent)) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            System.Drawing.Bitmap bmPhoto = new System.Drawing.Bitmap(Width, Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                             imgPhoto.VerticalResolution);

            System.Drawing.Graphics grPhoto = System.Drawing.Graphics.FromImage(bmPhoto);
            grPhoto.Clear(System.Drawing.Color.Transparent);
            grPhoto.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new System.Drawing.Rectangle(destX, destY, destWidth, destHeight),
                new System.Drawing.Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                System.Drawing.GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }

        protected override string GetCustomName() {
            return GetNameMethod(FileModel);
        }

        protected override FileBrowserEventArgs GetRequestArgs(FileBrowserEventArgs.RequestTypes type)
        {
            return new FileBrowserEventArgs(FileModel, type);
        }

        protected override void OnMouseOverRow(object sender, MouseEventArgs args)
        {
            var row = sender as FilesBrowserRow;
            if (row != null && args.LeftButton == MouseButtonState.Pressed)
            {
                Point currentPoint = Mouse.GetPosition(this);
                var diff =
                    Math.Abs(currentPoint.X - DragMouseCoordination.MousePrevX) >= FilesBrowserRowDragMouseCoordinator.MouseMoveEpsilonX ||
                    Math.Abs(currentPoint.Y - DragMouseCoordination.MousePrevY) >= FilesBrowserRowDragMouseCoordinator.MouseMoveEpsilonY;
                if (!diff) return;
                
                var dat = new FileBrowserRowDragData(this);
                DragDrop.DoDragDrop(this, dat, DragDropEffects.Move);
            }
        }

        protected override void OnFileBrowserRowDrop(FileBrowserRowDragData passedData, FilesBrowserRow targetRow)
        {
            return;
        }

        private ContextMenu GetContextMenu(FilesBrowserRow fileBrowserRow)
        {
            if (fileBrowserRow == null) { throw new ArgumentNullException(); }

            var subMenu = new ContextMenu();
            subMenu.PlacementTarget = fileBrowserRow;

            subMenu.Items.Add(CreateMenuItem(subMenu, fileBrowserRow, FileBrowserEventArgs.RequestTypes.RENAME_FILE, "Přejmenovat soubor"));
            subMenu.Items.Add(CreateMenuItem(subMenu, fileBrowserRow, FileBrowserEventArgs.RequestTypes.DELETE_FILE, "Odstranit soubor", GetConfirmationMethod("Opravdu odstranit soubor?", "Odstranění souboru")));
            subMenu.Items.Add(CreateMenuItem(subMenu, fileBrowserRow, FileBrowserEventArgs.RequestTypes.DELETE_ALL_SELECTED_FILES, "Odstranit všechny vybrané soubory", GetConfirmationMethod("Opravdu odstranit všechny vybrané soubory?", "Hromadné odstranění souborů")));
            subMenu.Items.Add(CreateMenuItem(subMenu, fileBrowserRow, FileBrowserEventArgs.RequestTypes.OPEN_FILE, "Otevřít soubor"));
            subMenu.Items.Add(CreateMenuItem(subMenu, fileBrowserRow, FileBrowserEventArgs.RequestTypes.OPEN_FILE_IN_EXPLORER, "Otevřít umístění souboru"));


            return subMenu;
        }
    }
}
 