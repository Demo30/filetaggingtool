﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Demos.FileTaggingTool.UserInterface.Controls.FileBrowsing
{
    class FilesBrowserAsyncThumbnailRenderer
    {
        private List<FilesBrowserFileRow> _fileRowsToRender = new List<FilesBrowserFileRow>();

        private BackgroundWorker _backgroundWorker = new BackgroundWorker();

        public FilesBrowserAsyncThumbnailRenderer()
        {
            this._backgroundWorker.DoWork += _backgroundWorker_DoWork;
        }

        private void _backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            while(this._fileRowsToRender.Count > 0)
            {
                FilesBrowserFileRow fr = this._fileRowsToRender.ElementAt(0);
                this._fileRowsToRender.Remove(fr);
                fr.PrepareThumbnail();
                App.Current.Dispatcher.Invoke(new Action(() => {
                    fr.RenderThumbnail();
                }));
            }
        }

        public void AddFileBrowserRowToRender(FilesBrowserFileRow fileRow)
        {
            if (!this._fileRowsToRender.Contains(fileRow))
            {
                this._fileRowsToRender.Add(fileRow);
                if (!this._backgroundWorker.IsBusy)
                {
                    this._backgroundWorker.RunWorkerAsync();
                }
            }
        }

        public void ClearQueue()
        {
            this._fileRowsToRender.Clear();
        }
    }
}
