﻿namespace Demos.FileTaggingTool.UserInterface.Controls.FileBrowsing;

public class FilesBrowserRowDragMouseCoordinator
{
    public const double MouseMoveEpsilonX = 6;
    public const double MouseMoveEpsilonY = 5;

    public double MousePrevX = 0;
    public double MousePrevY = 0;
}