﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using File = Demos.FileTagger.ORM.File;

namespace Demos.FileTaggingTool.UserInterface.Controls.FileBrowsing
{
    internal abstract class FilesBrowserRow : ListViewItem
    {
        internal enum RowTypes
        {
            FileRow,
            DirectoryRow
        }

        public static FilesBrowserRowDragMouseCoordinator DragMouseCoordination = new FilesBrowserRowDragMouseCoordinator();

        public event EventHandler<FileBrowserEventArgs> FileRequest = null;

        public RowTypes RowType { get; protected set; }
        public delegate string GetName(File file);
        public GetName GetNameMethod
        {
            get { if (this._getNameMethod == null) { return ((file) => { return file.ToString(); }); } else { return this._getNameMethod; } }
            set { this._getNameMethod = value; }
        }
        public string CustomName { get { return this.GetCustomName(); } }

        private GetName _getNameMethod = null;

        protected StackPanel _mainContent = new StackPanel();

        public FilesBrowserRow()
        {
            this.InitializeCommons();
        }

        private void InitializeCommons()
        {
            this._mainContent.HorizontalAlignment = HorizontalAlignment.Stretch;
            this.Content = this._mainContent;
            this.MouseMove += this.OnMouseOverRow;
            this.Drop += this.OnFileBrowserRowDropCore;
            this._mainContent.Orientation = Orientation.Horizontal;
            this.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            this.PreviewMouseLeftButtonDown += this.RowLeftButton;
        }

        protected abstract void InitializeVisual();

        protected abstract string GetCustomName();

        private void RowLeftButton(object sender, MouseButtonEventArgs args)
        {
            Point k = Mouse.GetPosition(this);
            FilesBrowserRow.DragMouseCoordination.MousePrevX = k.X;
            FilesBrowserRow.DragMouseCoordination.MousePrevY = k.Y;
        }

        protected MenuItem CreateMenuItem(ContextMenu cMenu, FilesBrowserRow fileBrowserRow, FileBrowserEventArgs.RequestTypes type, string title, Func<bool> confirmation = null, bool isDirRow = false)
        {
            MenuItem item = new MenuItem() { Header = title };
            item.Click += (object sender, RoutedEventArgs args) =>
            {
                if (confirmation == null || confirmation() == true)
                {
                    fileBrowserRow.SendRequest(type);
                    cMenu.IsOpen = false;
                }
            };

            return item;
        }

        protected Func<bool> GetConfirmationMethod(string message, string title)
        {
            Func<bool> fc = () =>
            {
                MessageBoxResult res = MessageBox.Show(message, title, MessageBoxButton.OKCancel);

                if (res == MessageBoxResult.Yes || res == MessageBoxResult.OK) { return true; }
                else { return false; }
            };

            return fc;
        }

        protected void SendRequest(FileBrowserEventArgs.RequestTypes type)
        {
            if (this.FileRequest != null)
            {
                try
                {
                    FileBrowserEventArgs args = this.GetRequestArgs(type);
                    if (args != null)
                    {
                        this.FileRequest(this, args);
                    }
                }
                catch { }
            }
        }

        protected abstract FileBrowserEventArgs GetRequestArgs(FileBrowserEventArgs.RequestTypes type);

        protected abstract void OnMouseOverRow(object sender, MouseEventArgs args);

        private void OnFileBrowserRowDropCore(object sender, DragEventArgs e)
        {
            FilesBrowserRow targetRow = sender as FilesBrowserRow;
            FileBrowserRowDragData passedData = FileBrowserRowDragData.GetDropDataFromDragEventArgs(e);

            if (passedData is FileBrowserRowDragData && targetRow is FilesBrowserRow)
            {
                if (passedData.Sender.Equals(targetRow))
                {
                    return;
                }

                this.OnFileBrowserRowDrop(passedData, targetRow);
            }
            else
            {
                return;
            }
        }

        protected abstract void OnFileBrowserRowDrop(FileBrowserRowDragData passedData, FilesBrowserRow targetRow);
    }
}
 