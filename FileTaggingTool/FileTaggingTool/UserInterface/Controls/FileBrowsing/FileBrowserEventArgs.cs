﻿using System;
using Demos.FileTagger.ORM;

namespace Demos.FileTaggingTool.UserInterface.Controls.FileBrowsing
{
    public class FileBrowserEventArgs : EventArgs
    {
        public enum RequestTypes
        {
            NO_REQUEST,
            DELETE_FILE,
            DELETE_ALL_SELECTED_FILES,
            OPEN_FILE,
            OPEN_FILE_IN_EXPLORER,
            PROCESS_FILES_IN_FOLDER,
            RENAME_FILE,
            OPEN_DIR_IN_EXPLORER,
            DELETE_DIR,
            CREATE_DIR,
            RENAME_DIR
        }

        public File File { get; }
        public System.IO.DirectoryInfo Directory { get; }
        public RequestTypes Request { get; private set; }
        public bool IsDirectoryRow { get; private set; }
        public bool IsVoidSpace { get; private set; }


        public FileBrowserEventArgs(File file, RequestTypes request = RequestTypes.NO_REQUEST)
        {
            if (file == null) { throw new ArgumentNullException(); }

            this.IsDirectoryRow = false;
            this.Request = request;
            this.File = file;
        }

        public FileBrowserEventArgs(System.IO.DirectoryInfo directory, bool isDirectoryRow, RequestTypes request = RequestTypes.NO_REQUEST)
        {
            if (directory == null) { throw new ArgumentNullException(); }

            this.IsDirectoryRow = isDirectoryRow;
            this.Request = request;
            this.Directory = directory;
        }
    }
}
