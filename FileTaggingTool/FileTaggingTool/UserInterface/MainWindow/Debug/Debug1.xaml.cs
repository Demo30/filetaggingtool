﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Reflection;
using System.IO;
using Demos.DemosHelpers;
using System.Resources;
using System.Globalization;
using System.Collections;


namespace Demos.FileTaggingTool.UserInterface
{

    

    /// <summary>
    /// Interaction logic for Debug1.xaml
    /// </summary>
    public partial class Debug1 : Page
    {
        

        public Debug1()
        {
            InitializeComponent();
        }

        private void btn_debug1_click(object sender, RoutedEventArgs args)
        {
            //var kk = FileTaggingTool.Services.AppConfigurationManager.GetIdentifierStrategy();

            //FileTaggingTool.Services.AppConfigurationManager.SetIdentifierStrategy(new FileTagger.IdentifierStrategyNo2());

            //var oo = FileTaggingTool.Services.AppConfigurationManager.GetIdentifierStrategy();

            List<DirectoryInfo> dirs = new List<DirectoryInfo>();
            dirs.Add(new DirectoryInfo(@"C:\\Parent\\\\"));
            dirs.Add(new DirectoryInfo(@"C:\\Parent\A\AA\AAA"));
            dirs.Add(new DirectoryInfo(@"C:\\Parent\A\AA\AAB"));
            dirs.Add(new DirectoryInfo(@"C:\\Parent\A\AA\AAC"));
            dirs.Add(new DirectoryInfo(@"C:\\Parent\A\AA\BAA"));
            //dirs.Add(new DirectoryInfo(@"C:\\Parent\C"));

        }

    }
}
