﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Demos.FileTaggingTool.UserInterface
{
    public partial class TextPrompt : Window
    {
        public const string BUTTON_OK = "btn_OK";
        public const string BUTTON_CANCEL = "btn_CANCEL";

        public Action NoTextAction = null;
        public Action<string> SuccessAction = null;

        public Color DefaultTextColor { get; set; } = Colors.Black;

        private Color PromptDefaultTextColor { get; } = Colors.Gray;
        private bool TreatFilledPromptTextAsOverwritableDefault { get; }
        private string FilledPromptText = "";


        public TextPrompt(string prefilledText = null, bool treatPrefilledTextAsOverwritableDefault = false)
        {
            InitializeComponent();

            this.TreatFilledPromptTextAsOverwritableDefault = treatPrefilledTextAsOverwritableDefault;
            this.txtbx_prompt.Text = prefilledText;
        }

        private bool _OkButtonPressed = false;

        public void Initialize(string windowTitle, string PromptDescription, Action noTextAction, Action<string> successAction)
        {
            this.Title = windowTitle;
            this.WindowTitle.Content = windowTitle;
            this.lbl_Desc.Content = PromptDescription;
            this.NoTextAction = noTextAction;
            this.SuccessAction = successAction;

            if (this.TreatFilledPromptTextAsOverwritableDefault)
            {
                this.txtbx_prompt.Foreground = new SolidColorBrush(this.PromptDefaultTextColor);
            } else
            {
                if (!String.IsNullOrEmpty(this.txtbx_prompt.Text))
                {
                    this.txtbx_prompt.CaretIndex = this.txtbx_prompt.Text.Length;
                }
            }

            this.BindEvents();

            this.txtbx_prompt.Focus();
        }

        private void BindEvents()
        {
            this.Closed += (object sender, EventArgs args) =>
            {
                if (this._OkButtonPressed)
                {
                    if (this.SuccessAction != null) { try { this.SuccessAction(this.FilledPromptText); } catch { } }
                }
            };

            this.txtbx_prompt.TextChanged += this.TextInputAction;
        }

        #region  Core main window GUI events

        private void setDefaultWindowBorder()
        {
            SetterBaseCollection GeneralWindowStyleSetters = ((Style)FileTaggingToolStatic.FileTaggingToolAppInstance.Resources["GeneralWindowStyle"]).Setters;
            Thickness CustomStyleDefinedBorderThickness = new Thickness();
            foreach (Setter Setter in GeneralWindowStyleSetters)
            {
                if (Setter.Property.Name == "BorderThickness")
                {
                    CustomStyleDefinedBorderThickness = (Thickness)Setter.Value;
                }
            }
            if (CustomStyleDefinedBorderThickness != default(Thickness))
            {
                this.BorderThickness = CustomStyleDefinedBorderThickness;
            }
        }

        private void DragWindow_LeftButtonDown(object sender, MouseButtonEventArgs args)
        {
            this.DragMove();
        }

        private void Close_Click(object sender, RoutedEventArgs args)
        {
            this.Close();
        }

        private void TopBarButtons_Enter(object sender, RoutedEventArgs args)
        {
            ((Grid)sender).Background = new SolidColorBrush(Color.FromRgb(0xEB, 0xEF, 0xF0));
        }

        private void TopBarButtons_Leave(object sender, RoutedEventArgs args)
        {
            ((Grid)sender).Background = new SolidColorBrush(Colors.Transparent);
        }

        #endregion

        private void TextInputAction(object sender, TextChangedEventArgs args)
        {
            if (this.TreatFilledPromptTextAsOverwritableDefault)
            {
                this.txtbx_prompt.Text = this.txtbx_prompt.Text.Substring(0, 1);
                this.txtbx_prompt.CaretIndex = 1;
                this.txtbx_prompt.Foreground = new SolidColorBrush(this.DefaultTextColor);
            }

            this.txtbx_prompt.TextChanged -= this.TextInputAction;
        }

        private void btn_click(object sender, RoutedEventArgs args)
        {
            Button btn = (Button)sender;

            string promptText = this.txtbx_prompt.Text;
            switch (btn.Name)
            {
                case TextPrompt.BUTTON_OK:
                    if (String.IsNullOrEmpty(promptText))
                    {
                        if (this.NoTextAction != null)
                        {
                            this.NoTextAction();
                        }
                        return;
                    }
                    else
                    {
                        this._OkButtonPressed = true;
                        this.FilledPromptText = promptText;
                        this.Close();
                    }
                    break;
                case TextPrompt.BUTTON_CANCEL:
                    this._OkButtonPressed = false;
                    this.Close();
                    break;
                default: throw new InvalidProgramException();
            }
        }
    }
}
