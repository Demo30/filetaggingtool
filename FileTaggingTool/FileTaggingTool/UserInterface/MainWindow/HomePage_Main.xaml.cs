﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Forms = System.Windows.Forms;
using System.IO;
using Demos.FileTagger;
using Demos.FileTagger.ORM;
using Demos.DemosHelpers;
using System.ComponentModel;
using System.Security;
using System.Timers;
using Demos.FileTagger.SupportStructs;
using Demos.FileTagger.Utils;
using Demos.FileTaggingTool.Services;
using Demos.FileTaggingTool.UserInterface.Controls.FileBrowsing;
using Demos.FileTaggingTool.UserInterface.Controls.TaggingRelated.AvailableTagsListView;
using Demos.FileTaggingTool.UserInterface.Controls.TaggingRelated.FilteringTagControl;
using File = Demos.FileTagger.ORM.File;

namespace Demos.FileTaggingTool.UserInterface.HomePage;

public partial class HomePage_Main
{
    private static Manager ManagerInstance => Manager.Instance;
    private IEnumerable<FileTagger.ORM.File> ValidFileRecordsMatchingTagFilter => ManagerInstance.GetFilesByTags(
        GetTagIdsFromFilter(),
        true,
        ftc_filteredTags.CurrentFilteringRelationship,
        new PathSearch(lstV_libraryStructure.CurrentFilesFilterRelativePath, lstV_libraryStructure.IncludeSubdirectories),
        ftc_filteredTags.WithTagSubLevels);
    private static Tag[] ValidTags => ManagerInstance.GetTags(true);

    private string CurrentFileTextSearch => txtbx_filesTextSearchFiltering.Text;

    private const int TagSearchWindowMs = 200;
    private const int FileSearchWindowMs = 400;
    private Timer _tagSearchDelayed;
    private Timer _fileSearchDelayed;

    private Tag CurrentlySelectedAvailableTag
    {
        get
        {
            var numOfSelectedRows = lstv_availableTags.SelectedItems.Count;
            if (numOfSelectedRows != 1)
            {
                return null;
            }

            if (lstv_availableTags.SelectedValue is not TagListItem tagListItem)
            {
                throw new InvalidOperationException("Unexpected row type.");
            }

            return tagListItem.TagOrmObject;
        }
    }
    private FileTagger.ORM.File CurrentlySelectedFile
    {
        get
        {
            var row = lstV_libraryStructure.SelectedValue as FilesBrowserFileRow;
            return row?.FileModel;
        }
    }
    private FileTagger.ORM.File[] CurrentlySelectedFileAll
    {
        get
        {
            var files = new List<FileTagger.ORM.File>();
            foreach(var row in lstV_libraryStructure.SelectedItems)
            {
                if (row is FilesBrowserFileRow curFileRow)
                {
                    files.Add(curFileRow.FileModel);
                }
            }

            return files.ToArray();
        }
    }
    private TextPrompt _promptWindow = null;
    private readonly BackgroundWorker _fileProcessingWorker = new();
    private string TagFilterMask => txtbx_tagFiltering.Text;

    public HomePage_Main()
    {
        InitializeComponent();

        BindToEvents();
        PrepareControls();
        TryOpenLatestLibrary();
    }

    private void BindToEvents()
    {
        ManagerInstance.LibraryOpened += OnLibraryOpened;
        _fileProcessingWorker.DoWork += _fileProcessingWorker_DoWork;
        ManagerInstance.FileProcessingProgressUpdate += (_, args2) =>
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (args2.Stage == FileProcessingReportEventArgs.Stages.CompletedByFailure)
                {
                    MessageBox.Show(UserMessages.CompletedByFailure, UserMessages.ErrorCaption);
                }
                else
                {
                    MyProgressBarHelperStructure.comment.Content = GetFileProcessingStageTranslation(args2.Stage);
                    if (args2.Stage == FileProcessingReportEventArgs.Stages.CompletedSuccessfully)
                    {
                        UpdateVisuals();
                    }
                }
            });
        };

        ftc_filteredTags.FilteringRelationshipChanged += FilteredTagRelationSwitcher_StateChanged;
        ftc_filteredTags.FilteredTagsChanged += (_, _) => { UpdateVisuals(); };
    }

    private void PrepareControls()
    {
        PrepareTagSearchDelayed();

        PrepareAvailableTagsListView();
        PrepareLibraryFileStructure();
        PrepareImagePreviewControl();
        PreparePaneResizers();
        PrepareAssignedTagsControls();
    }

    private void PrepareImagePreviewControl()
    {
        var subMenu = new ContextMenu
        {
            PlacementTarget = cc_filePreview
        };

        var toRight = new MenuItem {Header = UserMessages.RotateBy90Degrees };
        toRight.Click += (_, _) => { RotatePreviewedImage(System.Drawing.RotateFlipType.Rotate90FlipNone); };
            
        var toLeft = new MenuItem { Header =  UserMessages.RotateBy270Degrees};
        toLeft.Click += (_, _) => { RotatePreviewedImage(System.Drawing.RotateFlipType.Rotate270FlipNone); };
            
        var flipVertical = new MenuItem { Header =  UserMessages.FlipVertically};
        flipVertical.Click += (_, _) => { RotatePreviewedImage(System.Drawing.RotateFlipType.RotateNoneFlipX); };

        var flipHorizontal = new MenuItem { Header =  UserMessages.FlipHorizontally};
        flipHorizontal.Click += (_, _) => { RotatePreviewedImage(System.Drawing.RotateFlipType.RotateNoneFlipY); };

        subMenu.Items.Add(toRight);
        subMenu.Items.Add(toLeft);
        subMenu.Items.Add(flipVertical);
        subMenu.Items.Add(flipHorizontal);

        cc_filePreview.ContextMenu = subMenu;
    }

    private void PrepareAvailableTagsListView()
    {
        lstv_availableTags.Instantiate(ManagerInstance, () => CurrentlySelectedFileAll);
        lstv_availableTags.StructureChanged += (_, _) => { lstv_availableTags.UpdateTagPresentation(GetAssignedTags(true).Select(t => t.Tag).ToArray(), ValidTags, TagFilterMask); };
        lstv_availableTags.TagListItemChanged += (_, _) => ResetTagSearchDelayWindow();
        lstv_availableTags.TagRequest += OnTagRequest;
    }

    private void PrepareTagSearchDelayed()
    {
        _tagSearchDelayed = new Timer(TagSearchWindowMs);
        _tagSearchDelayed.AutoReset = false;
        _tagSearchDelayed.Elapsed += (_, _) => Application.Current.Dispatcher.Invoke(UpdateTagRelatedGUI);
        
        _fileSearchDelayed = new Timer(FileSearchWindowMs);
        _fileSearchDelayed.AutoReset = false;
        _fileSearchDelayed.Elapsed += (_, _) => Application.Current.Dispatcher.Invoke(UpdateVisuals);
    }

    private void OnTagRequest(object sender, TagEventArgs args)
    {
        switch (args.Request)
        {
            case TagEventArgs.RequestTypes.ADD_TAG_TO_FILTER: Tag_Filtering(AppActionCollectionAlterations.ADD, args.PassedTag); break;
            case TagEventArgs.RequestTypes.DELETE: Tag_RemoveTag(args.PassedTag); break;
            case TagEventArgs.RequestTypes.CREATE_NESTED_TAG: Tag_AddNewTag(args.PassedTag.Id); break;
            case TagEventArgs.RequestTypes.CREATE_ROOT_TAG: Tag_AddNewTag(null); break;
            case TagEventArgs.RequestTypes.DISSOCIATE_TAG_GENERAL:
            {
                if (CurrentlySelectedFile != null)
                {
                    Tag_DissociateTag(args.PassedTag.Id, null);
                }
            }
                break;
            case TagEventArgs.RequestTypes.DISSOCIATE_TAG_PRIMARY:
            {
                if (CurrentlySelectedFile != null)
                {
                    Tag_DissociateTag(args.PassedTag.Id, TagAssignmentType.PrimaryTag);
                }
            }
                break;
            case TagEventArgs.RequestTypes.DISSOCIATE_TAG_SECONDARY:
            {
                if (CurrentlySelectedFile != null)
                {
                    Tag_DissociateTag(args.PassedTag.Id, TagAssignmentType.SecondaryTag);
                }
            }
                break;
            case TagEventArgs.RequestTypes.COLLAPSE_NESTED_NODES: ResetTagSearchDelayWindow(); break;
            case TagEventArgs.RequestTypes.ASSIGN_TAG:
            {
                if (CurrentlySelectedFile != null)
                {
                    Tag_AssignTag(args.PassedTag.Id, TagAssignmentType.PrimaryTag);
                }
            }
                break;
            case TagEventArgs.RequestTypes.ASSIGN_TAG_SECONDARY:
            {
                if (CurrentlySelectedFile != null)
                {
                    Tag_AssignTag(args.PassedTag.Id, TagAssignmentType.SecondaryTag);
                }
            }
                break;
        }
    }

    private void PrepareLibraryFileStructure()
    {
        lstV_libraryStructure.SelectionChanged += OnTreev_libraryStructure_SelectionChanged;

        lstV_libraryStructure.ChangeDirectoryAction = UpdateVisuals;
        lstV_libraryStructure.UpdateVisualsRequest += (_, _) => UpdateVisuals();
        lstV_libraryStructure.FileBrowserRequest += (_, args) =>
        {
            switch(args.Request)
            {
                case FileBrowserEventArgs.RequestTypes.RENAME_FILE: File_SetUserName(args.File); break;
                case FileBrowserEventArgs.RequestTypes.CREATE_DIR: CreateNewDir(args); break;
                case FileBrowserEventArgs.RequestTypes.RENAME_DIR: RenameDir(args); break;
                default: throw new InvalidOperationException();
            }
        };
    }

    private void PreparePaneResizers()
    {
        pr_tagXpreviewPane.ResizableColumn = clmnDef_main_1;
        pr_assignedTagsXpreviewPane.ResizableRow = rowDef_previewPane_1;
        pr_assignedTagsXpreviewPane.SafetyMarginMultiplication = 7;
    }

    private void PrepareAssignedTagsControls()
    {
        assignedTags_primary.Request += OnTagRequest;
        assignedTags_secondary.Request += OnTagRequest;
    }

    private void TryOpenLatestLibrary()
    {
        try
        {
            var lastOpenedLibPath = Services.ApplicationConfiguration.AppConfigurationManager.GetLastOpenedLibrary();
            if (!string.IsNullOrEmpty(lastOpenedLibPath))
            {
                CommonGUILogic.OpenLibrary(lastOpenedLibPath);
            }
        }
        catch (Exception ex)
        {
            Services.FileTaggingToolLoggingManager.LogMessage(Logging.MessageTypes.ERROR_INTERNAL, ex.Message, ex.StackTrace);
        }
    }

    private void btn_GetDirectory_Click(object sender, RoutedEventArgs args)
    {
        CommonGUILogic.OpenLibrary();
    }

    private void btn_ProcessUserData_Click(object sender, RoutedEventArgs args)
    {
        if (!ManagerInstance.IsLibraryFolderPathValid(ManagerInstance.OpenedLibraryFolder))
        {
            Forms.MessageBox.Show(UserMessages.NoLibraryOpened, UserMessages.ErrorCaption);
            return;
        }

        try
        {
            ToggleControlsWhenProcessing(false);
            _fileProcessingWorker.RunWorkerAsync();
        }
        catch (Exception ex)
        {
            Services.FileTaggingToolLoggingManager.LogMessage(Logging.MessageTypes.ERROR_INTERNAL, ex.Message, ex.StackTrace);
            Forms.MessageBox.Show("Došlo k chybě při zpracovávání souborů v knihovně.", UserMessages.ErrorCaption);
        }
    }

    private void FilteredTagRelationSwitcher_StateChanged(object sender, EventArgs e)
    {
        UpdateVisuals();
    }

    private void _fileProcessingWorker_DoWork(object sender, DoWorkEventArgs args)
    {
        try
        {
            ManagerInstance.ProcessFilesInOpenedLibrary();
        }
        finally
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                MyProgressBarHelperStructure.ResetBar();
                ToggleControlsWhenProcessing(true);
                UpdateVisuals();
            });
        }
    }

    private void ButtonActions(object sender, RoutedEventArgs args)
    {
        var btn = (Button)sender;

        switch(btn.Name)
        {
            case "btn_tag_assign": lstv_availableTags.Tag_Assign(CurrentlySelectedAvailableTag, CurrentlySelectedFileAll, TagAssignmentType.PrimaryTag); break;
            case "btn_tag_assign_secondary": lstv_availableTags.Tag_Assign(CurrentlySelectedAvailableTag, CurrentlySelectedFileAll, TagAssignmentType.SecondaryTag); break;
            case "btn_tag_addNewTag": Tag_AddNewTag(); break;
            case "btn_tag_removeTag": Tag_RemoveTag(); break;
            case "btn_tag_renameTag": Tag_renameTag(); break;
            case "btn_tag_addToFilter": Tag_Filtering(AppActionCollectionAlterations.ADD); break;
            case "btn_tag_removeFromFilter": Tag_Filtering(AppActionCollectionAlterations.REMOVE); break;
            case "btn_file_userName": File_SetUserName(); break;
            default:
                throw new InvalidOperationException($"No implementation for button of name: {btn.Name}");
        }

        ResetTagSearchDelayWindow();
    }

    /// <summary>
    /// Dissociates tag from file. When null is passed for assignmentType, tag is dissociated from file for all assignment types.
    /// </summary>
    /// <param name="tagId"></param>
    /// <param name="assignmentType"></param>
    private void Tag_DissociateTag(int tagId, TagAssignmentType? assignmentType = null)
    { 
        ManagerInstance.DissociateTags(tagId, CurrentlySelectedFileAll.Select(f => f.Id), assignmentType);
        ResetTagSearchDelayWindow();
    }
        
    private void Tag_AssignTag(int tagId, TagAssignmentType assignmentType)
    {
        foreach(var file in CurrentlySelectedFileAll)
        {
            var assignedTags = ManagerInstance.GetAssignedTags(file.Id, false);
            if (assignedTags.Any(t => t.Tag.Id == tagId && t.TagAssignmentType == assignmentType))
            {
                return;
            }
            ManagerInstance.AssignTag(tagId, file.Id, assignmentType);
        }

        ResetTagSearchDelayWindow();
    }

    private void Tag_AddNewTag(int? parentID = null)
    {
        void SuccessfulCreationAction(long newTagId)
        {
            if (parentID != null)
            {
                Manager.Instance.MoveInTagTreeStructure((int) newTagId, parentID);
            }

            ResetTagSearchDelayWindow();
        }

        CommonGUILogic.CreateNewTag(SuccessfulCreationAction);
    }

    private void Tag_RemoveTag(Tag specifiedTag = null)
    {
        var selectedTag = specifiedTag ?? CurrentlySelectedAvailableTag;

        if (selectedTag == null) { return; }

        ManagerInstance.RemoveTag(selectedTag.Id);

        ResetTagSearchDelayWindow();
    }

    private void Tag_renameTag()
    {
        var tag = CurrentlySelectedAvailableTag;
        if (tag == null) { return; }

        Action successfulRenameAction = ResetTagSearchDelayWindow;

        CommonGUILogic.RenameTag(tag, successfulRenameAction);
    }

    private void Tag_Filtering(AppActionCollectionAlterations action, Tag specifiedTag = null)
    {
        var selectedAvailableTag = specifiedTag ?? CurrentlySelectedAvailableTag;
        var selectedFilteringTag = ftc_filteredTags.CurrentlySelectedFilteringTag;

        switch (action)
        {
            case AppActionCollectionAlterations.ADD:
                if (selectedAvailableTag == null || ftc_filteredTags.FilteringTags.Any(ft => ft.TagId == selectedAvailableTag.Id)) { return; }
                ftc_filteredTags.AddFilteringTag(FilteredTag.ToFilteredTag(selectedAvailableTag, CommonGUILogic.GetDefaultAssignmentTypesForFilter()));
                break;
            case AppActionCollectionAlterations.REMOVE:
                if (selectedFilteringTag == null || !ftc_filteredTags.FilteringTags.Contains(selectedFilteringTag)) { return; }
                ftc_filteredTags.RemoveFilteringTag(selectedFilteringTag);
                break;
            default:
                throw new InvalidOperationException($"Unexpected action requested: {action.ToString()}");
        }

        UpdateVisuals();
    }

    private void File_SetUserName(FileTagger.ORM.File specifiedFile = null)
    {
        var file = specifiedFile ?? CurrentlySelectedFile;

        if (file == null) { return; }

        var filenameBeforeChange = file.GetFileName(false, false);

        if (_promptWindow != null && _promptWindow.IsLoaded) { return; }
        _promptWindow = new TextPrompt(file.GetFileName(false, false));

        void NoTextAction() => MessageBox.Show("Nebyl vyplněn žádný text. K přemenování souboru nedojde");

        void SuccessAction(string userFilename)
        {
            if (!ManagerInstance.IsFileNameValid(userFilename, false))
            {
                MessageBox.Show($@"Zvolili jste neplatný název souboru. Název nesmí obsahovat speciální znaky jako: \, <, >, |, :, *, ?, /", "Neplatný název souboru");
                return;
            }

            try
            {
                ManagerInstance.SetUserFilename(file.Id, userFilename);
                FileEvaluationSettings settings = new FileEvaluationSettings() {FileSelector = new FileIdentifierDatabaseSelector(new string[] {file.Identifier})};
                ManagerInstance.ProcessFiles(settings);
            }
            catch (Exception ex)
            {
                try
                {
                    ManagerInstance.SetUserFilename(file.Id, filenameBeforeChange);
                }
                catch (Exception innerException)
                {
                    Services.FileTaggingToolLoggingManager.LogMessage(Logging.MessageTypes.ERROR_INTERNAL, innerException.Message, innerException.StackTrace);
                }

                FileTaggingToolLoggingManager.LogMessage(Logging.MessageTypes.ERROR_INTERNAL, ex.Message, ex.StackTrace);
                MessageBox.Show("Nastal problém při pokusu o přejmenování souboru. Byl navrácen původní název.", UserMessages.ErrorCaption);
                return;
            }

            try
            {
                PopulateLibraryFileTree();
                ResetTagSearchDelayWindow();
            }
            catch (Exception ex)
            {
                FileTaggingToolLoggingManager.LogMessage(Logging.MessageTypes.ERROR_INTERNAL, ex.Message, ex.StackTrace);
                MessageBox.Show("Došlo k úspěšnému pojmenování souboru, ale nastala chyba při aktualizaci stránky. Zkuste prosím opětovně načíst knihovnu.", UserMessages.ErrorCaption);
                return;
            }
        }

        _promptWindow.Initialize("Pojmenování souboru", "Nový název souboru", NoTextAction, SuccessAction);
        _promptWindow.Show();
    }
        
    private void CreateNewDir(FileBrowserEventArgs args)
    {
        void MyAction()
        {
            UpdateVisuals();
        }

        CommonGUILogic.CreateNewDirectory(args.Directory, MyAction);
    }

    private void RenameDir(FileBrowserEventArgs args)
    {
        void RenameDirectory()
        {
            FileEvaluationSettings sett = null;
            if (args.Directory.Parent != null && Manager.IsPathWithinOpenedLibPath(args.Directory.Parent.FullName))
            {
                sett = new FileEvaluationSettings
                {
                    FileSelector = new FileDirectorySelector(new DirectoryInfo(args.Directory.Parent.FullName))
                };
            }

            ManagerInstance.ProcessFiles(sett);
            UpdateVisuals();
        }

        CommonGUILogic.RenameDirectory(args.Directory, RenameDirectory);
    }

    private void RotatePreviewedImage(System.Drawing.RotateFlipType rotation)
    {
        cc_filePreview.Content = null;

        var currentImage = CurrentlySelectedFile;
        var imageFullPath = currentImage.GetFullFinalFilepath(ManagerInstance.OpenedLibraryUserDataDirPath);
        var img = System.Drawing.Image.FromFile(imageFullPath);

        img.RotateFlip(rotation);
        img.Save(imageFullPath);
        UpdatePreview();
    }

    #region Updating GUI visuals

    private void OnLibraryOpened(object sender, EventArgs args)
    {
        try
        {
            UpdateVisuals();
        }
        catch (Exception ex)
        {
            FileTaggingToolLoggingManager.LogMessage(Logging.MessageTypes.ERROR_INTERNAL, ex.Message, ex.StackTrace);
            MessageBox.Show("Došlo k chybě v návaznosti na otevření knihovny", UserMessages.ErrorCaption);
        }
    }
    
    private void txtbx_filesTextSearchFiltering_TextChanged(object sender, TextChangedEventArgs args)
    {
        ResetFileSearchDelayWindow();
    }

    private void txtbx_tagFiltering_textChanged(object sender, TextChangedEventArgs ars)
    {
        ResetTagSearchDelayWindow();
    }
    private void OnTreev_libraryStructure_SelectionChanged(object sender, RoutedEventArgs args)
    {
        UpdatePreview();
        UpdateIdentifierLabel();
        ResetTagSearchDelayWindow();
    }

    private void btn_files_filterUpLevel_Click(object sender, RoutedEventArgs args)
    {
        var upLevel = GetUpperPathLevel(lstV_libraryStructure.CurrentFilesFilterRelativePath, 1);
        lstV_libraryStructure.CurrentFilesFilterRelativePath = upLevel;
        UpdateCurrentFilePathVisual();
        PopulateLibraryFileTree();
    }

    private void On_chkbx_files_subdirs(object sender, RoutedEventArgs args)
    {
        lstV_libraryStructure.IncludeSubdirectories = Convert.ToBoolean(chkbx_files_subdirs.IsChecked);
        UpdateVisuals();
    }

    private void On_chkbx_tagless_files(object sender, RoutedEventArgs args)
    {
        lstV_libraryStructure.OnlyTaglessFiles = Convert.ToBoolean(chkbx_tagless_files.IsChecked);
        UpdateVisuals();
    }

    private void UpdateVisuals()
    {
        txtbx_MainDirectoryPath.Text = ManagerInstance.OpenedLibraryFolder;
        PopulateLibraryFileTree();
        ResetTagSearchDelayWindow();
        UpdatePreview();
        UpdateCurrentFilePathVisual();
    }

    private void UpdateTagRelatedGUI()
    {
        PopulateTagsListView();
        PopulateAssignedTagsControl();
        ftc_filteredTags.PopulateFilteringTagListView();
    }

    private void UpdateIdentifierLabel()
    {
        if (CurrentlySelectedFile != null && !string.IsNullOrEmpty(CurrentlySelectedFile.Identifier))
        {
            lbl_current_identifier.Text = string.Concat(CurrentlySelectedFile.Identifier);
        }
        else if (CurrentlySelectedFile != null && string.IsNullOrEmpty(CurrentlySelectedFile.Identifier))
        {
            lbl_current_identifier.Text = "Chyba! Identifikátor nedostupný";
        }
        else
        {
            lbl_current_identifier.Text = "------";
        }
    }

    private void PopulateLibraryFileTree()
    {
        var allValidFilesMatchingTagFilter = ValidFileRecordsMatchingTagFilter;

        var filesFilteredByTextSearch = allValidFilesMatchingTagFilter
            .Where(FilterFilesByTextSearch)
            .ToList();

        var numberOfFiles = lstV_libraryStructure.UpdateFileTree(filesFilteredByTextSearch);
        label_numberOfFiles.Content = numberOfFiles;

        bool FilterFilesByTextSearch(File file)
        {
            if (string.IsNullOrWhiteSpace(CurrentFileTextSearch))
            {
                return true;
            }
            
            try
            {
                return System.Text.RegularExpressions.Regex.IsMatch(file.GetFileName(false, true), CurrentFileTextSearch, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            }
            catch
            {
                return true;
            }
        }
    }

    private void PopulateTagsListView()
    {
        lstv_availableTags.UpdateTagPresentation(GetAssignedTags(true).Select(t => t.Tag).ToArray(), ValidTags, TagFilterMask);
    }

    private void PopulateAssignedTagsControl()
    {
        grd_noAssignedTags.Visibility = Visibility.Hidden; // TODO TW: I'm not too happy with this :)
        grd_noFileSelected.Visibility = Visibility.Hidden;
        grd_assignedTags_tags.Visibility = Visibility.Visible;
            
        if (CurrentlySelectedFile == null)
        {
            grd_noFileSelected.Visibility = Visibility.Visible;
            grd_noAssignedTags.Visibility = Visibility.Hidden;
            grd_assignedTags_tags.Visibility = Visibility.Hidden;
            return;
        }

        var assignedTags = GetAssignedTags(true).OrderBy(tag => tag.Tag.Header).ToArray();
            
        if (!assignedTags.Any())
        {
            grd_noAssignedTags.Visibility = Visibility.Visible;
            grd_noFileSelected.Visibility = Visibility.Hidden;
            grd_assignedTags_tags.Visibility = Visibility.Hidden;
            return;
        }
            
        assignedTags_primary.SetAssignedTags(assignedTags.Where(t => t.TagAssignmentType == TagAssignmentType.PrimaryTag).Select(t => t.Tag), OnTagRequest);
        assignedTags_secondary.SetAssignedTags(assignedTags.Where(t => t.TagAssignmentType == TagAssignmentType.SecondaryTag).Select(t => t.Tag), OnTagRequest);
    }

    private void UpdatePreview()
    {
        if (lstV_libraryStructure.SelectedItem == null)
        {
            cc_filePreview.Content = new Label
            {
                Content = UserMessages.FilePreviewNoFileSelected,
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center
            };

            return;
        }

        if (lstV_libraryStructure.SelectedItem is not FilesBrowserRow fileRecord) { throw new Exception("Unexpected item withing tree view control."); }

        ImageSource imageSource = null;
        try
        {
            var currentImage = CurrentlySelectedFile;
            var imageFullPath = currentImage.GetFullFinalFilepath(ManagerInstance.OpenedLibraryUserDataDirPath);

            FileUtils.RemoveReadonlyAttributeIfPresent(imageFullPath);

            var bmi = new BitmapImage();

            using (var fs = new FileStream(imageFullPath, FileMode.Open, FileAccess.ReadWrite))
            {
                bmi.BeginInit();
                bmi.StreamSource = fs;
                bmi.CacheOption = BitmapCacheOption.OnLoad;
                bmi.EndInit();
            }

            bmi.Freeze(); //Important to freeze it, otherwise it will still have minor leaks

            imageSource = bmi;
        }
        catch { }

        if (imageSource == null)  // not necessarily an error
        {
            var noImagePlaceholder = new Label()
            {
                Content = UserMessages.FilePreviewNotAvailable,
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center
            };
            cc_filePreview.Content = noImagePlaceholder;
            return;
        }

        var img = new Image { Source = imageSource };
        cc_filePreview.Content = img;
    }

    private void UpdateCurrentFilePathVisual()
    {
        txtbx_filesCurrentFilterPath.Text = string.IsNullOrEmpty(lstV_libraryStructure.CurrentFilesFilterRelativePath)
            ? UserMessages.RootFolder
            : lstV_libraryStructure.CurrentFilesFilterRelativePath;
    }
    
    private void ResetFileSearchDelayWindow() => ResetTimer(_fileSearchDelayed);
    
    private void ResetTagSearchDelayWindow() => ResetTimer(_tagSearchDelayed);

    private static void ResetTimer(Timer timer)
    {
        timer.Stop();
        timer.Start();
    }

    #endregion

    #region  Supportive methods

    private FileTagger.ORM.File[] GetValidFileRecords()
    {
        return ManagerInstance.GetFiles(true);
    }

    /// <summary>
    /// Gets assigned tags for currently selected file.
    /// </summary>
    private IEnumerable<AssignedTag> GetAssignedTags(bool validOnly)
    {
        return CurrentlySelectedFile == null
            ? Array.Empty<AssignedTag>()
            : ManagerInstance.GetAssignedTags(CurrentlySelectedFile.Id, validOnly);
    }

    private Dictionary<int, IEnumerable<TagAssignmentType>> GetTagIdsFromFilter()
    {
        var selectedTags = ftc_filteredTags.FilteringTags.ToDictionary(x => x.TagId, x => x.TagAssignmentType);
        return selectedTags;
    }

    private string GetUpperPathLevel(string path, int levelOffset)
    {
        var pathClean = FileTagger.ORM.File.CleanPath(path);
        var splitted = pathClean.Split('\\');

        if (levelOffset > splitted.Length) { return ""; }

        var sb = new StringBuilder();

        var iterations = splitted.Length - levelOffset;
        for (var i = 0; i < iterations; i++)
        {
            sb.Append(string.Concat(splitted[i], '\\'));
        }

        var result = sb.ToString().TrimEnd('\\');

        return result;
    }

    private string GetFileProcessingStageTranslation(FileProcessingReportEventArgs.Stages stage)
    {
        return stage switch
        {
            FileProcessingReportEventArgs.Stages.CompletedByFailure => UserMessages.CompletedByFailure,
            FileProcessingReportEventArgs.Stages.CompletedSuccessfully => UserMessages.CompletedSuccessfully,
            FileProcessingReportEventArgs.Stages.InitializingFileRecords => UserMessages.InitializingFileRecords,
            FileProcessingReportEventArgs.Stages.InvalidatingFileRecords => UserMessages.InvalidatingFileRecords,
            FileProcessingReportEventArgs.Stages.ProcessingStarted => UserMessages.ProcessingStarted,
            FileProcessingReportEventArgs.Stages.RenamingFiles => UserMessages.RenamingFiles,
            _ => stage.ToString()
        };
    }

    private void ToggleControlsWhenProcessing(bool enabled)
    {
        var controls = new Control[]
        {
            lstV_libraryStructure,
            ftc_filteredTags,
            lstv_availableTags,
            btn_BatchRename,
            btn_files_filterUpLevel,
            btn_file_userName,
            btn_GetDirectory,
            btn_tag_addNewTag,
            btn_tag_addToFilter,
            btn_tag_assign,
            btn_tag_assign_secondary,
            btn_tag_removeFromFilter,
            btn_tag_removeTag,
            btn_tag_renameTag
        };

        foreach (var t in controls)
        {
            t.IsEnabled = enabled;
        }
    }

    #endregion
}