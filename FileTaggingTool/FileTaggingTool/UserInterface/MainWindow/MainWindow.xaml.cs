﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Demos.DemosHelpers;
using Forms = System.Windows.Forms;
using Demos.FileTagger;

namespace Demos.FileTaggingTool.UserInterface
{
    public partial class MainWindow : System.Windows.Window
    {
        internal TextPrompt NewLibraryNamePromptWindow = null;
        public Manager ManagerInstance { get { return Manager.Instance; } }


        public MainWindow()
        {
            InitializeComponent();

            this.StateChanged += MainWindowStateChanged;
            this.MaxHeight = SystemParameters.MaximizedPrimaryScreenHeight;

            MyProgressBarHelperStructure.actualBar = this.rctngl_MainWindowProgressBar_Bar;
            MyProgressBarHelperStructure.comment = this.lbl_MainWindowProgressBar_Comment;

            AppWindowHandler.ChangeWindowContent("AppMainWindow", new HomePage.HomePage_Main());

#if DEBUG
            MenuItem debugItem = new MenuItem()
            {
                Header = "debug",
                Name = "Debug1"
            };
            debugItem.Click += this.MenuItemClicked;
            this.menu_mainMenu.Items.Add(debugItem);
#endif

            this.UpdateGUI();
            this.BindToEvents();
        }

        private void BindToEvents()
        {
            Services.ApplicationConfiguration.AppConfigurationManager.Instance.ConfigurationChanged += (sender, args) =>
            {
                if (args.ConfigInfo == Services.ApplicationConfiguration.ConfigMapper.GetMapperInfo(Services.ApplicationConfiguration.ConfigMapper.ConfigTypes.GENERAL_PROCESS_FILES_AUTOMATICALLY))
                {
                    this.UpdateGUI();
                }
            };
        }

        private void MenuItemClicked(object sender, RoutedEventArgs args)
        {
            MenuItem menuItem = (MenuItem)sender;

            switch (menuItem.Name)
            {
                case "Ukoncit":
                    FileTaggingToolStatic.FileTaggingToolAppInstance.Shutdown();
                    break;
                case "OpenExisting":
                    this.OpenLibrary();
                    break;
                case "CreateNew":
                    this.CreateNewLibrary();
                    break;
                case "Uvod":
                    AppWindowHandler.ChangeWindowContent("AppMainWindow", new HomePage.HomePage_Main());
                    break;
                case "UpdateApplication":
                    AppWindowHandler.ChangeWindowContent("AppMainWindow", new ApplicationUpdate.ApplicationUpdate());
                    break;
                case "AboutApp":
                    MessageBox.Show(
                        $"Název aplikace: File Tagging Tool" + Environment.NewLine +
                        $"Verze aplikace: {FileTaggingToolStatic.Version.ToString()}" + Environment.NewLine +
                        $"Vývojář: Demo; Opensource: https://bitbucket.org/Demo30/filetaggingtool",
                    "O aplikaci");
                    break;
                case "btn_autoProcess":
                    bool currentAutoProcess = Services.LibraryService.LibraryService.Instance.FilesProcessedAutomatically;
                    Services.LibraryService.LibraryService.Instance.FilesProcessedAutomatically = !currentAutoProcess;
                    break;
                case "btn_processAllFiles":
                    Services.LibraryService.LibraryService.Instance.ProcessAllFilesInLibrary();
                    break;
                case "Debug1":
                    AppWindowHandler.ChangeWindowContent("AppMainWindow", new Debug1() );
                    break;
                default:
                    AppWindowHandler.ChangeWindowContent("AppMainWindow", new HomePage.HomePage_Main());
                    break;
            }
        }

        #region  Core main window GUI events

        private void UpdateGUI()
        {
            this.btn_autoProcess.IsChecked = Services.ApplicationConfiguration.AppConfigurationManager.GetProcessFilesAutomatically();
        }

        private void MainWindowStateChanged(object sender, EventArgs args)
        {
            if (this.WindowState == WindowState.Maximized)
            {
                this.BorderThickness = new Thickness(15, 20, 15, 15);
                this.NormMaxWindow.Source = GeneralHelperClass.GetImageSourceFromString(Demos.FileTaggingTool.Resources.InternalResourceMapper.ImageResource + "Normal_15.png");
            }
            else
            {
                this.NormMaxWindow.Source = GeneralHelperClass.GetImageSourceFromString(Demos.FileTaggingTool.Resources.InternalResourceMapper.ImageResource + "Maximize_15.png");

                setDefaultWindowBorder();
            }
        }

        private void setDefaultWindowBorder()
        {
            SetterBaseCollection GeneralWindowStyleSetters = ((Style)FileTaggingToolStatic.FileTaggingToolAppInstance.Resources["GeneralWindowStyle"]).Setters;
            Thickness CustomStyleDefinedBorderThickness = new Thickness();
            foreach (Setter Setter in GeneralWindowStyleSetters)
            {
                if (Setter.Property.Name == "BorderThickness")
                {
                    CustomStyleDefinedBorderThickness = (Thickness)Setter.Value;
                }
            }
            if (CustomStyleDefinedBorderThickness != default(Thickness))
            {
                this.BorderThickness = CustomStyleDefinedBorderThickness;
            }
        }

        private void MainWindow_Menu_File_Close_Click(object sender, RoutedEventArgs args)
        {
            FileTaggingToolStatic.FileTaggingToolAppInstance.Shutdown();
        }

        private void DragWindow_LeftButtonDown(object sender, MouseButtonEventArgs args)
        {
            this.DragMove();
        }

        private void Close_Click(object sender, RoutedEventArgs args)
        {
            FileTaggingToolStatic.FileTaggingToolAppInstance.Shutdown();
        }

        private void NormalMaximize_Click(object sender, RoutedEventArgs args)
        {
            if (this.WindowState == WindowState.Maximized)
            {
                this.WindowState = WindowState.Normal;
            }
            else
            {
                this.WindowState = WindowState.Maximized;
                this.OnStateChanged(new EventArgs());
            }
        }

        private void Minimize_Click(object sender, RoutedEventArgs args)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void TopBarButtons_Enter(object sender, RoutedEventArgs args)
        {
            ((Grid)sender).Background = new SolidColorBrush(Color.FromRgb(0xEB, 0xEF, 0xF0));
        }

        private void TopBarButtons_Leave(object sender, RoutedEventArgs args)
        {
            ((Grid)sender).Background = new SolidColorBrush(Colors.Transparent);
        }

        #endregion

        #region Library opening events
        
        private void OpenLibrary()
        {
            CommonGUILogic.OpenLibrary();
        }

        private void CreateNewLibrary()
        {
            CommonGUILogic.StartNewLibrary();
        }
        
        #endregion
    }

}

