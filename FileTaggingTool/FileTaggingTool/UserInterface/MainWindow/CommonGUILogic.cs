﻿using System;
using System.Collections.Generic;
using Forms = System.Windows.Forms;
using Demos.FileTagger;
using System.Windows;
using System.IO;
using Demos.FileTagger.ORM;
using File = System.IO.File;

namespace Demos.FileTaggingTool.UserInterface;

public static class CommonGUILogic
{
    private static TextPrompt NewLibraryNamePromptWindow = null;
    private static TextPrompt TagNamePromptWindow = null;
    private static TextPrompt FolderNamePromptWindow = null;

    public static Manager ManagerInstance => Manager.Instance;

    public static void OpenLibrary()
    {
        var dirDialog = new Forms.FolderBrowserDialog();
        var dires = dirDialog.ShowDialog();

        if (dires != Forms.DialogResult.OK) { return; }

        var path = dirDialog.SelectedPath;
        OpenLibrary(path);
    }

    public static void OpenLibrary(string pathToLibrary)
    {
        try
        {
            ManagerInstance.OpenLibraryFolder(pathToLibrary);
            ManagerInstance.UpdateLibraryDatabaseStructure();

            try
            {
                Services.ApplicationConfiguration.AppConfigurationManager.Instance.SetLastLoadedLibraryPath(pathToLibrary);
            } catch (Exception ex)
            {
                Services.FileTaggingToolLoggingManager.LogMessage(Logging.MessageTypes.ERROR_INTERNAL, ex.Message, ex.StackTrace);
            }
        }
        catch (Exception ex)
        {
            Forms.MessageBox.Show("Pokus o otevření knihovny se nezdařil. Ujistěte se, že jste zvolili cestu k validní knihovně souborů.", UserMessages.ErrorCaption);
            Services.FileTaggingToolLoggingManager.LogMessage(Logging.MessageTypes.ERROR_INTERNAL, ex.Message, ex.StackTrace);
        }
    }

    public static void StartNewLibrary()
    {
        var dirDialog = new Forms.FolderBrowserDialog();
        dirDialog.ShowDialog();
        var path = dirDialog.SelectedPath;

        if (string.IsNullOrEmpty(path)) { return; }

        if (NewLibraryNamePromptWindow != null && NewLibraryNamePromptWindow.IsLoaded) { return; }
        NewLibraryNamePromptWindow = new TextPrompt();

        Action<string> success = (promptText) =>
        {
            ManagerInstance.SetNameForNewLibrary(promptText);
            try
            {
                ManagerInstance.SetupNewLibraryFolder(path);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Knihovnu se nezdařilo vytvořit. Pokud problémy přetrvávají kontaktujte vývojáře.", UserMessages.ErrorCaption);
            }
                
        };
        var notTextFilled = () => { Forms.MessageBox.Show("Název knihovny nemůže být prázdný."); };

        NewLibraryNamePromptWindow.Initialize("Název knihovny", "Název nově vytvářené knihovny souborů", notTextFilled, success);
        NewLibraryNamePromptWindow.Show();
    }

    public static void RenameTag(FileTagger.ORM.Tag tag, Action successfulRenameAction = null)
    {
        if (TagNamePromptWindow != null && TagNamePromptWindow.IsLoaded) { return; }
        TagNamePromptWindow = new TextPrompt(tag.Header);

        var noTextAction = () => { MessageBox.Show("Nebyl vyplněn žádný text. K přejmenování tagu nedojde"); };
        Action<string> mainSuccessAction = (text) =>
        {
            var proceedWithRename = true;

            if (ManagerInstance.DoesTagExists(text, false))
            {
                var res = MessageBox.Show(string.Format(UserMessages.ErrorDupliciteTagCreation, text), UserMessages.ErrorCaptionDupliciteTag, MessageBoxButton.OK);
                proceedWithRename = false;
            }

            if (proceedWithRename)
            {
                ManagerInstance.RenameTag(tag.Id, text);
                if (successfulRenameAction != null) { successfulRenameAction(); }
            }
            else
            {
                RenameTag(tag, successfulRenameAction);
            }
        };
        TagNamePromptWindow.Initialize("Přejmenování tagu", "Nový název tagu", noTextAction, mainSuccessAction);
        TagNamePromptWindow.Show();
    }

    public static void CreateNewTag(Action<long> successfulCreateTagAction = null)
    {
        if (TagNamePromptWindow is {IsLoaded: true}) { return; }
        TagNamePromptWindow = new TextPrompt("Název štítku", true);

        var noTextAction = () => { MessageBox.Show("Nebyl vyplněn žádný text. Štítek nebude vytvořen."); };
        Action<string> mainSuccessAction = (text) =>
        {
            var proceedWithRename = true;

            if (ManagerInstance.DoesTagExists(text, false))
            {
                var res = MessageBox.Show(string.Format(UserMessages.ErrorDupliciteTagCreation, text), UserMessages.ErrorCaptionDupliciteTag, MessageBoxButton.OK);
                proceedWithRename = false;
            }

            if (proceedWithRename)
            {
                var newId = ManagerInstance.AddTag(text);
                successfulCreateTagAction?.Invoke(newId);
            } else
            {
                CreateNewTag(successfulCreateTagAction);
            }
        };
        TagNamePromptWindow.Initialize(UserMessages.RenameTag, "Nový název tagu", noTextAction, mainSuccessAction);
        TagNamePromptWindow.Show();
    }

    public static void CreateNewDirectory(DirectoryInfo containingDir, Action successAction)
    {
        if (FolderNamePromptWindow != null && FolderNamePromptWindow.IsLoaded) { return; }

        FolderNamePromptWindow = new TextPrompt("Nová složka");
        Action noTextAction = () => { };
        Action<string> mainSuccessAction = (text) =>
        {
            var dirPath = $"{containingDir.FullName}\\{text}";
            try
            {
                Directory.CreateDirectory(dirPath);
                successAction?.Invoke();
            }
            catch (Exception ex)
            {
                var warning = $"Složku s názvem {text} se nepodařilo vytvořit" + $" ({ex.Message})";
                MessageBox.Show(warning, "Chyba při vytváření složky");
                Services.FileTaggingToolLoggingManager.LogMessage(Logging.MessageTypes.NOTICE, warning, ex.StackTrace);
            }
            FolderNamePromptWindow = null;
        };
        FolderNamePromptWindow.Initialize("Vytvoření nové složky", "Nová složka", noTextAction, mainSuccessAction);
        FolderNamePromptWindow.Show();
    }

    public static void RenameDirectory(DirectoryInfo dirToRename, Action successAction)
    {
        if (FolderNamePromptWindow != null && FolderNamePromptWindow.IsLoaded) { return; }

        FolderNamePromptWindow = new TextPrompt(dirToRename.Name);
        var noTextAction = () => { };
        Action<string> mainSuccessAction = (text) =>
        {
            try
            {
                string newDir = dirToRename.FullName.Replace(dirToRename.Name, "") + text;

                Directory.Move(dirToRename.FullName, newDir);
                if (successAction != null)
                {
                    successAction();
                }
            }
            catch (Exception ex)
            {
                var warning = $"Složku se nepodařilo přejmenovat." + $" ({ex.Message})";
                MessageBox.Show(warning, "Chyba při přejmenovávání složky");
                Services.FileTaggingToolLoggingManager.LogMessage(Logging.MessageTypes.NOTICE, warning, ex.StackTrace);
            }
            FolderNamePromptWindow = null;
        };
        FolderNamePromptWindow.Initialize("Vytvoření nové složky", "Nová složka", noTextAction, mainSuccessAction);
        FolderNamePromptWindow.Show();
    }

    public static void MoveContentToLibrary(DirectoryInfo destinationDirectory, string[] fullPaths, bool copy)
    {
        foreach (var path in fullPaths)
        {
            if (File.Exists(path))
            {
                var fi = new FileInfo(path);
                var newFilePath = $"{destinationDirectory.FullName}\\{fi.Name}";
                if (copy)
                {
                    File.Copy(fi.FullName, newFilePath);
                }
                else
                {
                    File.Move(fi.FullName, newFilePath);
                }

            }
            else if (Directory.Exists(path))
            {
                var di = new DirectoryInfo(path);
                var newDirPath = $"{destinationDirectory.FullName}\\{di.Name}";
                var diTarget = new DirectoryInfo(newDirPath);
                if (copy)
                {
                    DemosHelpers.GeneralHelperClass.CopyDirectoriesWithContent(di, diTarget);
                } 
                else
                {
                    Directory.Move(di.FullName, diTarget.FullName);
                }
            }
            else
            {
                throw new Exception("File drop failed. File does not exist on specified path.");
            }
        }
    }

    public static IEnumerable<TagAssignmentType> GetDefaultAssignmentTypesForFilter()
    {
        return new[] {TagAssignmentType.PrimaryTag};
    }
}