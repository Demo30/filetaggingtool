﻿using System;
using System.Windows;
using System.Windows.Controls;
using Demos.FileTaggingTool.Services;
using Demos.DesktopClient.Updater;

namespace Demos.FileTaggingTool.UserInterface.ApplicationUpdate
{
    public partial class ApplicationUpdate : Page
    {
        private bool? _newerVersionAvailable = null;
        private IInfoData _latestVersionInfoDataPackage = null;


        public ApplicationUpdate()
        {
            InitializeComponent();
            this.PrepareControls();
        }

        private void PrepareControls()
        {
            this.UpdateVersionDetail();
        }

        private void UpdateVersionDetail()
        {
            this.stckpn_versionDetail.Children.Clear();
            this.stckpn_versionDetail.Children.Add(new Separator() { VerticalAlignment = VerticalAlignment.Top });

            Label textMessage = new Label()
            {
                Margin = new Thickness(0, 10, 0, 0),
                Content = String.Empty
            };

            this.stckpn_versionDetail.Children.Add(textMessage);

            if (this._newerVersionAvailable == null)
            {
                textMessage.Content = "Informace o aktuálnosti verze nedostupná. Proveďte nejprve kontrolu.";
            }
            else if (this._newerVersionAvailable == false)
            {
                textMessage.Content = "Užíváte nejaktuálnější dostupnou verzi aplikace.";
            }
            else
            {
                textMessage.Content = "Byla nalezena novější verze aplikace! Je možné spustit aktualizaci.";

                Label versionInfo = new Label();

                if (this._latestVersionInfoDataPackage != null)
                {
                    versionInfo.Content = $"Nová verze: {this._latestVersionInfoDataPackage.Version.ToString()} (Aktuální verze: {FileTaggingToolStatic.Version.ToString()})";
                }
                else
                {
                    versionInfo.Content = "Informace o nové verzi nejsou dostupné.";
                }

                StackPanel rowVersion = new StackPanel()
                {
                    Orientation = Orientation.Horizontal
                };
                rowVersion.Children.Add(versionInfo);

                this.stckpn_versionDetail.Children.Add(rowVersion);

                if (this._latestVersionInfoDataPackage.ChangesInVersion != null && this._latestVersionInfoDataPackage.ChangesInVersion.Length > 0)
                {
                    Button btn_messages = new Button()
                    {
                        Content = "Změny v této verzi",
                        Margin = new Thickness(10, 0, 0, 0),
                        Width = 150,
                        Height = 30
                    };
                    btn_messages.Click += (sender, args) =>
                    {
                        Window wnd = new Window()
                        {
                            Title = "Změny v nové verzi"
                        };
                        StackPanel stckpl = new StackPanel()
                        {
                            Margin = new Thickness(30)
                        };

                        foreach (string message in this._latestVersionInfoDataPackage.ChangesInVersion)
                        {
                            stckpl.Children.Add(new TextBlock { Margin = new Thickness(0, 10, 0, 0), Text = message, TextWrapping = TextWrapping.Wrap });
                        }

                        wnd.Content = stckpl;
                        wnd.Show();
                    };

                    rowVersion.Children.Add(btn_messages);
                }

                Button btn_update = new Button()
                {
                    VerticalAlignment = VerticalAlignment.Top,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    Margin = new Thickness(0, 10, 0, 0),
                    Height = 40,
                    Width = 150,
                    Content = "Aktualizovat aplikaci"
                };
                btn_update.Click += (btn, args) =>
                {
                    UpdateChecker.StartUpdater();
                };

                this.stckpn_versionDetail.Children.Add(btn_update);
            }
        }

        private void btn_checkUpdate_Click(object sender, RoutedEventArgs args)
        {
            try
            {
                this._newerVersionAvailable = UpdateChecker.NewVersionAvailable();
            }
            catch
            {
                this._newerVersionAvailable = null;
                MessageBox.Show("Nepodařilo se ověřit dostupnost nové verze aplikace. Zkontrolujte, zda je správně nastaven konfigurační soubor aktualizačního nástroje.");
            }

            try
            {
                IInfoData info = UpdateChecker.LatestVersionInfo();
                this._latestVersionInfoDataPackage = info;
            }
            catch { }

            this.UpdateVersionDetail();
        }


    }
}
