﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Demos.FileTaggingTool.UserInterface
{
    static class AppWindowHandler
    {
        public static void ChangeWindowContent(string chosenWindowByName, Page newPage)
        {
            #warning WIP: need to implement interface with WindowContent member or something and connect with AppGenericWindow....

            string newPageTitle = newPage.Title;

            if (chosenWindowByName == null || newPage == null) throw new ArgumentNullException();

            Window chosenWindow = AppWindowHandler.GetAppWindowByName(chosenWindowByName);
            if (chosenWindow == null)
                throw new ArgumentException("Application window of the provided name could not be found.");

            object newContent = newPage.Content;
            newPage.Content = null;

            switch (chosenWindow.Name)
            {
                case "AppMainWindow":
                    ((MainWindow)chosenWindow).WindowContent.Content = newContent;
                    chosenWindow.Title = newPageTitle;
                    break;
            }
        }

        public static Window GetAppWindowByName(string WindowName)
        {
            foreach (Window window in Application.Current.Windows)
            {
                if (window.Name.Equals(WindowName)) return window;
            }
            return null;
        }

        public static Grid GetProgressBar(string WindowName)
        {
            Grid progressBar = null;
            Window MainWindow = AppWindowHandler.GetAppWindowByName(WindowName);
            if (MainWindow != null)
            {
                progressBar = (Grid)((UserInterface.MainWindow)MainWindow).MainWindowProgressBar;
            }

            if (progressBar != null)
                return progressBar;
            else
                throw new Exception("Internal error. Progress bar could not be loaded.");
        }

        public static Label GetProgressBarCommentLabel(string WindowName)
        {
            Grid progressBar = GetProgressBar(WindowName);

            if (progressBar== null)
                throw new Exception("Progress bar comment label could not be loaded, because its progress bar could not be found.");

            Label commentLabel = null;
            
            if (progressBar.Children == null)
                throw new Exception("Progress bar comment label could not be loaded, because its progress bar does not contain any children.");

            foreach (object currentProgressBarChild in progressBar.Children)
            {
                if (currentProgressBarChild is Label && ((Label)currentProgressBarChild).Name == "lbl_ProgressComment")
                {
                    commentLabel = (Label)currentProgressBarChild;
                }
            }

            return commentLabel;
        }

        public static Grid GetMainWindowMenu(string WindowName)
        {
            UserInterface.MainWindow mainWindow;
            mainWindow = (UserInterface.MainWindow)AppWindowHandler.GetAppWindowByName(WindowName);
            if (mainWindow != null)
                return mainWindow.MainWindowMenu;
            else
                throw new Exception("Internal error. Couldn't find menu of the main window.");
        }
    }
}
