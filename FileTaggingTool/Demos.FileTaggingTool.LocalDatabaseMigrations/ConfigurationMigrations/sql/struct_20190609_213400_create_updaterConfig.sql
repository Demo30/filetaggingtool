CREATE TABLE updaterConfiguration
(
	id INTEGER primary key AUTOINCREMENT,
	config_name text UNIQUE NOT NULL,
	pathToUpdaterDirectory text,

    FOREIGN KEY (config_name) REFERENCES availableConfigs(CONFIG_NAME)
);