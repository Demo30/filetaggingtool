CREATE TABLE generalConfiguration
(
	id INTEGER primary key AUTOINCREMENT,
	config_name text UNIQUE NOT NULL,
	config_value text,

    FOREIGN KEY (config_name) REFERENCES availableConfigs(CONFIG_NAME)
);