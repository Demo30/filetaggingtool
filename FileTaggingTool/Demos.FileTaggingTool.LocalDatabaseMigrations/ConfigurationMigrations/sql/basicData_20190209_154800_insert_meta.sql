INSERT INTO
	meta (
		databaseName, 
		databaseCreationDate,
		lastDataModification
	)
	values (
		'FileTaggingTool',
		date('now'),
		date('now')
)