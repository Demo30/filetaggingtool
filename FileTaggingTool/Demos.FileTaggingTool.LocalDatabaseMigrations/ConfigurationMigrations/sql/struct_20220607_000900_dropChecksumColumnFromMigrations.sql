ALTER TABLE migrations RENAME TO _migrations_old;

CREATE TABLE "migrations" (
      "id"	INTEGER,
      "name"	varchar(500),
      "type"	varchar(255),
      "executionDate"	text,
      "success"	int(1),
      PRIMARY KEY("id")
);

INSERT INTO migrations (id, name, type, executionDate, success)
SELECT id, name, type, executionDate, success
FROM _migrations_old;

DROP TABLE _migrations_old;