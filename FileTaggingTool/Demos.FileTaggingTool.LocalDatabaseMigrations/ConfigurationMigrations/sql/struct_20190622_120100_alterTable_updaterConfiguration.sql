ALTER TABLE updaterConfiguration RENAME TO _updaterConfiguration_old;

CREATE TABLE updaterConfiguration
(
	id INTEGER primary key AUTOINCREMENT,
	config_name text UNIQUE NOT NULL,
	config_value text,

    FOREIGN KEY (config_name) REFERENCES availableConfigs(CONFIG_NAME)
);

INSERT INTO updaterConfiguration (config_name, config_value)
  SELECT config_name, pathToUpdaterDirectory
  FROM _updaterConfiguration_old;

DROP TABLE _updaterConfiguration_old;