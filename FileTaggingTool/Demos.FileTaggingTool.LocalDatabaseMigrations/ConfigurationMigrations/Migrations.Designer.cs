﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Demos.FileTaggingTool.LocalDatabaseMigrations.ConfigurationMigrations {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Migrations {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Migrations() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Demos.FileTaggingTool.LocalDatabaseMigrations.ConfigurationMigrations.Migrations", typeof(Migrations).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO
        ///	meta (
        ///		databaseName, 
        ///		databaseCreationDate,
        ///		lastDataModification
        ///	)
        ///	values (
        ///		&apos;FileTaggingTool&apos;,
        ///		date(&apos;now&apos;),
        ///		date(&apos;now&apos;)
        ///).
        /// </summary>
        internal static string basicData_20190209_154800_insert_meta {
            get {
                return ResourceManager.GetString("basicData_20190209_154800_insert_meta", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO
        ///	availableConfigs (
        ///		CONFIG_NAME 
        ///	)
        ///	values (
        ///		&apos;PATH_TO_UPDATER_DIR&apos;
        ///).
        /// </summary>
        internal static string basicData_20190609_213100_insert_availableConfigs {
            get {
                return ResourceManager.GetString("basicData_20190609_213100_insert_availableConfigs", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO
        ///	updaterConfiguration (
        ///		config_name,
        ///		pathToUpdaterDirectory
        ///	)
        ///	values (
        ///	    &apos;PATH_TO_UPDATER_DIR&apos;,
        ///		&apos;..\Updater&apos;
        ///).
        /// </summary>
        internal static string basicData_20190609_213900_insert_updaterConfig {
            get {
                return ResourceManager.GetString("basicData_20190609_213900_insert_updaterConfig", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO
        ///	availableConfigs (
        ///		CONFIG_NAME 
        ///	)
        ///	values (
        ///		&apos;UPDATER_SILENT_ERROR&apos;
        ///).
        /// </summary>
        internal static string basicData_20190622_115900_insert_availableConfig_silentMode {
            get {
                return ResourceManager.GetString("basicData_20190622_115900_insert_availableConfig_silentMode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO
        ///	updaterConfiguration (
        ///		config_name,
        ///		pathToUpdaterDirectory
        ///	)
        ///	values (
        ///	    &apos;UPDATER_SILENT_ERROR&apos;,
        ///		&apos;1&apos;
        ///).
        /// </summary>
        internal static string basicData_20190622_120000_insert_updaterConfig_silentMode {
            get {
                return ResourceManager.GetString("basicData_20190622_120000_insert_updaterConfig_silentMode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO
        ///	availableConfigs (
        ///		CONFIG_NAME 
        ///	)
        ///	values (
        ///		&apos;UPDATER_RUN_AS_ADMIN&apos;
        ///).
        /// </summary>
        internal static string basicData_20190622_121500_insert_availableConfig_runAsAdmin {
            get {
                return ResourceManager.GetString("basicData_20190622_121500_insert_availableConfig_runAsAdmin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO
        ///	updaterConfiguration (
        ///		config_name,
        ///		config_value
        ///	)
        ///	values (
        ///	    &apos;UPDATER_RUN_AS_ADMIN&apos;,
        ///		&apos;0&apos;
        ///).
        /// </summary>
        internal static string basicData_20190622_121600_insert_updaterConfig_runAsAdmin {
            get {
                return ResourceManager.GetString("basicData_20190622_121600_insert_updaterConfig_runAsAdmin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO
        ///	availableConfigs (
        ///		CONFIG_NAME 
        ///	)
        ///	values (
        ///		&apos;GENERAL_LAST_LIBRARY_PATH&apos;
        ///).
        /// </summary>
        internal static string basicData_20190622_185600_insert_availableConfig_general_lastLib {
            get {
                return ResourceManager.GetString("basicData_20190622_185600_insert_availableConfig_general_lastLib", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO
        ///	generalConfiguration (
        ///		config_name,
        ///		config_value
        ///	)
        ///	values (
        ///	    &apos;GENERAL_LAST_LIBRARY_PATH&apos;,
        ///		&apos;&apos;
        ///).
        /// </summary>
        internal static string basicData_20190622_185700_insert_updaterConfig_general_lastLib {
            get {
                return ResourceManager.GetString("basicData_20190622_185700_insert_updaterConfig_general_lastLib", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO
        ///	availableConfigs (
        ///		CONFIG_NAME 
        ///	)
        ///	values (
        ///		&apos;GENERAL_PROCESS_FILES_AUTOMATICALLY&apos;
        ///).
        /// </summary>
        internal static string basicData_20200222_141600_insert_availableConfig_general_autoProcess {
            get {
                return ResourceManager.GetString("basicData_20200222_141600_insert_availableConfig_general_autoProcess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO
        ///	generalConfiguration (
        ///		config_name,
        ///		config_value
        ///	)
        ///	values (
        ///	    &apos;GENERAL_PROCESS_FILES_AUTOMATICALLY&apos;,
        ///		&apos;1&apos;
        ///).
        /// </summary>
        internal static string basicData_20200222_141700_insert_updaterConfig_general_autoProcess {
            get {
                return ResourceManager.GetString("basicData_20200222_141700_insert_updaterConfig_general_autoProcess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO
        ///	availableConfigs (
        ///		CONFIG_NAME 
        ///	)
        ///	values (
        ///		&apos;GENERAL_IDENTIFIER_STRATEGY&apos;
        ///).
        /// </summary>
        internal static string basicData_20200809_193200_insert_availableConfig_identifierStrategy {
            get {
                return ResourceManager.GetString("basicData_20200809_193200_insert_availableConfig_identifierStrategy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO
        ///	generalConfiguration (
        ///		config_name,
        ///		config_value
        ///	)
        ///	values (
        ///	    &apos;GENERAL_IDENTIFIER_STRATEGY&apos;,
        ///		&apos;1&apos;
        ///).
        /// </summary>
        internal static string basicData_20200809_193300_insert_generalConfig_identifierStrategy {
            get {
                return ResourceManager.GetString("basicData_20200809_193300_insert_generalConfig_identifierStrategy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to UPDATE
        ///	generalConfiguration
        ///SET config_value = 2
        ///WHERE config_name = &apos;GENERAL_IDENTIFIER_STRATEGY&apos;.
        /// </summary>
        internal static string basicData_20200829_185500_update_defaultIdentifierStrategy {
            get {
                return ResourceManager.GetString("basicData_20200829_185500_update_defaultIdentifierStrategy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CREATE TABLE meta
        ///(
        ///	id INTEGER primary key AUTOINCREMENT,
        ///	databaseName text,
        ///	databaseCreationDate text,
        ///	lastDataModification text
        ///);.
        /// </summary>
        internal static string struct_20190209_153600_create_meta {
            get {
                return ResourceManager.GetString("struct_20190209_153600_create_meta", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CREATE TABLE availableConfigs
        ///(
        ///	id INTEGER primary key AUTOINCREMENT,
        ///	CONFIG_NAME text UNIQUE NOT NULL
        ///);.
        /// </summary>
        internal static string struct_20190609_213000_create_availableConfigs {
            get {
                return ResourceManager.GetString("struct_20190609_213000_create_availableConfigs", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CREATE TABLE updaterConfiguration
        ///(
        ///	id INTEGER primary key AUTOINCREMENT,
        ///	config_name text UNIQUE NOT NULL,
        ///	pathToUpdaterDirectory text,
        ///
        ///    FOREIGN KEY (config_name) REFERENCES availableConfigs(CONFIG_NAME)
        ///);.
        /// </summary>
        internal static string struct_20190609_213400_create_updaterConfig {
            get {
                return ResourceManager.GetString("struct_20190609_213400_create_updaterConfig", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ALTER TABLE updaterConfiguration RENAME TO _updaterConfiguration_old;
        ///
        ///CREATE TABLE updaterConfiguration
        ///(
        ///	id INTEGER primary key AUTOINCREMENT,
        ///	config_name text UNIQUE NOT NULL,
        ///	config_value text,
        ///
        ///    FOREIGN KEY (config_name) REFERENCES availableConfigs(CONFIG_NAME)
        ///);
        ///
        ///INSERT INTO updaterConfiguration (config_name, config_value)
        ///  SELECT config_name, pathToUpdaterDirectory
        ///  FROM _updaterConfiguration_old;
        ///
        ///DROP TABLE _updaterConfiguration_old;.
        /// </summary>
        internal static string struct_20190622_120100_alterTable_updaterConfiguration {
            get {
                return ResourceManager.GetString("struct_20190622_120100_alterTable_updaterConfiguration", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CREATE TABLE generalConfiguration
        ///(
        ///	id INTEGER primary key AUTOINCREMENT,
        ///	config_name text UNIQUE NOT NULL,
        ///	config_value text,
        ///
        ///    FOREIGN KEY (config_name) REFERENCES availableConfigs(CONFIG_NAME)
        ///);.
        /// </summary>
        internal static string struct_20190622_185600_create_generalConfig {
            get {
                return ResourceManager.GetString("struct_20190622_185600_create_generalConfig", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ALTER TABLE migrations RENAME TO _migrations_old;
        ///
        ///CREATE TABLE &quot;migrations&quot; (
        ///      &quot;id&quot;	INTEGER,
        ///      &quot;name&quot;	varchar(500),
        ///      &quot;type&quot;	varchar(255),
        ///      &quot;executionDate&quot;	text,
        ///      &quot;success&quot;	int(1),
        ///      PRIMARY KEY(&quot;id&quot;)
        ///);
        ///
        ///INSERT INTO migrations (id, name, type, executionDate, success)
        ///SELECT id, name, type, executionDate, success
        ///FROM _migrations_old;
        ///
        ///DROP TABLE _migrations_old;.
        /// </summary>
        internal static string struct_20220607_000900_dropChecksumColumnFromMigrations {
            get {
                return ResourceManager.GetString("struct_20220607_000900_dropChecksumColumnFromMigrations", resourceCulture);
            }
        }
    }
}
