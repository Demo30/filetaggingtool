﻿using System.Globalization;
using Demos.DatabaseMigrations;

namespace Demos.FileTaggingTool.LocalDatabaseMigrations.ConfigurationMigrations
{
    public class ConfigurationMigrationManager : DatabaseMigrationManager
    {
        private const string DATABASE_NAME = "ApplicationConfiguration.sqlite";

        public override void LoadResources()
        {
            var resourceSet = Migrations.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            LoadMigrationsFromResource(resourceSet);
        }

        public override string GetDatabaseFileName()
        {
            return DATABASE_NAME;
        }
    }
}
