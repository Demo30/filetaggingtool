﻿using System.Globalization;
using Demos.DatabaseMigrations;

namespace Demos.FileTaggingTool.LocalDatabaseMigrations.LoggingDatabaseMigrations
{
    public class LoggingDatabaseMigrationManager : DatabaseMigrationManager
    {
        private const string DATABASE_FILENAME = "Logging.sqlite";

        public override void LoadResources()
        {
            
            var resourceSet = migrations.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            LoadMigrationsFromResource(resourceSet);
        }

        public override string GetDatabaseFileName()
        {
            return DATABASE_FILENAME;
        }
    }
}