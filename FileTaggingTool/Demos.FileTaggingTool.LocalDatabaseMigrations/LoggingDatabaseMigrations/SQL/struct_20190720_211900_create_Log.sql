﻿CREATE TABLE log
(
	id INTEGER primary key AUTOINCREMENT,
	timestamp text,
	messageType text,
	message text
);