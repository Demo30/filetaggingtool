﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Demos.FileTaggingTool.LocalDatabaseMigrations.LoggingDatabaseMigrations {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class migrations {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal migrations() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Demos.FileTaggingTool.LocalDatabaseMigrations.LoggingDatabaseMigrations.migration" +
                            "s", typeof(migrations).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CREATE TABLE log
        ///(
        ///	id INTEGER primary key AUTOINCREMENT,
        ///	timestamp text,
        ///	messageType text,
        ///	message text
        ///);.
        /// </summary>
        internal static string struct_20190720_211900_create_Log {
            get {
                return ResourceManager.GetString("struct_20190720_211900_create_Log", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ALTER TABLE log ADD stacktrace text;.
        /// </summary>
        internal static string struct_20190811_093800_alter_log_newColumn {
            get {
                return ResourceManager.GetString("struct_20190811_093800_alter_log_newColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ALTER TABLE log ADD appversion text;.
        /// </summary>
        internal static string struct_20190811_094100_alter_log_newColumn {
            get {
                return ResourceManager.GetString("struct_20190811_094100_alter_log_newColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ALTER TABLE migrations RENAME TO _migrations_old;
        ///
        ///CREATE TABLE &quot;migrations&quot; (
        ///      &quot;id&quot;	INTEGER,
        ///      &quot;name&quot;	varchar(500),
        ///      &quot;type&quot;	varchar(255),
        ///      &quot;executionDate&quot;	text,
        ///      &quot;success&quot;	int(1),
        ///      PRIMARY KEY(&quot;id&quot;)
        ///);
        ///
        ///INSERT INTO migrations (id, name, type, executionDate, success)
        ///SELECT id, name, type, executionDate, success
        ///FROM _migrations_old;
        ///
        ///DROP TABLE _migrations_old;.
        /// </summary>
        internal static string struct_20220607_000900_dropChecksumColumnFromMigrations {
            get {
                return ResourceManager.GetString("struct_20220607_000900_dropChecksumColumnFromMigrations", resourceCulture);
            }
        }
    }
}
