Update files
Set fileExtension = '.' || fileExtension
Where fileExtension is not null and substr(fileExtension, 1, 1) <> '.';
