CREATE TABLE files
(
    id INTEGER primary key AUTOINCREMENT,
    identifier varchar(32) UNIQUE,
    userFilename varchar(512),
    originalFileName varchar(1024),
    originalFilePath varchar(1024),
    currentFilePath varchar(1024),
    fileExtension varchar(255),
    valid int(1)
);