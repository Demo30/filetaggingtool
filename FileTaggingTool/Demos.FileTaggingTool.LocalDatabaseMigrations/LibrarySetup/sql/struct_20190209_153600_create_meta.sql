CREATE TABLE meta
(
	id INTEGER primary key AUTOINCREMENT,
	databaseName text,
	databaseCreationDate text,
	lastDataModification text
);