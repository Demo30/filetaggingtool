CREATE TABLE files2tags
(
    id INTEGER primary key AUTOINCREMENT,
    fid int fk,
    tid int fk,

    CONSTRAINT fk_files FOREIGN KEY (fid) REFERENCES files(id) ON DELETE CASCADE
    CONSTRAINT fk_tags FOREIGN KEY (tid) REFERENCES tags(id) ON DELETE CASCADE
    CONSTRAINT unique_composite1
        UNIQUE (fid, tid)
);