CREATE TABLE tags
(
    id INTEGER primary key AUTOINCREMENT,
    parent int fk,
    tag varchar(255) UNIQUE,
    valid int(1),

    CONSTRAINT fk_parent FOREIGN KEY (parent) REFERENCES tags(id)
);