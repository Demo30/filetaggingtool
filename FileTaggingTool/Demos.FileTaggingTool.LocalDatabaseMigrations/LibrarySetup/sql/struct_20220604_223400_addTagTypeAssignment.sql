ALTER TABLE files2tags RENAME TO _files2tags_old;

CREATE TABLE "files2tags" (
      "id"	INTEGER,
      "fid"	int fk,
      "tid"	int fk,
      "assignment_type" smallint not null,
      CONSTRAINT "fk_tags" FOREIGN KEY("tid") REFERENCES "tags"("id") ON DELETE CASCADE,
      CONSTRAINT "fk_files" FOREIGN KEY("fid") REFERENCES "files"("id") ON DELETE CASCADE,
      PRIMARY KEY("id" AUTOINCREMENT),
      CONSTRAINT "unique_composite1" UNIQUE("fid","tid","assignment_type")
);

INSERT INTO files2tags (id, fid, tid, assignment_type)
SELECT id, fid, tid, 1
FROM _files2tags_old;

DROP TABLE _files2tags_old;