INSERT INTO
	meta (
		databaseName, 
		databaseCreationDate,
		lastDataModification
	)
	values (
		'FILE_TAGGER',
		date('now'),
		date('now')
)