﻿using System.Globalization;
using System.Resources;
using Demos.DatabaseMigrations;

namespace Demos.FileTaggingTool.LocalDatabaseMigrations.LibrarySetup
{
    public class LibraryDatabaseMigrations : DatabaseMigrationManager
    {
        private const string DATABASE_NAME = "Database.sqlite";

        public override void LoadResources()
        {
            var resourceSet = Migrations.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            LoadMigrationsFromResource(resourceSet);
        }

        public override string GetDatabaseFileName()
        {
            return DATABASE_NAME;
        }
    }
}
