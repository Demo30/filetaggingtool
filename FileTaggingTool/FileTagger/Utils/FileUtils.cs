using System.IO;

namespace Demos.FileTagger.Utils
{
    public class FileUtils
    {
        public static void RemoveReadonlyAttributeIfPresent(string filePath)
        {
            var atr = System.IO.File.GetAttributes(filePath);
            if ((atr & FileAttributes.ReadOnly) != FileAttributes.ReadOnly)
            {
                return;
            }
            atr &= ~FileAttributes.ReadOnly;
            System.IO.File.SetAttributes(filePath, atr);
        }
    }
}