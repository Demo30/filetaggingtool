using System;
using System.Text;

namespace Demos.FileTagger.Utils
{
    public class SqlUtils
    {
        #warning this should be part of DemosHelpers + it needs to create binding placeholders and then bind params, not to directly inject the values... // TODO TW
        public static string GetMultipleSqlValues(int[] ids, string wrappings)
        {
            var sb = new StringBuilder();

            var delimiter = ", ";
            for (var i = 0; i < ids.Length; i++)
            {
                if (i == ids.Length - 1) { delimiter = ""; }
                sb.Append(string.Format("{1}{0}{1}{2}", ids[i], wrappings, delimiter));
            }

            return sb.ToString();
        }
    }
}