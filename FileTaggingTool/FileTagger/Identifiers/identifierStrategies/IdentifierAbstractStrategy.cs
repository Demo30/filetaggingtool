﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.FileTagger
{
    public abstract class IdentifierAbstractStrategy : IIdentifierStrategy
    {
        public int IdentifierLength { get { return this.IdentifierPrefix.Length + this.IdentifierLengthCore + this.IdentifierSuffix.Length; } }
        public abstract int IdentifierLengthCore { get; }
        public abstract string IdentifierPrefix { get; }
        public abstract string IdentifierSuffix { get; }
        public abstract char[] IdentifierBuildingChars { get; }

        public abstract string ConcatIdentifierWithFileName(string identifier, string filename);
        public abstract string ExtractIdentifierFromFileInfo(FileInfo fileInfo, bool withPrefixSuffix, bool exceptionWhenNotFound = false);
        public abstract string GetNewFileIdentifier(string[] existingIdentifiers);
        public abstract bool IsValidIdentifierFormat(string value, bool withPrefixAndSuffix);
        public abstract string CleanFileName(System.IO.FileInfo fileInfo);

        public string GetIdentifierCoreFromFullIdentifier(string fullIdentifier)
        {
            if (this.IsValidIdentifierFormat(fullIdentifier, true))
            {
                return fullIdentifier.Substring(this.IdentifierPrefix.Length, this.IdentifierLengthCore);
            }
            else
            {
                throw new ArgumentException($"Supplied identifier \"{fullIdentifier}\" is invalid.");
            }
        }

        protected bool StringConsistsOfBuildingCharacters(string value)
        {
            char[] chars = this.IdentifierBuildingChars;
            foreach (char c in value)
            {
                if (!chars.Contains(c))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
