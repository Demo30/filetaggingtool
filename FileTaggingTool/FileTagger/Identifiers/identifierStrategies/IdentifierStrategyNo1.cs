﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.FileTagger
{
    public class IdentifierStrategyNo1 : IdentifierAbstractStrategy
    {
        public override int IdentifierLengthCore { get { return 5; } }
        public override string IdentifierPrefix { get { return ""; } }
        public override string IdentifierSuffix { get { return "_"; } }
        public override char[] IdentifierBuildingChars
        {
            get
            {
                char[] chars = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
                return chars;
            }
        }

        private Random _random = new Random();

        public override bool IsValidIdentifierFormat(string value, bool withPrefixAndSuffix)
        {
            if (String.IsNullOrEmpty(value))
            {
                return false;
            }
            else
            {
                if (withPrefixAndSuffix)
                {
                    return
                        (value.Length == this.IdentifierLength) &&
                        (this.StringConsistsOfBuildingCharacters(value.Substring(this.IdentifierPrefix.Length, this.IdentifierLengthCore))) &&
                        (value.Substring(0, this.IdentifierPrefix.Length) == this.IdentifierPrefix) &&
                        (value.Substring(this.IdentifierLength - this.IdentifierSuffix.Length, this.IdentifierSuffix.Length) == this.IdentifierSuffix);
                }
                else
                {
                    return
                        (value.Length == this.IdentifierLengthCore) &&
                        (this.StringConsistsOfBuildingCharacters(value));
                }
            }
        }

        public override string ConcatIdentifierWithFileName(string identifier, string filename)
        {
            return this.IdentifierPrefix + identifier + this.IdentifierSuffix + filename;
        }

        public override string ExtractIdentifierFromFileInfo(FileInfo fileInfo, bool withPrefixSuffix, bool exceptionWhenNotFound = false)
        {
            string fileNameWithExtension = fileInfo.Name;
            string identifier = null;
            if (fileNameWithExtension.Length >= this.IdentifierLength)
            {
                identifier = fileNameWithExtension.Substring(0, this.IdentifierLength);
            }
            
            if (this.IsValidIdentifierFormat(identifier, true))
            {
                identifier = withPrefixSuffix ?
                    fileNameWithExtension.Substring(0, this.IdentifierLength) :
                    fileNameWithExtension.Substring(this.IdentifierPrefix.Length, this.IdentifierLengthCore);
                return identifier;
            }
            else
            {
                if (exceptionWhenNotFound)
                {
                    throw new Exception($"Valid identifier not recognized in file: \"{fileNameWithExtension}\"");
                }
                else
                {
                    return null;
                }
            }
        }

        public override string GetNewFileIdentifier(string[] existingIdentifiers)
        {
            string result = "";
            while (String.IsNullOrEmpty(result))
            {
                StringBuilder sb = new StringBuilder();

                int lengthOfFinalIdentifier = this.IdentifierLengthCore; // 26^5 = 11.8m potential files, that seems reasonable limitation enough
                char[] chars = this.IdentifierBuildingChars;
                for (int i = 0; i < lengthOfFinalIdentifier; i++)
                {
                    int rnindex = this._random.Next(0, chars.Length);
                    char rnchr = chars[rnindex];
                    sb.Append(rnchr);
                }

                if (existingIdentifiers.Contains(sb.ToString()))
                {
                    sb.Clear();
                }
                else
                {
                    result = sb.ToString();
                }
            }
            return result;
        }

        public override string CleanFileName(FileInfo fileInfo)
        {
            return fileInfo.Name;
        }
    }
}
