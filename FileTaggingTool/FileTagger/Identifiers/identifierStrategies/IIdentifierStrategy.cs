﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.FileTagger
{
    public interface IIdentifierStrategy
    {
        int IdentifierLength { get; }
        int IdentifierLengthCore { get; }
        string IdentifierPrefix { get; }
        string IdentifierSuffix { get; }
        char[] IdentifierBuildingChars { get; }

        bool IsValidIdentifierFormat(string value, bool withPrefixAndSuffix);
        string GetNewFileIdentifier(string[] existingIdentifiers);
        string ExtractIdentifierFromFileInfo(System.IO.FileInfo fileInfo, bool withPrefixSuffix, bool exceptionWhenNotFound = false);
        string CleanFileName(System.IO.FileInfo fileInfo);
        string ConcatIdentifierWithFileName(string identifier, string filename);
        string GetIdentifierCoreFromFullIdentifier(string fullIdentifier);
    }
}
