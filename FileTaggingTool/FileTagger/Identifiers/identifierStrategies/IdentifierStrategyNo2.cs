﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.FileTagger
{
    public class IdentifierStrategyNo2 : IdentifierAbstractStrategy
    {
        public override int IdentifierLengthCore { get { return 5; } }
        public override string IdentifierPrefix { get { return "._"; } }
        public override string IdentifierSuffix { get { return ""; } }
        public override char[] IdentifierBuildingChars { get {
            char[] chars = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            return chars;
        } }

        private Random _random = new Random();

        public override bool IsValidIdentifierFormat(string value, bool withPrefixAndSuffix)
        {
            if (String.IsNullOrEmpty(value))
            {
                return false;
            }
            else
            {
                if (withPrefixAndSuffix)
                {
                    if (value.Length == this.IdentifierLength)
                    {
                        string prefixPart = value.Substring(0, this.IdentifierPrefix.Length);
                        string corePart = value.Substring(this.IdentifierPrefix.Length, this.IdentifierLengthCore);
                        string suffixPart = value.Substring(this.IdentifierPrefix.Length + this.IdentifierLengthCore, this.IdentifierSuffix.Length);
                        bool valid =
                            (prefixPart == this.IdentifierPrefix) &&
                            (corePart.Length == this.IdentifierLengthCore) &&
                            (this.StringConsistsOfBuildingCharacters(corePart)) &&
                            (suffixPart == this.IdentifierSuffix);
                        return valid;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return
                        (value.Length == this.IdentifierLengthCore) &&
                        (this.StringConsistsOfBuildingCharacters(value));
                }
            }
        }

        public override string ConcatIdentifierWithFileName(string identifier, string filename)
        {
            return filename + this.IdentifierPrefix + identifier + this.IdentifierSuffix;
        }

        public override string ExtractIdentifierFromFileInfo(FileInfo fileInfo, bool withPrefixSuffix, bool exceptionWhenNotFound = false)
        {
            string fileNameWithExtension = fileInfo.Name;
            string identifier = this.ExtractIdentifierFromFileNameRecursive(fileNameWithExtension, withPrefixSuffix);
            if (!String.IsNullOrEmpty(identifier))
            {
                return identifier;
            }
            else
            {
                if (exceptionWhenNotFound)
                {
                    throw new ArgumentException($"Filename \"{fileNameWithExtension}\" does not seem to contain any identifier.");
                }
                else
                {
                    return null;
                }
            }
        }

        public override string GetNewFileIdentifier(string[] existingIdentifiers)
        {
            string result = "";
            while (String.IsNullOrEmpty(result))
            {
                StringBuilder sb = new StringBuilder();

                int lengthOfFinalIdentifier = this.IdentifierLengthCore; // 26^5 = 11.8m potential files, that seems reasonable limitation enough
                char[] chars = this.IdentifierBuildingChars;
                for (int i = 0; i < lengthOfFinalIdentifier; i++)
                {
                    int rnindex = this._random.Next(0, chars.Length);
                    char rnchr = chars[rnindex];
                    sb.Append(rnchr);
                }

                if (existingIdentifiers.Contains(sb.ToString()))
                {
                    sb.Clear();
                }
                else
                {
                    result = sb.ToString();
                }
            }
            return result;
        }

        public override string CleanFileName(FileInfo fileInfo)
        {
            string identifier = this.ExtractIdentifierFromFileInfo(fileInfo, true);
            string fileExtension = FileRenamer.ExtractFileExtension(fileInfo, this);
            if (!String.IsNullOrEmpty(identifier))
            {
                string fileName = fileInfo.Name;
                int identifierPos = fileName.IndexOf(identifier);
                string fileNameBeforeIdentifier = fileName.Substring(0, identifierPos + this.IdentifierLength);
                fileName = String.Concat(fileNameBeforeIdentifier, fileExtension);
                if (fileInfo.Exists && fileName != fileInfo.Name)
                {
                    string newFullName = String.Concat(fileInfo.FullName.Substring(0, fileInfo.FullName.Length - fileInfo.Name.Length), fileName);
                    File.Move(fileInfo.FullName, newFullName);
                }
                return fileName;
            }
            else
            {
                return fileInfo.Name;
            }
        }

        private string ExtractIdentifierFromFileNameRecursive(string fileNameWithExtension, bool withPrefixSuffix)
        {
            if (!String.IsNullOrEmpty(this.IdentifierPrefix))
            {
                int i = fileNameWithExtension.IndexOf(this.IdentifierPrefix);
                if (i == -1)
                {
                    return null;
                }
                else if (fileNameWithExtension.Length >= i + this.IdentifierLength)
                {
                    string possibleIdentifier = fileNameWithExtension.Substring(i, this.IdentifierLength);
                    if (this.IsValidIdentifierFormat(possibleIdentifier, true))
                    {
                        if (withPrefixSuffix)
                        {
                            return possibleIdentifier;
                        }
                        else
                        {
                            return possibleIdentifier.Substring(this.IdentifierPrefix.Length, this.IdentifierLengthCore);
                        }
                    }
                    else // try extraction recursively for cases when some other possible matching prefix exists in filename
                    {
                        int iAfterOccurence = i + 1;
                        if (fileNameWithExtension.Length >= iAfterOccurence)
                        {
                            string remainingPart = fileNameWithExtension.Substring(iAfterOccurence, (fileNameWithExtension.Length - iAfterOccurence));
                            return this.ExtractIdentifierFromFileNameRecursive(remainingPart, withPrefixSuffix);
                        }
                        else
                        {
                            return this.ExtractIdentifierFromFileNameRecursive("", withPrefixSuffix);
                        }
                    }
                }
                else
                {
                    return this.ExtractIdentifierFromFileNameRecursive("", withPrefixSuffix);
                }
            }
            else
            {
                // could extend the same logic to depent on prefix || suffix instead of prefix only...
                throw new Exception("Identifier extraction method requires non-empty identifier prefix.");
            }
        }

    }
}
