﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.FileTagger
{
    internal class IdentifierManager
    { 
        public IIdentifierStrategy IdentifierStrategy { get; set; }

        public IdentifierManager(IIdentifierStrategy identifierStrategy)
        {
            this.IdentifierStrategy = identifierStrategy;
        }

    }
}
