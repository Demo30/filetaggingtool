﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Data.SQLite;
using System.Data;
using Demos.FileTagger.Exceptions;
using Demos.FileTaggingTool.LocalDatabaseMigrations.LibrarySetup;

namespace Demos.FileTagger
{
    internal class LibrarySetup
    {
        public const string STRUCTURE_LIBRARYDATA = "ApplicationData";
        public const string STRUCTURE_USERDATA = "UserData";
        public const string STRUCTURE_DBNAME = "FILE_TAGGER";
        public const string STRUCTURE_DBFILENAME = "Database.sqlite";
        public const string DATABASE_COLUMN_COMMON_ID = "id";
        public const string DATABASE_COLUMN_COMMON_VALID = "valid";
        public const string DATABASE_COLUMN_FILES_ORIGINALFILE = "originalFileName";
        public const string DATABASE_COLUMN_FILES_ORIGINALFILEPATH = "originalFilePath";
        public const string DATABASE_COLUMN_FILES_IDENTIFIER = "identifier";
        public const string DATABASE_COLUMN_FILES_FILEEXTENSION = "fileExtension";
        public const string DATABASE_COLUMN_FILES_USERFILENAME = "userFilename";
        public const string DATABASE_COLUMN_FILES_CURRENTPATH = "currentFilePath";
        public const string DATABASE_COLUMN_TAGS_TAGTEXT = "tag";
        public const string DATABASE_COLUMN_TAGS_TAGPARENT = "parent";
        public const string DATABASE_COLUMN_FILE2TAGS_ASSIGNMENT_TYPE = "assignment_type";

        private const string STRUCTURE_DBSTRUCTURE_VERSION = "1";

        internal event EventHandler<MessageEventArgs> LibraryOpened;
        internal event EventHandler<MessageEventArgs> NewLibraryNameSet;
        internal event EventHandler<MessageEventArgs> NewLibraryCreated;

        internal string OpenedLibraryFolder => _openedLibraryFolder;
        internal string OpenedLibraryDatabaseFilePath => string.Concat(OpenedLibraryFolder, '\\', STRUCTURE_LIBRARYDATA, '\\', STRUCTURE_DBFILENAME);
        internal string OpenedLibraryUserDataDirPath => string.Concat(OpenedLibraryFolder, '\\', STRUCTURE_USERDATA);

        internal bool LastMigrationSuccessful { get; private set; }

        private readonly LibraryDatabaseMigrations _libraryDatabaseMigrations = new LibraryDatabaseMigrations();
        
        internal string NewLibraryName
        {
            get => _newLibraryName;
            set
            {
                _newLibraryName = value;
                NewLibraryNameSet?.Invoke(this, new MessageEventArgs { Message = _newLibraryName });
            }
        }
        private string _openedLibraryFolder = null;
        private string _newLibraryName = null;

        public void OpenLibraryFolder(string libraryFolderPath)
        {
            if (!IsLibraryFolderPathValid(libraryFolderPath))
            {
                throw new InvalidFolderPath($"Library folder path \"{libraryFolderPath}\" is not valid.");
            }

            _openedLibraryFolder = libraryFolderPath;
            LibraryOpened?.Invoke(this, new MessageEventArgs { Message = _openedLibraryFolder });
        }

        public void SetupNewLibraryFolder(string libraryFolderDest)
        {
            var isSuccess = false;
            NewLibraryChecks(libraryFolderDest, NewLibraryName);
            var mainDirFullPath = GetFullPath(libraryFolderDest, NewLibraryName);

            Directory.CreateDirectory(mainDirFullPath);

            try
            {
                Directory.CreateDirectory(string.Concat(mainDirFullPath, '\\', STRUCTURE_LIBRARYDATA));
                Directory.CreateDirectory(string.Concat(mainDirFullPath, '\\', STRUCTURE_USERDATA));
                var libDataPath = string.Concat(mainDirFullPath, '\\', STRUCTURE_LIBRARYDATA);
                var dbFullPath = string.Concat(libDataPath, '\\', STRUCTURE_DBFILENAME);

                SQLiteConnection.CreateFile(dbFullPath);

                using (var connection = GetLibraryDatabase(dbFullPath))
                {
                    connection.Open();

                    _libraryDatabaseMigrations.InitializeMigrationTable(connection);
                    
                    _libraryDatabaseMigrations.LoadResources();
                    _libraryDatabaseMigrations.UpdateMigrations(new []
                    {
                        DatabaseMigrations.MIGRATION_TYPES.BASIC_DATA,
                        DatabaseMigrations.MIGRATION_TYPES.DUMMY_DATA,
                        DatabaseMigrations.MIGRATION_TYPES.STRUCTURE
                    }, connection);
                }

                isSuccess = true;
            }
            catch(Exception)
            {
                // Clean-up on fail.
                Directory.Delete(mainDirFullPath, true);
                throw new Exception($"New library (\"{mainDirFullPath}\") failed to be created.");
            }
            finally
            {
                if (isSuccess)
                {
                    NewLibraryCreated?.Invoke(this, new MessageEventArgs { Message = mainDirFullPath });
                }
            }
        }

        public void UpdateDbStructure()
        {
            var libraryMigration = new LibraryDatabaseMigrations();

            using (var conn = GetLibraryDatabase(string.Empty))
            {
                conn.Open();
                try
                {
                    libraryMigration.LoadResources();
                    libraryMigration.UpdateMigrations(new []
                    {
                        DatabaseMigrations.MIGRATION_TYPES.BASIC_DATA,
                        DatabaseMigrations.MIGRATION_TYPES.DUMMY_DATA,
                        DatabaseMigrations.MIGRATION_TYPES.STRUCTURE
                    }, conn);
                    LastMigrationSuccessful = true;
                }
                catch
                {
                    LastMigrationSuccessful = false;
                }
            }
        }

        private void NewLibraryChecks(string libraryFolderDest, string libraryFolderName)
        {
            var cond1 = !Directory.Exists(libraryFolderDest);
            var cond2 = Directory.Exists(GetFullPath(libraryFolderDest, libraryFolderName));

            if (cond1) { throw new InvalidFolderPath("Destination folder does not exists."); }

            if (cond2) { throw new InvalidFolderPath("Folder already exists. Pick an unused path."); }
        }

        public static bool IsLibraryFolderPathValid(string libraryFolderPath)
        {
            var failConditions = new List<Func<bool>>
            {
                () => string.IsNullOrEmpty(libraryFolderPath),
                () => !Directory.Exists(string.Concat(libraryFolderPath, '\\', STRUCTURE_LIBRARYDATA)),
                () => !Directory.Exists(string.Concat(libraryFolderPath, '\\', STRUCTURE_USERDATA)),
                () => !File.Exists(string.Concat(libraryFolderPath, '\\', STRUCTURE_LIBRARYDATA, '\\', STRUCTURE_DBFILENAME))
            };

            return failConditions.Any(condition => !condition());
        }

        #region Supportive

        private string GetFullPath(string libraryFolderDest, string libraryFolderName)
        {
            if (libraryFolderDest.Last() == '\\' || libraryFolderDest.Last() == '/')
            {
                libraryFolderDest = libraryFolderDest.Remove(libraryFolderDest.Length - 1, 1);
            }

            var fullPath = string.Concat(libraryFolderDest, '\\', libraryFolderName);
            return fullPath;
        }

        private IDbConnection GetLibraryDatabase(string pathToDatabaseFile = "")
        {
            pathToDatabaseFile = string.IsNullOrEmpty(pathToDatabaseFile) ? OpenedLibraryDatabaseFilePath : pathToDatabaseFile;
            return new SQLiteConnection($"Data Source={pathToDatabaseFile};Version=3;");
        }

        #endregion
    }
}
