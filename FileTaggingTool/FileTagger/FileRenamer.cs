﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;

namespace Demos.FileTagger
{
    internal class FileRenamer
    {
        public const string KEEP_ORIG_EXTENSION = "KEEP_ORIG_EXTENSION";

        private IdentifierManager IdentifierManager { get { return Manager.Instance.IdentifierManager; } }
        private Manager _manager = null;

        public FileRenamer(Manager manager)
        {
            if (manager == null)
            {
                throw new ArgumentNullException();
            }
            this._manager = manager;
        }

        public void ExecuteBatchRename(OriginalFileNewInfoPairing[] OriginalFilesWithNewIdentifiers)
        {
            if (OriginalFilesWithNewIdentifiers == null || OriginalFilesWithNewIdentifiers.Count() == 0) { return; }

#warning issue that should be fixed - error when file with same name already exists which happens in an unlikely situation when a file with future identifier is added

            for (int i = 0; i < OriginalFilesWithNewIdentifiers.Length; i++)
            {
                OriginalFileNewInfoPairing curPair = OriginalFilesWithNewIdentifiers[i];

                if (!curPair.IdentifierKnown)
                {
                    FileInfo originalFile = curPair.OriginalFile;
                    string oldFilePath = originalFile.FullName;

                    string newFullName = string.Concat(
                        curPair.OriginalFile.Directory.FullName, "\\",
                        curPair.NewFileName,                        
                        curPair.FileExtension);

                    File.Move(oldFilePath, newFullName);
                }
            }
        }

        public void RenameFile(int fileID, string newName)
        {
            this.RenameFile(fileID, newName, FileRenamer.KEEP_ORIG_EXTENSION);
        }

        public void RenameFile(int fileID, string newName, string newExtension = FileRenamer.KEEP_ORIG_EXTENSION)
        {
            ORM.File file = ORM.File.Initialize(fileID);

            string lib = Manager.Instance.OpenedLibraryUserDataDirPath;
            string oldFullPath = file.GetFullFinalFilepath(lib);

            FileInfo fi = new FileInfo(oldFullPath);

            if (fi.Exists)
            {
                file.UserFilename = newName;
                if (newExtension != FileRenamer.KEEP_ORIG_EXTENSION)
                {
                    file.FileExtension = newExtension;
                }
                string newFullPath = file.GetFullFinalFilepath(this._manager.OpenedLibraryUserDataDirPath);
                File.Move(fi.FullName, newFullPath);
            }
            else
            {
                throw new Exception();
            }
        }

        public bool IsFileNameValid(string filename, bool allowBackSlash = false)
        {
            var problem1 = string.IsNullOrEmpty(filename);
            var problem2 = filename?.Length >= 255;

            var invalidChars = Path.GetInvalidFileNameChars();
            if (allowBackSlash)
            {
                var temp = invalidChars.ToList();
                temp.Remove('\\');
                invalidChars = temp.ToArray();
            }

            var problem3 = filename?.IndexOfAny(invalidChars) > 0;

            return !problem1 && !problem2 && !problem3;
        }

        #region Static helpers

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <param name="withIdentifier"></param>
        /// <param name="withExtension">Compound extensions (filename.xx.yy.zz) are considered to be extensions</param>
        /// <returns></returns>
        public static string ExtractFileName(System.IO.FileInfo fileInfo, bool withIdentifier, bool withExtension, IIdentifierStrategy identifierStrategy)
        {
            var fileName = fileInfo.Name;

            var identifier = identifierStrategy.ExtractIdentifierFromFileInfo(fileInfo, true, false);
            var fileExtension = ExtractFileExtension(fileInfo, identifierStrategy);

            string RemoveIdentifier(string fn) => !string.IsNullOrEmpty(identifier) ? fn.Replace(identifier, "") : fn;
            string RemoveExtension(string fn) => fn.Substring(0, fn.Length - fileExtension.Length);

            fileName = identifierStrategy.CleanFileName(fileInfo);
            switch (withIdentifier)
            {
                case true when withExtension:
                    return fileName;
                case true:
                    return RemoveExtension(fileName);
                case false when withExtension:
                    return RemoveIdentifier(fileName);
                default:
                    return RemoveIdentifier(RemoveExtension(fileName)); // in this order since some '.' chars could lead identifier
            }
        }

        internal static string ExtractFileExtension(FileInfo fileInfo, IIdentifierStrategy identifierStrategy)
        {
            var fullName = fileInfo.Name;

            /* If identifier exists, get rid of it and everything before it. */
            var identifier = identifierStrategy.ExtractIdentifierFromFileInfo(fileInfo, true, false);
            if (!string.IsNullOrEmpty(identifier)) // TODO TW: ??
            {
                var i = fileInfo.Name.IndexOf(identifier) + identifierStrategy.IdentifierLength;
                fullName = fullName.Substring(i, fullName.Length - i);
            }

            return fileInfo.Extension;
        }

        public IdentifierChangeResult[] ChangeIdentifiers(string rootDir, IIdentifierStrategy originalStrategy, IIdentifierStrategy destinationStrategy)
        {
            var results = new List<IdentifierChangeResult>();
            var resetStrategy = Manager.Instance.IdentifierStrategy;
            try
            {
                Manager.Instance.IdentifierStrategy = destinationStrategy;
                var di = new DirectoryInfo(rootDir);
                if (di.Exists && Manager.IsPathWithinOpenedLibPath(rootDir))
                {
                    var data = new List<ORM.File>();
                    var allFiles = di.GetFiles("*", SearchOption.AllDirectories);
                    foreach (var fi in allFiles)
                    {
                        var result = new IdentifierChangeResult();
                        try
                        {
                            result.FileInstance = fi;
                            result.State = IdentifierChangeResult.States.NO_CHANGE;

                            var identifier = originalStrategy.ExtractIdentifierFromFileInfo(fi, false, false);
                            if (!string.IsNullOrEmpty(identifier))
                            {
                                var fileModel = Manager.Instance.GetFileByIdentifier(identifier, false);
                                if (fileModel != null) // disqualifies a falsely identified identifier
                                {
                                    data.Add(fileModel);

                                    var newFullPath = fileModel.GetFullFinalFilepath(Manager.Instance.OpenedLibraryUserDataDirPath);
                                    File.Move(fi.FullName, newFullPath);
                                    result.State = IdentifierChangeResult.States.CHANGE_SUCCESSFUL;
                                }
                                else
                                {
                                    result.Message = $"No change performed. Identifier: \"{identifier}\" not found among known identifiers.";
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            result.State = IdentifierChangeResult.States.CHANGE_FAILED;
                            result.Message = ex.Message;
                        }
                        finally
                        {
                            results.Add(result);
                        }
                    }
                    var tm = new TagManager();
                    tm.UpdateFiles(data.ToArray());
                }
                else
                {
                    throw new Exception("Library must be opened and specified root dir path must be a directory within user data path.");
                }
            }
            finally
            {
                Manager.Instance.IdentifierManager.IdentifierStrategy = resetStrategy;
            }

            return results.ToArray();
        }

        #endregion

        #region supportive methods


        #endregion
    }
}