﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.FileTagger.Exceptions
{
    public class InvalidFolderPath : Exception
    {
        public InvalidFolderPath() : base ()
        {

        }

        public InvalidFolderPath(string message) : base (message)
        {

        }
    }
}
