﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Demos.FileTagger
{
    internal class OriginalFileNewInfoPairing
    {
        public FileInfo OriginalFile { get; set; }
        public bool IdentifierKnown { get; set; }
        public bool? IdentifierValid { get; set; }
        public string FileExtension { get { return FileRenamer.ExtractFileExtension(OriginalFile, this._identifierStrategy); } }
        public string NewIdentifier { get; set; }
        public string NewFileName { get; set; }

        private IIdentifierStrategy _identifierStrategy;

        public OriginalFileNewInfoPairing(IIdentifierStrategy identifierStrategy)
        {
            if (identifierStrategy != null)
            {
                this._identifierStrategy = identifierStrategy;
            }
            else
            {
                throw new ArgumentNullException();
            }
            
        }
    }
}
