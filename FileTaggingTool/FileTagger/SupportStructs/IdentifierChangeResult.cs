﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Demos.FileTagger
{
    public class IdentifierChangeResult
    {
        public enum States
        {
            CHANGE_SUCCESSFUL,
            CHANGE_FAILED,
            NO_CHANGE,
        }

        public System.IO.FileInfo FileInstance { get; set; }
        public string Message { get; set; }
        public States State { get; set; }

        public static void SaveToFile(string filePath, IdentifierChangeResult[] results)
        {
            // manual serialization...

            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            int tracker = 0;
            foreach(IdentifierChangeResult result in results)
            {
                if (tracker > 0)
                {
                    sb.Append(", " + Environment.NewLine);
                }
                sb.Append("{" + Environment.NewLine);
                sb.Append($"File: \"{result.FileInstance.FullName}\"" + Environment.NewLine);
                sb.Append($"State: \"{result.State.ToString()}\"" + Environment.NewLine);
                sb.Append($"Message: \"{result.Message}\"" + Environment.NewLine);
                sb.Append("}" + Environment.NewLine);
                tracker++;
            }
            sb.Append("]");
            using (System.IO.FileStream fs = new System.IO.FileStream(filePath, System.IO.FileMode.OpenOrCreate))
            {
                byte[] bytes = Encoding.UTF8.GetBytes(sb.ToString());
                fs.Write(bytes, 0, bytes.Length);
            }
        }
    }
}
