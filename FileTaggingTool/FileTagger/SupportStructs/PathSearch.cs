namespace Demos.FileTagger.SupportStructs;

public class PathSearch
{
    public string Path { get; }
    public bool IncludeSubDirs { get; }

    public PathSearch(string path, bool includeSubDirs)
    {
        Path = string.IsNullOrEmpty(path)
            ? string.Empty
            : path.StartsWith("\\") ? path : string.Concat("\\", path);
        IncludeSubDirs = includeSubDirs;
    }
}