﻿using System.Reflection;
using Demos.DemosHelpers;


namespace Demos.FileTagger
{
    public static class FileTaggerMeta
    {
        public static Versioning Version
        {
            get
            {
                string versionString = Assembly.GetExecutingAssembly()
                    .GetName()
                    .Version
                    .ToString();
                Versioning versioning = new Versioning();
                versioning.LoadVersion(versionString);
                return versioning;
            }
        }
    }
}
