﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Demos.FileTagger
{
    public class FileProcessingReportEventArgs : EventArgs
    {
        public enum Stages
        {
            ProcessingStarted,
            InitializingFileRecords,
            RenamingFiles,
            InvalidatingFileRecords,
            CompletedSuccessfully,
            CompletedByFailure
        }

        public Stages Stage { get; set; }
    }
}
