﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Demos.DemosHelpers;
using Demos.FileTagger.ORM;
using Demos.FileTagger.SupportStructs;
using File = Demos.FileTagger.ORM.File;

namespace Demos.FileTagger
{
    public class Manager
    {
        #region  Singleton initialization
        public static Manager Instance => _instance ?? (_instance = new Manager());
        private static Manager _instance = null;
        private Manager() { Initialization(); }
        #endregion

        #region public constants
        public const string STRUCTURE_LIBRARYDATA = LibrarySetup.STRUCTURE_LIBRARYDATA;
        public const string STRUCTURE_USERDATA = LibrarySetup.STRUCTURE_USERDATA;
        public const string DATABASE_COLUMN_COMMON_ID = LibrarySetup.DATABASE_COLUMN_COMMON_ID;
        public const string DATABASE_COLUMN_COMMON_VALIDITYSTATUS = LibrarySetup.DATABASE_COLUMN_COMMON_VALID;
        public const string DATABASE_COLUMN_FILES_ORIGINALFILE = LibrarySetup.DATABASE_COLUMN_FILES_ORIGINALFILE;
        public const string DATABASE_COLUMN_FILES_ORIGINALFILEPATH = LibrarySetup.DATABASE_COLUMN_FILES_ORIGINALFILEPATH;
        public const string DATABASE_COLUMN_FILES_IDENTIFIER = LibrarySetup.DATABASE_COLUMN_FILES_IDENTIFIER;
        public const string DATABASE_COLUMN_FILES_FILEEXTENSION = LibrarySetup.DATABASE_COLUMN_FILES_FILEEXTENSION;
        #endregion

        internal IdentifierManager IdentifierManager { get; set; }

        public IIdentifierStrategy IdentifierStrategy
        {
            get => IdentifierManager.IdentifierStrategy;
            set => IdentifierManager.IdentifierStrategy = value;
        }

        public string OpenedLibraryFolder => _librarySetup.OpenedLibraryFolder;
        public string OpenedLibraryDatabaseFilePath => _librarySetup.OpenedLibraryDatabaseFilePath;
        public string OpenedLibraryUserDataDirPath => _librarySetup.OpenedLibraryUserDataDirPath;

        public bool UnsavedProgress { get; } = false;
        public event EventHandler<EventArgs> LibraryOpened = null;
        public event EventHandler<MessageEventArgs> NewLibraryNameSet;
        public event EventHandler<MessageEventArgs> NewLibraryCreated;
        public event EventHandler<FileProcessingReportEventArgs> FileProcessingProgressUpdate = null;

        private LibrarySetup _librarySetup = null;
        private FileRenamer _fileRenamer = null;
        private readonly IIdentifierStrategy _defaultStrategy = new IdentifierStrategyNo2();
        private readonly TagManager _tagManager = new TagManager();

        private void Initialization()
        {
            _librarySetup = new LibrarySetup();
            IdentifierManager = new IdentifierManager(_defaultStrategy);
            _librarySetup.LibraryOpened += (sender, args) => { LibraryOpened?.Invoke(this, args); };
            _librarySetup.NewLibraryNameSet += (sender, args) => { NewLibraryNameSet?.Invoke(this, args); };
            _librarySetup.NewLibraryCreated += (sender, args) => { NewLibraryCreated?.Invoke(this, args); };
            NewLibraryCreated += (sender, args) => { OpenLibraryFolder(args.Message); };
            _fileRenamer = new FileRenamer(this);
        }

        public void OpenLibraryFolder(string libraryFolderPath)
        {
            _librarySetup.OpenLibraryFolder(libraryFolderPath);
        }

        public void SetupNewLibraryFolder(string libraryFolderDest)
        {
            _librarySetup.SetupNewLibraryFolder(libraryFolderDest);
        }

        public string ExtractFileName(FileInfo fileInfo, bool withIdentifier, bool withExtension)
        {
            return FileRenamer.ExtractFileName(fileInfo, withExtension, withExtension, IdentifierStrategy);
        }

        public void UpdateLibraryDatabaseStructure()
        {
            if (!string.IsNullOrEmpty(OpenedLibraryFolder))
            {
                _librarySetup.UpdateDbStructure();
            }
            else
            {
                throw new InvalidOperationException("Library needs to be opened in order to update its structure.");
            }

            if (!_librarySetup.LastMigrationSuccessful)
            {
                throw new DatabaseMigrations.MigrationFailException("Update of database structure failed. One Of the migrations failed. Check the database migration records for the failed row.");
            }
        }

        public bool IsLibraryFolderPathValid(string libraryFolderPath)
        {
            return LibrarySetup.IsLibraryFolderPathValid(libraryFolderPath);
        }

        public void ProcessFilesInOpenedLibrary()
        {
            var settings = new FileEvaluationSettings()
            {
                FileSelector = new FileDirectorySelector(new DirectoryInfo(OpenedLibraryUserDataDirPath))
            };
            ProcessFiles(settings);
        }

        public void ProcessFiles(FileEvaluationSettings additionalSettings = null)
        {
            ReportFileProcessingProgress(new FileProcessingReportEventArgs { Stage = FileProcessingReportEventArgs.Stages.ProcessingStarted });
            if (additionalSettings == null)
            {
                additionalSettings = new FileEvaluationSettings();
                var dirSel = new FileDirectorySelector(new DirectoryInfo(Instance.OpenedLibraryUserDataDirPath));
                additionalSettings.FileSelector = dirSel;
            }
            var evaluator = new FileEvaluator();

            // ==============================================================================
            // ======= PART 1
            // ==============================================================================

            // #1 - Load existing identifiers (flagged) and generate new ones for each file satisfying possible restrictions

            var originalFilesWithNewIdentifiers = evaluator.EvaluateFiles(additionalSettings);
            var newIdentifiers =
                (from curPair in originalFilesWithNewIdentifiers where curPair.IdentifierKnown == false select curPair).ToArray();

            if (newIdentifiers.Any())
            {
                // #2 - Initialize new file records for cases where new identifier has been generated

                ReportFileProcessingProgress(new FileProcessingReportEventArgs() { Stage = FileProcessingReportEventArgs.Stages.InitializingFileRecords });
                _tagManager.InitializeFileRecords(newIdentifiers.ToArray());

                // #3 - When file is not known, file is renamed by prepending newly assigned identifier

                ReportFileProcessingProgress(new FileProcessingReportEventArgs() { Stage = FileProcessingReportEventArgs.Stages.RenamingFiles });
                _fileRenamer.ExecuteBatchRename(newIdentifiers);
            }

            // ==============================================================================
            // ======= PART 2 (independent of part 1)
            // ==============================================================================

            // #4 - Updating file records: 
            //          - invalidating file records that are no longer present
            //          - re-validating those that are present once more
            //          - updating file paths
            //          - updating file record names

            ReportFileProcessingProgress(new FileProcessingReportEventArgs { Stage = FileProcessingReportEventArgs.Stages.InvalidatingFileRecords });
            var updatedFileData = evaluator.ReevaluateFileRecordState(additionalSettings); // all files, not only valid ones!
            _tagManager.UpdateFiles(updatedFileData);

            ReportFileProcessingProgress(new FileProcessingReportEventArgs { Stage = FileProcessingReportEventArgs.Stages.CompletedSuccessfully });
        }

        private void ReportFileProcessingProgress(FileProcessingReportEventArgs report)
        {
            if (report == null) { throw new ArgumentNullException(); }

            if (FileProcessingProgressUpdate == null) return;
            try { FileProcessingProgressUpdate(this, report); } catch { }
        }

        internal string[] GetFileIdentifierRecords(bool validOnly)
        {
            return _tagManager.GetFileIdentifierRecords(validOnly);
        }

        public void SetNameForNewLibrary(string newLibraryName)
        {
            if (string.IsNullOrEmpty(newLibraryName))
            {
                throw new ArgumentNullException();
            }

            _librarySetup.NewLibraryName = newLibraryName;
        }

        public void DeleteFiles(IEnumerable<File> files)
        {
            var toUpdate = files.Select(f =>
            {
                f.Valid = false;
                return f;
            }).ToArray();
            _tagManager.UpdateFiles(toUpdate);
        }
        
        public string GetCurrentNameForNewLibrary()
        {
            return _librarySetup.NewLibraryName;
        }

        public long AddTag(string tag)
        {
            return _tagManager.AddTag(tag);
        }

        public void ToggleTagValidityState(int tagID, bool isValid)
        {
            _tagManager.ToggleTagValidityState(tagID, isValid);
        }

        public void RemoveTag(int tagId)
        {
            _tagManager.RemoveTag(tagId);
        }

        public void AssignTag(int tagId, int identifierId, TagAssignmentType assignmentType = TagAssignmentType.PrimaryTag)
        {
            _tagManager.AssignTag(tagId, identifierId, assignmentType);
        }
        
        public void AssignTag(IEnumerable<(int, int)> tagId2fileId, TagAssignmentType assignmentType = TagAssignmentType.PrimaryTag)
        {
            _tagManager.AssignTag(tagId2fileId, assignmentType);
        }
        
        public void DissociateTags(int tagId, IEnumerable<int> fileIds, TagAssignmentType? assignmentType)
        {
            _tagManager.DissociateTag(tagId, fileIds, assignmentType);
        }

        public void RenameTag(int tagId, string newName)
        {
            _tagManager.RenameTag(tagId, newName);
        }

        public void MoveInTagTreeStructure(int tagId, int? newParentTagId)
        {
            _tagManager.MoveInTagTreeStructure(tagId, newParentTagId);
        }

        public void SetUserFilename(int fileId, string userFilename)
        {
            if (!_fileRenamer.IsFileNameValid(userFilename, false)) return;
            _fileRenamer.RenameFile(fileId, userFilename);
            _tagManager.SetUserFilename(fileId, userFilename);
        }

        public Tag[] GetTags(bool validOnly)
        {
            return _tagManager.GetTags(validOnly);
        }

        public bool DoesTagExists(string tagHeader, bool validOnly)
        {
            var tags = _tagManager.GetTags(validOnly);
            var match = from tag in tags where string.Equals(tag.Header, tagHeader, StringComparison.CurrentCultureIgnoreCase) select tag;
            return match.Any();
        }
        
        public IEnumerable<AssignedTag> GetAssignedTags(int fileId, bool validOnly)
        {
            return GetAssignedTags(new[] {fileId}, validOnly);
        }

        public IEnumerable<AssignedTag> GetAssignedTags(IEnumerable<int> fileIds, bool validOnly)
        {
            return _tagManager.GetAssignedTags(fileIds, validOnly);
        }

        public File[] GetFiles(bool validOnly)
        {
            return _tagManager.GetFiles(validOnly);
        }

        public File GetFileByIdentifier(string identifier, bool validOnly)
        {
            return _tagManager.GetFileByIdentifier(identifier, validOnly);
        }

        public IEnumerable<File> GetFilesWithoutTags(bool validOnly)
        {
            return _tagManager.GetFilesWithoutTags(validOnly, new PathSearch(string.Empty, false));
        }
        
        public IEnumerable<File> GetFilesWithoutTags(bool validOnly, string path, bool includeSubPaths)
        {
            return _tagManager.GetFilesWithoutTags(validOnly, new PathSearch(path, includeSubPaths));
        }

        public File GetFileByIdentifier(string identifier, IEnumerable<File> bank, bool? validOnly)
        {
            IEnumerable<File> matchingFile;
            if (validOnly == true)
            {
                matchingFile = from file in bank where file.Identifier == identifier && file.Valid select file;
            }
            else
            {
                matchingFile = from file in bank where file.Identifier == identifier select file;
            }

            matchingFile = matchingFile.ToArray();
            var numOfMatches = matchingFile.Count();
            if (numOfMatches > 1)
            {
                throw new InvalidDataException($"More than one match for an identifier: {identifier}");
            }

            return numOfMatches == 0 ? null : matchingFile.First();
        }

        public IEnumerable<File> GetFilesByTags(Dictionary<int, IEnumerable<TagAssignmentType>> filteredTags, bool validOnly, SQLConditionRelationships conditionRelationships, PathSearch pathSearch, bool includeSubTags)
        {
            return _tagManager.GetFilesByTags(filteredTags, validOnly, conditionRelationships, pathSearch, includeSubTags);
        }

        public bool IsFileNameValid(string filename, bool allowBackSlash = false)
        {
            var result = _fileRenamer.IsFileNameValid(filename);
            return result;
        }

        public IdentifierChangeResult[] ChangeIdentifiers(IIdentifierStrategy originalStrategy, IIdentifierStrategy destinationStrategy)
        {
            return _fileRenamer.ChangeIdentifiers(OpenedLibraryUserDataDirPath, originalStrategy, destinationStrategy);
        }

        public static System.Data.SQLite.SQLiteConnection OpenConnection(string databaseFilePath)
        {
            var con = new System.Data.SQLite.SQLiteConnection(
                $"Data Source={databaseFilePath};Version=3;foreign keys=true;");
            con.Open();
            return con;
        }

        public static bool IsPathWithinOpenedLibPath(string path)
        {
            var userPath = Instance.OpenedLibraryUserDataDirPath;
            return
                !string.IsNullOrEmpty(userPath) &&
                path.IndexOf(Instance.OpenedLibraryUserDataDirPath, StringComparison.Ordinal) == 0;
        }
    }
}
