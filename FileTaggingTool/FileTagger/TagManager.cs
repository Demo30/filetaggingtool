﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Data.SQLite;
using Demos.DemosHelpers;
using System.Data;
using Demos.FileTagger.ORM;
using Demos.FileTagger.SupportStructs;

namespace Demos.FileTagger
{
    internal class TagManager
    {
        private string ApplicationDataPath =>
            string.Concat(Manager.Instance.OpenedLibraryFolder, '\\', Manager.STRUCTURE_LIBRARYDATA);

        private string UserDataPath =>
            string.Concat(Manager.Instance.OpenedLibraryFolder, '\\', Manager.STRUCTURE_USERDATA);

        private string DatabaseFilePath => string.Concat(ApplicationDataPath, '\\', LibrarySetup.STRUCTURE_DBFILENAME);

        internal void InitializeFileRecords(OriginalFileNewInfoPairing[] oldAndNewIdentifiers)
        {
            AddFileRecordsInDatabase(oldAndNewIdentifiers);
        }

        #region exposed methods

        internal long AddTag(string tag)
        {
            long newTagID = -1;

            if (string.IsNullOrEmpty(tag))
            {
                throw new ArgumentNullException();
            }

            using (var con = Manager.OpenConnection(DatabaseFilePath))
            {
                using (var tra = con.BeginTransaction(IsolationLevel.Serializable))
                {
                    var sql = String.Format(@"
                        INSERT INTO tags(tag, valid) VALUES ('{0}', {1})",
                        tag, 1);

                    UnifiedDatabaseHelperClass.ExecuteNonQuery(sql, con);

                    newTagID = con.LastInsertRowId;

                    tra.Commit();
                    tra.Dispose();
                }

                con.Close();
            }

            return newTagID;
        }

        internal void ToggleTagValidityState(int tagID, bool isValid)
        {
            var convertedBool = isValid ? 1 : 0;

            using (var con = Manager.OpenConnection(DatabaseFilePath))
            {
                using (var tra = con.BeginTransaction(IsolationLevel.Serializable))
                {
                    var sql = String.Format(@"
                        UPDATE tags SET valid = {1}
                        WHERE id = {0}",
                        tagID, convertedBool);

                    UnifiedDatabaseHelperClass.ExecuteNonQuery(sql, con);

                    tra.Commit();
                    tra.Dispose();
                }

                con.Close();
            }
        }

        internal void RemoveTag(int tagID)
        {
            using (var con = Manager.OpenConnection(DatabaseFilePath))
            {
                using (var tra = con.BeginTransaction(IsolationLevel.Serializable))
                {
                    var sql = String.Format(@"
                        DELETE FROM tags
                        WHERE id = {0}",
                        tagID);

                    UnifiedDatabaseHelperClass.ExecuteNonQuery(sql, con);

                    tra.Commit();
                    tra.Dispose();
                }

                con.Close();
            }
        }

        internal void AssignTag(int tagId, int identifierId,
            TagAssignmentType assignmentType = TagAssignmentType.PrimaryTag)
        {
            using (var con = Manager.OpenConnection(DatabaseFilePath))
            using (var tra = con.BeginTransaction(IsolationLevel.Serializable))
            {
                const string sql =
                    @"INSERT INTO files2tags(tid, fid, assignment_type) VALUES(@tagID, @identifierID, @assignmentType)";

                UnifiedDatabaseHelperClass.ExecuteNonQuery(sql, con, new Dictionary<string, string>
                {
                    {"tagID", tagId.ToString()},
                    {"identifierID", identifierId.ToString()},
                    {"assignmentType", ((ushort) assignmentType).ToString()}
                });

                tra.Commit();
                tra.Dispose();
                con.Close();
            }
        }

        
        internal void AssignTag(IEnumerable<(int, int)> tid2fid, TagAssignmentType assignmentType = TagAssignmentType.PrimaryTag)
        {
            using (var con = Manager.OpenConnection(DatabaseFilePath))
            using (var tra = con.BeginTransaction(IsolationLevel.Serializable))
            {

                tid2fid.ToList().ForEach(t2f =>
                {
                    const string sql = @"INSERT INTO files2tags(tid, fid, assignment_type) VALUES(@tagId, @identifierID, @assignmentType)";
                    UnifiedDatabaseHelperClass.ExecuteNonQuery(sql, con, new Dictionary<string, string>
                    {
                        { "tagId", t2f.Item1.ToString()},
                        { "identifierID", t2f.Item2.ToString()},
                        { "assignmentType", ((ushort)assignmentType).ToString()}
                    });
                });
                
                tra.Commit();
                tra.Dispose();
                con.Close();
            }
        }
        
        internal void DissociateTag(int tagId, IEnumerable<int> fileIds, TagAssignmentType? assignmentType)
        {
            using (var con = Manager.OpenConnection(DatabaseFilePath))
            using (var tra = con.BeginTransaction(IsolationLevel.Serializable))
            {
                var assignmentTypeConstraint = assignmentType is null
                    ? "1 = 1"
                    : $"{LibrarySetup.DATABASE_COLUMN_FILE2TAGS_ASSIGNMENT_TYPE} = {(int) assignmentType}";
                var sql =
                    $@"DELETE FROM files2tags WHERE {assignmentTypeConstraint} and tid = @tagId and fid in ({string.Join(", ", fileIds)})"; // TODO TW: fileIds - you should construct sql so that multiple values can be bound - too many values inside IN() needs to be considered

                UnifiedDatabaseHelperClass.ExecuteNonQuery(sql, con, new Dictionary<string, string>()
                {
                    {"tagId", tagId.ToString()},
                });

                tra.Commit();
                tra.Dispose();
                con.Close();
            }
        }

        internal void RenameTag(int tagID, string newText)
        {
            if (string.IsNullOrEmpty(newText))
            {
                throw new ArgumentNullException(nameof(newText));
            }

            var sql =
                $@"UPDATE tags SET {LibrarySetup.DATABASE_COLUMN_TAGS_TAGTEXT} = '{newText}' WHERE {Manager.DATABASE_COLUMN_COMMON_ID} = {tagID}";

            using (var con = Manager.OpenConnection(Manager.Instance.OpenedLibraryDatabaseFilePath))
            {
                UnifiedDatabaseHelperClass.ExecuteNonQuery(sql, con);
            }
        }

        internal void MoveInTagTreeStructure(int tagID, int? parentTagID)
        {
            if (tagID == parentTagID)
            {
                return;
            }

            string sql = null;

            if (parentTagID == null)
            {
                sql = $@"
                    UPDATE tags SET {LibrarySetup.DATABASE_COLUMN_TAGS_TAGPARENT} = null
                    WHERE {Manager.DATABASE_COLUMN_COMMON_ID} = '{tagID}';
                ";
            }
            else
            {
                sql = $@"
                    UPDATE tags SET {LibrarySetup.DATABASE_COLUMN_TAGS_TAGPARENT} = '{parentTagID}'
                    WHERE {Manager.DATABASE_COLUMN_COMMON_ID} = '{tagID}';
                ";
            }

            using (SQLiteConnection con = Manager.OpenConnection(Manager.Instance.OpenedLibraryDatabaseFilePath))
            {
                UnifiedDatabaseHelperClass.ExecuteNonQuery(sql, con);
            }
        }

        internal void SetUserFilename(int fileID, string newText)
        {
            if (string.IsNullOrEmpty(newText))
            {
                throw new ArgumentNullException();
            }

            var sql = $@"
                UPDATE files
                SET {LibrarySetup.DATABASE_COLUMN_FILES_USERFILENAME} = :NEW_TEXT
                WHERE {Manager.DATABASE_COLUMN_COMMON_ID} = :FILE_ID
            ";

            var queryParams = new Dictionary<string, string>()
            {
                {"NEW_TEXT", newText},
                {"FILE_ID", fileID.ToString()}
            };

            using (var con = Manager.OpenConnection(Manager.Instance.OpenedLibraryDatabaseFilePath))
            {
                UnifiedDatabaseHelperClass.ExecuteNonQuery(sql, con, queryParams);
            }
        }

        internal void SetCurrentFilepath(int fileID, string filePath)
        {
            var sql = String.Format(@"
                UPDATE files SET {2} = '{3}' WHERE {0} = {1}
            ",
                Manager.DATABASE_COLUMN_COMMON_ID, fileID,
                LibrarySetup.DATABASE_COLUMN_FILES_CURRENTPATH, filePath);

            using (var con = Manager.OpenConnection(Manager.Instance.OpenedLibraryDatabaseFilePath))
            {
                UnifiedDatabaseHelperClass.ExecuteNonQuery(sql, con);
            }
        }

        internal bool IsFileIdentifierPresent(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException();
            }

            var present = GetFileIdentifierRecords(false);

            return present.Contains(fileName);
        }

        internal void UpdateFiles(ORM.File[] fileData)
        {
            using (var con = Manager.OpenConnection(Manager.Instance.OpenedLibraryDatabaseFilePath))
            {
                var sql = $@"
                    UPDATE files SET
                        {Manager.DATABASE_COLUMN_COMMON_VALIDITYSTATUS} = :VALIDITY_STATUS,
                        {LibrarySetup.DATABASE_COLUMN_FILES_CURRENTPATH} = :CURRENT_FILE_PATH,
                        {LibrarySetup.DATABASE_COLUMN_FILES_USERFILENAME} = :USER_FILENAME,
                        {LibrarySetup.DATABASE_COLUMN_FILES_FILEEXTENSION} = :FILE_EXTENSION
                    WHERE {Manager.DATABASE_COLUMN_COMMON_ID} = :FILE_ID";

                foreach (var curFileData in fileData)
                {
                    var validValue = curFileData.Valid ? "1" : "0";

                    var queryParams = new Dictionary<string, string>()
                    {
                        {"VALIDITY_STATUS", validValue},
                        {"CURRENT_FILE_PATH", curFileData.CurrentFilePath},
                        {"USER_FILENAME", curFileData.UserFilename},
                        {"FILE_ID", curFileData.Id.ToString()},
                        {"FILE_EXTENSION", curFileData.FileExtension}
                    };

                    UnifiedDatabaseHelperClass.ExecuteNonQuery(sql, con, queryParams);
                }
            }
        }

        internal string[] GetFileIdentifierRecords(bool validOnly)
        {
            var fileRecordsPresent = new List<string>();

            using (var con = Manager.OpenConnection(DatabaseFilePath))
            {
                var sql = "Select identifier From files";
                if (validOnly)
                {
                    sql = "Select identifier From files Where valid = 1";
                }

                var data = UnifiedDatabaseHelperClass.GetResultsList(sql, con);
                for (var i = 1; i < data.Count; i++) //starting from 1, contains header
                {
                    var row = data[i];
                    var fileNameIdentifierRecord = row[0].ToString();
                    fileRecordsPresent.Add(fileNameIdentifierRecord);
                }

                con.Close();
            }

            return fileRecordsPresent.ToArray();
        }

        internal ORM.File GetFileByIdentifier(string identifier, bool validOnly)
        {
            if (string.IsNullOrEmpty(identifier))
            {
                throw new ArgumentNullException();
            }

            DataTable data = null;

            var sql = $"Select * From files Where identifier = '{identifier}' Order by identifier";
            using (var con = Manager.OpenConnection(DatabaseFilePath))
            {
                if (validOnly)
                {
                    sql =
                        $"Select * From files Where identifier = '{identifier}' and {Manager.DATABASE_COLUMN_COMMON_VALIDITYSTATUS} = 1 Order by identifier";
                }

                data = UnifiedDatabaseHelperClass.GetResultsDataTable(sql, con);

                con.Close();
            }

            var numOfRows = data.Rows.Count;

            if (numOfRows == 0)
            {
                return null;
            }

            if (numOfRows > 1)
            {
                throw new InvalidDataException($"More than one row returned for an identifier: {identifier}");
            }

            var id = data.Rows[0][Manager.DATABASE_COLUMN_COMMON_ID]
                .ToInt32(GeneralHelperClass.ToInt32ConversionTypes.IntOrException);

            var file = ORM.File.Initialize(id);
            return file;
        }

        internal IEnumerable<ORM.File> GetFilesWithoutTags(bool validOnly, PathSearch pathSearch)
        { 
            DataTable data = null;

            var sql =
                $@"Select f.id
                From files f
                Where 1 = 1
                    and not exists (Select fid From files2tags f2t Where f2t.fid = f.id)
                    and {(validOnly ? "f.valid = 1" : "1 = 1")}
                    and {GetFilePathCondition(pathSearch)}";

            using (var con = Manager.OpenConnection(DatabaseFilePath))
            {
                data = UnifiedDatabaseHelperClass.GetResultsDataTable(sql, con);
                con.Close();
            }

            var fileIds = Enumerable.Range(0, data.Rows.Count)
                .Select(rowIndex => data.Rows[rowIndex][Manager.DATABASE_COLUMN_COMMON_ID]
                    .ToInt32(GeneralHelperClass.ToInt32ConversionTypes.IntOrException))
                .ToArray();

            return fileIds.Length > 0 ? ORM.File.Initialize(fileIds) : Array.Empty<ORM.File>();
        }

        internal ORM.File[] GetFiles(bool validOnly)
        {
            DataTable data = null;

            using (var con = Manager.OpenConnection(DatabaseFilePath))
            {
                var sql = "Select * From files Order by identifier";
                if (validOnly)
                {
                    sql = String.Format("Select * From files Where {0} = 1 Order by identifier",
                        Manager.DATABASE_COLUMN_COMMON_VALIDITYSTATUS);
                }

                data = UnifiedDatabaseHelperClass.GetResultsDataTable(sql, con);

                con.Close();
            }

            var numOfRows = data.Rows.Count;

            var ids = new int[numOfRows];
            for (var i = 0; i < numOfRows; i++)
            {
                var curRow = data.Rows[i];
                var id = curRow[Manager.DATABASE_COLUMN_COMMON_ID]
                    .ToInt32(GeneralHelperClass.ToInt32ConversionTypes.IntOrException);
                ids[i] = id;
            }

            var tags = ORM.File.Initialize(ids);
            return tags;
        }

        internal IEnumerable<ORM.File> GetFilesByTags(Dictionary<int, IEnumerable<TagAssignmentType>> filteredTags,
            bool validOnly, SQLConditionRelationships conditionRelationships, PathSearch pathSearch, bool includeSubTags)
        {
            DataTable data = null;

            using (var con = Manager.OpenConnection(DatabaseFilePath))
            {
                string BuildAssignmentTypeCondition(Dictionary<int, IEnumerable<TagAssignmentType>> ft, bool includeSabTagsFlag)
                {
                    if (ft.Count == 0)
                        return "1 = 1";
                    
                    return string.Join(conditionRelationships == SQLConditionRelationships.AND ? " and " : " or ",
                        ft.Keys.Select(t =>
                            $@"exists (
                                Select fid
                                From files2tags f2t
                                {(includeSabTagsFlag ? $"Inner join subTagsRec st on (st.id = f2t.tid and st.most_parent_id = {t})" : "")}
                                Where 1 = 1
                                    and f2t.fid = f.id
                                    and f2t.assignment_type in ({string.Join(", ", ft[t].Select(at => (int) at))})
                                    and {(includeSabTagsFlag ? "1 = 1" : $"f2t.tid = {t}")}
                            )"));
                            
                }
                
                var onlyValidFilesCondition = validOnly ? $"f.{Manager.DATABASE_COLUMN_COMMON_VALIDITYSTATUS} = 1" : "1 = 1";
                
                var sql = $@"
                    WITH RECURSIVE subTagsRec(id, parent, most_parent_id, tag, valid) AS
                    (
                        SELECT tags.id, tags.parent, tags.id, tags.tag, tags.valid
                        FROM tags

                        UNION ALL

                        SELECT tags.id, tags.parent, subTagsRec.most_parent_id, tags.tag, tags.valid
                        FROM subTagsRec
                        JOIN tags ON subTagsRec.id = tags.parent
                    )

                    Select * 
                    From files f
                    Where 1 = 1
                        and {onlyValidFilesCondition}
                        and {GetFilePathCondition(pathSearch)}
                        and (
                            {BuildAssignmentTypeCondition(filteredTags, includeSubTags)}
                        )
                    Order by identifier";

                data = UnifiedDatabaseHelperClass.GetResultsDataTable(sql, con);

                con.Close();
            }

            var numOfRows = data.Rows.Count;

            var fids = new int[numOfRows];
            for (var i = 0; i < numOfRows; i++)
            {
                var curRow = data.Rows[i];
                var id = curRow[Manager.DATABASE_COLUMN_COMMON_ID]
                    .ToInt32(GeneralHelperClass.ToInt32ConversionTypes.IntOrException);
                fids[i] = id;
            }

            var files = ORM.File.Initialize(fids);
            return files;
        }

        internal Tag[] GetTags(bool validOnly)
        {
            DataTable data = null;

            using (var con = Manager.OpenConnection(DatabaseFilePath))
            {
                var validOnlyCondition = validOnly ? "valid = 1" : "1 = 1";
                var sql = $"Select * " +
                          $"From tags " +
                          $"Where {validOnlyCondition} " +
                          $"Order by {LibrarySetup.DATABASE_COLUMN_TAGS_TAGTEXT}";

                data = UnifiedDatabaseHelperClass.GetResultsDataTable(sql, con);
                con.Close();
            }

            var numOfRows = data.Rows.Count;

            var ids = new int[numOfRows];
            for (var i = 0; i < numOfRows; i++)
            {
                var curRow = data.Rows[i];
                var id = curRow[LibrarySetup.DATABASE_COLUMN_COMMON_ID]
                    .ToInt32(GeneralHelperClass.ToInt32ConversionTypes.IntOrException);
                ids[i] = id;
            }

            if (ids.Length == 0)
            {
                return Array.Empty<Tag>();
            }

            var tags = Tag.Initialize(ids);
            return tags;
        }

        internal IEnumerable<AssignedTag> GetAssignedTags(IEnumerable<int> fileIds, bool validOnly)
        {
            DataTable data = null;

            using (var con = Manager.OpenConnection(DatabaseFilePath))
            {
                var validOnlySqlWhereFragment = validOnly ? "t.valid = 1" : "1 = 1";
                var sql =
                    $"Select distinct f2t.{LibrarySetup.DATABASE_COLUMN_COMMON_ID} " +
                    $"From files2tags f2t " +
                    $"Inner join tags t on t.id = f2t.tid " +
                    $"Where {validOnlySqlWhereFragment} and f2t.fid in ({string.Join(", ", fileIds)})";

                data = UnifiedDatabaseHelperClass.GetResultsDataTable(sql, con);

                con.Close();
            }

            var numOfRows = data.Rows.Count;

            var ids = new int[numOfRows];
            for (var i = 0; i < numOfRows; i++)
            {
                var curRow = data.Rows[i];
                ids[i] = curRow[LibrarySetup.DATABASE_COLUMN_COMMON_ID]
                    .ToInt32(GeneralHelperClass.ToInt32ConversionTypes.IntOrException);
            }

            return ids.Length == 0 ? Array.Empty<AssignedTag>() : AssignedTag.Initialize(ids);
        }

        #endregion

        private void AddFileRecordsInDatabase(OriginalFileNewInfoPairing[] oldAndNewIdentifiers)
        {
            using (var con = Manager.OpenConnection(DatabaseFilePath))
            using (var tra = con.BeginTransaction(IsolationLevel.Serializable))
            {
                try
                {
                    foreach (var t in oldAndNewIdentifiers)
                    {
                        var oldFile = t.OriginalFile;
                        var oldFileName = FileRenamer.ExtractFileName(oldFile, false, false,
                            Manager.Instance.IdentifierManager.IdentifierStrategy);
                        var oldFilePath = oldFile.Directory.FullName.Replace(
                            string.Concat(Manager.Instance.OpenedLibraryFolder, '\\', Manager.STRUCTURE_USERDATA), "");
                        oldFilePath = string.IsNullOrEmpty(oldFilePath)
                            ? string.Concat(oldFilePath, '\\')
                            : oldFilePath;
                        var fileExtension = FileRenamer.ExtractFileExtension(oldFile,
                            Manager.Instance.IdentifierManager.IdentifierStrategy);
                        var newFileIdentifier = t.NewIdentifier;

                        var sql = $@"
                                INSERT INTO files
                                (
                                    {Manager.DATABASE_COLUMN_FILES_ORIGINALFILE}, 
                                    {Manager.DATABASE_COLUMN_FILES_ORIGINALFILEPATH},
                                    {LibrarySetup.DATABASE_COLUMN_FILES_CURRENTPATH},
                                    {Manager.DATABASE_COLUMN_FILES_IDENTIFIER},
                                    {Manager.DATABASE_COLUMN_FILES_FILEEXTENSION},
                                    {Manager.DATABASE_COLUMN_COMMON_VALIDITYSTATUS}
                                )
                                VALUES
                                (
                                    :OLD_FILENAME,
                                    :ORIGINAL_FILEPATH,
                                    :CURRENT_FILEPATH, -- starting with the same path
                                    :FILE_IDENTIFIER,
                                    :FILE_EXTENSION,
                                    :VALIDITY_STATUS
                                );";

                        var queryParams = new Dictionary<string, string>()
                        {
                            {"OLD_FILENAME", oldFileName},
                            {"ORIGINAL_FILEPATH", oldFilePath},
                            {"CURRENT_FILEPATH", oldFilePath},
                            {"FILE_IDENTIFIER", newFileIdentifier},
                            {"FILE_EXTENSION", fileExtension.ToLower()},
                            {"VALIDITY_STATUS", "1"}
                        };

                        UnifiedDatabaseHelperClass.ExecuteNonQuery(sql, con, queryParams);
                    }

                    tra.Commit();
                    tra.Dispose();
                }
                catch (SQLiteException ex)
                {
                    tra.Rollback();
                    throw;
                }

                con.Close();
            }
        }

        private static string GetFilePathCondition(PathSearch pathSearch)
        {
            return
                $"(f.{LibrarySetup.DATABASE_COLUMN_FILES_CURRENTPATH} = '{pathSearch.Path}' or {(pathSearch.IncludeSubDirs ? $"f.{LibrarySetup.DATABASE_COLUMN_FILES_CURRENTPATH} LIKE '{pathSearch.Path}%'" : "1 = 2")})";
        }
    }
}