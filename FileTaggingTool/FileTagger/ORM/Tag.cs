﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Demos.DemosHelpers;
using System.Data;
using System.Data.SQLite;
using Demos.FileTagger.Utils;

namespace Demos.FileTagger.ORM
{
    public class Tag
    {
        public int Id { get; protected set; }
        public string Header { get; protected  set; }
        public int? ParentTagID { get; protected  set; }
        public bool Valid { get; protected  set; }

        private const string CLMN_TAGID = "tagID";
        private const string CLMN_TAGTEXT = "tagText";
        private const string CLMN_VALIDITYSTATUS = "isValid";

        protected Tag() { }

        public static Tag Initialize(int id)
        {
            var tags = Initialize(new int[] { id });
            return tags[0];
        }

        public static Tag[] Initialize(int[] ids)
        {
            DataTable data = null;

            using (var con = Manager.OpenConnection(Manager.Instance.OpenedLibraryDatabaseFilePath))
            {
                var sql = $@"
                    Select
                        id {CLMN_TAGID},
                        tag {CLMN_TAGTEXT},
                        valid {CLMN_VALIDITYSTATUS},
                        {LibrarySetup.DATABASE_COLUMN_TAGS_TAGPARENT}
                    From tags t
                    Where t.id in ({SqlUtils.GetMultipleSqlValues(ids, "'")})
                "; // TODO TW: binding?

                data = UnifiedDatabaseHelperClass.GetResultsDataTable(sql, con);

                con.Close();
            }

            if (data == null || data.Rows.Count != ids.Length)
            {
                throw new InvalidDataFromDatabase();
            }

            var initializedTags = new List<Tag>();

            for (var i = 0; i < ids.Length; i++)
            {
                var row = data.Rows[i];

                var parent = row[LibrarySetup.DATABASE_COLUMN_TAGS_TAGPARENT];

                var curTag = new Tag
                {
                    Id = row[CLMN_TAGID].ToInt32(GeneralHelperClass.ToInt32ConversionTypes.IntOrException),
                    Header = row[CLMN_TAGTEXT].ToString(),
                    Valid = row[CLMN_VALIDITYSTATUS].ToString() == "1",
                    ParentTagID =(parent == null || parent is DBNull) ? (int?)null : parent.ToInt32(GeneralHelperClass.ToInt32ConversionTypes.IntOrException), 
                };

                initializedTags.Add(curTag);
            }

            return initializedTags.ToArray();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Tag tag))
            {
                return false;
            }
            return tag.Id == Id;
        }
    }
}
