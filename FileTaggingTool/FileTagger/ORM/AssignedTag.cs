using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Demos.DemosHelpers;
using Demos.FileTagger.Utils;

namespace Demos.FileTagger.ORM
{
    public enum TagAssignmentType: ushort
    {
        PrimaryTag = 1,
        SecondaryTag = 2,
    }
    
    public class AssignedTag
    {
        private const string CLMN_PK_ID = "id";
        private const string CLMN_FK_TAG_ID = "tid";
        private const string CLMN_FK_FILE_ID = "fid";
        private const string CLMN_ASSIGNMENT_TYPE = "at";
        public Tag Tag { get; protected set; }
        
        public File File { get; protected set; }
        public TagAssignmentType TagAssignmentType { get; protected set; }

        private AssignedTag(File file, Tag tag, TagAssignmentType tagAssignmentType)
        {
            Tag = tag;
            File = file;
            TagAssignmentType = tagAssignmentType;
        }

        public static IEnumerable<AssignedTag> Initialize(IEnumerable<int> file2TagsId)
        {
            DataTable data = null;

            var ids = file2TagsId.ToArray();
            using (var con = Manager.OpenConnection(Manager.Instance.OpenedLibraryDatabaseFilePath))
            {
                var sql = $@"
                    Select
                        {LibrarySetup.DATABASE_COLUMN_COMMON_ID} {CLMN_PK_ID},
                        tid {CLMN_FK_TAG_ID},
                        fid {CLMN_FK_FILE_ID},
                        {LibrarySetup.DATABASE_COLUMN_FILE2TAGS_ASSIGNMENT_TYPE} {CLMN_ASSIGNMENT_TYPE}
                    From files2tags f2t
                    Where f2t.id in ({SqlUtils.GetMultipleSqlValues(ids, "'")})
                "; // TODO TW: binding?

                data = UnifiedDatabaseHelperClass.GetResultsDataTable(sql, con);

                con.Close();
            }

            if (data == null || data.Rows.Count != ids.Count())
            {
                throw new InvalidDataFromDatabase();
            }

            var auxData = Enumerable.Range(0, data.Rows.Count)
                .Select(row => data.Rows[row])
                .Select(row =>
                    (
                        row[CLMN_PK_ID].ToInt32(GeneralHelperClass.ToInt32ConversionTypes.IntOrException),
                        row[CLMN_FK_TAG_ID].ToInt32(GeneralHelperClass.ToInt32ConversionTypes.IntOrException),
                        row[CLMN_FK_FILE_ID].ToInt32(GeneralHelperClass.ToInt32ConversionTypes.IntOrException),
                        (TagAssignmentType) Enum.Parse(typeof(TagAssignmentType), row[CLMN_ASSIGNMENT_TYPE].ToString())
                    )
                )
                .ToArray();
                
            var tags = Tag.Initialize(auxData.Select(d => d.Item2).Distinct().ToArray()).ToDictionary(x => x.Id, x => x);
            var files = File.Initialize(auxData.Select(d => d.Item3).Distinct().ToArray()).ToDictionary(x => x.Id, x => x);

            return auxData.Select(x => new AssignedTag(files[x.Item3], tags[x.Item2], x.Item4));
        }
    }
}