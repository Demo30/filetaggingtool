﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SQLite;
using Demos.DemosHelpers;
using Demos.FileTagger.Utils;

namespace Demos.FileTagger.ORM
{
    public class File
    {
        public int Id { get; set; }
        public string Identifier { get; set; }
        public string UserFilename { get; set; }
        public string OriginalFileName { get; set; }
        public string OriginalFilePath { get; set; }
        public string CurrentFilePath { get; set; }
        public string FileExtension { get; set; }
        public bool Valid { get; set; }
        private const string CLMN_FILEID = "fileID";
        private const string CLMN_IDENTIFIER = "identifier";
        private const string CLMN_ORIGINALFILENAME = "originalFileName";
        private const string CLMN_ORIGINALFILEPATH = "originalFilePath";
        private const string CLMN_FILEEXTENSION = "fileExtension";
        private const string CLM_USERFILENAME = LibrarySetup.DATABASE_COLUMN_FILES_USERFILENAME;
        private const string CLM_CURRENTFILEPATH = LibrarySetup.DATABASE_COLUMN_FILES_CURRENTPATH;

        public File() { }

        public static File Initialize(int id)
        {
            File[] files = File.Initialize(new int[] { id });
            return files[0];
        }

        public static File[] Initialize(int[] ids)
        {
            DataTable data = null;

            using (SQLiteConnection con = Manager.OpenConnection(Manager.Instance.OpenedLibraryDatabaseFilePath))
            {
                string sql = String.Format(@"
                    Select
                        id {1},
                        identifier {2},
                        originalFileName {3},
                        originalFilePath {4},
                        fileExtension {5},
                        {6},
                        {7},
                        {8}
                    From files f
                    Where f.id in ({0})
                ",
                SqlUtils.GetMultipleSqlValues(ids, "'"),
                CLMN_FILEID, CLMN_IDENTIFIER, CLMN_ORIGINALFILENAME, CLMN_ORIGINALFILEPATH, CLMN_FILEEXTENSION, Manager.DATABASE_COLUMN_COMMON_VALIDITYSTATUS, CLM_USERFILENAME, CLM_CURRENTFILEPATH);

                data = UnifiedDatabaseHelperClass.GetResultsDataTable(sql, con);

                con.Close();
            }

            if (data == null || data.Rows.Count != ids.Length)
            {
                throw new InvalidDataFromDatabase();
            }

            var initializedFiles = new List<File>();

            for (var i = 0; i < ids.Length; i++)
            {
                var row = data.Rows[i];

                var curFile = new File
                {
                    Id = row[CLMN_FILEID].ToInt32(GeneralHelperClass.ToInt32ConversionTypes.IntOrException),
                    Identifier = row[CLMN_IDENTIFIER].ToString(),
                    UserFilename = row[CLM_USERFILENAME].ToString(),
                    OriginalFileName = row[CLMN_ORIGINALFILENAME].ToString(),
                    OriginalFilePath = row[CLMN_ORIGINALFILEPATH].ToString(),
                    CurrentFilePath = row[CLM_CURRENTFILEPATH].ToString(),
                    FileExtension = row[CLMN_FILEEXTENSION].ToString(),
                    Valid = row[Manager.DATABASE_COLUMN_COMMON_VALIDITYSTATUS].ToString() == "1"
                };

                initializedFiles.Add(curFile);
            }

            return initializedFiles.ToArray();
        }

        public string GetFileName(bool withIdentifier, bool withExtension)
        {
            var fileName = string.IsNullOrEmpty(UserFilename) ? OriginalFileName : UserFilename;
            fileName = withIdentifier && !string.IsNullOrEmpty(Identifier) ?
                Manager.Instance.IdentifierManager.IdentifierStrategy.ConcatIdentifierWithFileName(Identifier, fileName) :
                fileName;
            fileName = withExtension && !string.IsNullOrEmpty(FileExtension) ?
                string.Concat(fileName, FileExtension) :
                fileName;
            return fileName;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is File file))
            {
                return false;
            }
            return file.Id == Id;
        }

        public string GetFullFinalFilepath(string pathToLibraryUserData)
        {
            if (string.IsNullOrEmpty(pathToLibraryUserData)) { throw new ArgumentNullException(); }

            if (pathToLibraryUserData.Last() != '\\')
            {
                pathToLibraryUserData += '\\';
            }

            var final = string.Concat
            (
                pathToLibraryUserData,
                CleanPath(this.CurrentFilePath),
                '\\',
                GetFileName(true, true)
            );

            return final;
        }

        public static string CleanPath(string path)
        {
            if (path == null)
            {
                return String.Empty;
            }

            string alteredPath = path.Replace("//", "\\");
            alteredPath = alteredPath.TrimStart('\\');
            alteredPath = alteredPath.TrimEnd('\\');

            return alteredPath;
        }
    }
}
