﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Demos.FileTagger
{
    public class FileFullNameSelector : FileSelector
    {
        public string[] FullFileNames { get; private set; }

        public FileFullNameSelector(string[] fullFileNames)
        {
            if (fullFileNames != null)
            {
                this.FullFileNames = fullFileNames;
            }
            else
            {
                throw new ArgumentNullException();
            }
        }

        public override FileInfo[] GetFiles()
        {
            List<FileInfo> fis = new List<FileInfo>();
            foreach(string fn in this.FullFileNames)
            {
                fis.Add(new FileInfo(fn));
            }
            return fis.ToArray();
        }

    }
}
