﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Demos.FileTagger
{
    public abstract class FileSelector
    {
        public abstract FileInfo[] GetFiles();
    }
}
