﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Demos.FileTagger
{
    /// <summary>
    /// Retrieves files by identifiers from actual file paths in directory path
    /// </summary>
    public class FileIdentifierDirectorySelector : FileSelector
    {
        public string[] Identifiers { get; private set; }

        public FileIdentifierDirectorySelector(string[] Identifiers)
        {
            this.Identifiers = Identifiers;
        }

        public override FileInfo[] GetFiles()
        {
            throw new NotImplementedException();
        }

    }
}
