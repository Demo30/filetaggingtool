﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Demos.FileTagger
{
    public class FileDirectorySelector : FileSelector
    {
        public DirectoryInfo RootDirectory { get; private set; }

        public FileDirectorySelector(DirectoryInfo directory)
        {
            if (directory != null)
            {
                this.RootDirectory = directory;
            }
            else
            {
                throw new ArgumentNullException();
            }
        }

        public override FileInfo[] GetFiles()
        {
            if (this.RootDirectory.Exists)
            {
                List<FileInfo> files = new List<FileInfo>();
                string[] filePaths = Directory.GetFiles(this.RootDirectory.FullName, "*.*", SearchOption.AllDirectories);
                foreach (string filePath in filePaths)
                {
                    files.Add(new FileInfo(filePath));
                }
                return files.ToArray();
            }
            else
            {
                throw new Demos.FileTagger.Exceptions.InvalidFolderPath(this.RootDirectory.FullName);
            }
        }

    }
}
