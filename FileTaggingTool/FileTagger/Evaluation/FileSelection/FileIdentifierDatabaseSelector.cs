﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Demos.FileTagger
{
    /// <summary>
    /// Retrieves files by identifiers based on data from database
    /// </summary>
    public class FileIdentifierDatabaseSelector : FileSelector
    {
        public string[] Identifiers { get; private set; }

        public FileIdentifierDatabaseSelector(string[] Identifiers)
        {
            this.Identifiers = Identifiers;
        }

        public override FileInfo[] GetFiles()
        {
            List<FileInfo> fileInfos = new List<FileInfo>();
            Demos.FileTagger.Manager manager = Demos.FileTagger.Manager.Instance;
            foreach(string curIdentifier in this.Identifiers)
            {
                ORM.File curFile = manager.GetFileByIdentifier(curIdentifier, false);
                fileInfos.Add(new FileInfo(curFile.GetFullFinalFilepath(manager.OpenedLibraryUserDataDirPath)));
            }
            return fileInfos.ToArray();
        }

    }
}
