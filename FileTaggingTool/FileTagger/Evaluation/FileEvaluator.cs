﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Demos.FileTagger
{
    internal class FileEvaluator
    {
        public Manager Manager
        {
            get { return Demos.FileTagger.Manager.Instance; }
        }
        public IdentifierManager IdentifierManager
        {
            get { return  Manager.Instance.IdentifierManager; }
        }
        private ORM.File[] AllFilesInDb { get; set; }

        public FileEvaluator()
        {
            this.AllFilesInDb = this.Manager.GetFiles(false);
        }

        public OriginalFileNewInfoPairing[] EvaluateFiles(FileEvaluationSettings additionalSettings)
        {
            ORM.File[] allFilesInDb = this.AllFilesInDb;
            List<string> existingFileIdentifiers = new List<string>(this.Manager.GetFileIdentifierRecords(false));
            FileInfo[] selectedFiles = additionalSettings.FileSelector.GetFiles();
            List<OriginalFileNewInfoPairing> oldAndNewFilenames = new List<OriginalFileNewInfoPairing>();

            foreach(FileInfo currentOldFile in selectedFiles)
            {
                string currentFileIdentifier = this.IdentifierManager.IdentifierStrategy.ExtractIdentifierFromFileInfo(currentOldFile, false);

                string identifier = null;
                bool? identifierValid = null;
                bool identifierKnown = false;
                string fileNameToChange = null;

                if (!existingFileIdentifiers.Contains(currentFileIdentifier))
                {
                    identifierKnown = false;
                    identifierValid = null;
                    identifier = this.IdentifierManager.IdentifierStrategy.GetNewFileIdentifier(existingFileIdentifiers.ToArray());

                    string nameWithoutExtension = FileRenamer.ExtractFileName(currentOldFile, false, false, this.IdentifierManager.IdentifierStrategy);
                    fileNameToChange = this.IdentifierManager.IdentifierStrategy.ConcatIdentifierWithFileName(identifier, nameWithoutExtension);

                    existingFileIdentifiers.Add(identifier);
                }
                else
                {
                    ORM.File presentFile = this.Manager.GetFileByIdentifier(currentFileIdentifier, allFilesInDb, false);

                    if (presentFile == null) { throw new Exception("File could not be found. Critical failure."); }

                    identifierKnown = true;
                    identifierValid = presentFile.Valid;
                    fileNameToChange = this.IdentifierManager.IdentifierStrategy.ConcatIdentifierWithFileName(presentFile.Identifier, presentFile.OriginalFileName);

                    identifier = presentFile.Identifier;
                }

                OriginalFileNewInfoPairing pairing = new OriginalFileNewInfoPairing(this.IdentifierManager.IdentifierStrategy)
                {
                    OriginalFile = currentOldFile,
                    NewIdentifier = identifier,
                    IdentifierKnown = identifierKnown,
                    IdentifierValid = identifierValid,
                    NewFileName = fileNameToChange
                };
                oldAndNewFilenames.Add(pairing);
            }

            return oldAndNewFilenames.ToArray();
        }

        public ORM.File[] ReevaluateFileRecordState(FileEvaluationSettings settings)
        {
            List<ORM.File> results = new List<ORM.File>();

            FileInfo[] selectedFiles = settings.FileSelector.GetFiles();
            Dictionary<string, FileInfo> filesInLibraryByIdentifier = new Dictionary<string, FileInfo>(); // attempt to speed up the search
            foreach(FileInfo fi in selectedFiles)
            {
                string identifier = this.IdentifierManager.IdentifierStrategy.ExtractIdentifierFromFileInfo(fi, false);
                if (!String.IsNullOrEmpty(identifier) && !filesInLibraryByIdentifier.ContainsKey(identifier))
                {
                    filesInLibraryByIdentifier[identifier] = fi;
                }
            }

            Func<ORM.File, bool> isOnDirPath = (dbFile) =>
            {
                if (settings.FileSelector is FileDirectorySelector dirSelector)
                {
                    if (dbFile.GetFullFinalFilepath(Manager.OpenedLibraryUserDataDirPath).IndexOf(dirSelector.RootDirectory.FullName) >= 0)
                    {
                        return true;
                    }
                }
                return false;
            };

            // the CORE = gathering evaluated data, the actual changes are taking place in a different method
            //      iterating through files in the database (SHOULD RECEIVE ALL RECORDS, NOT ONLY THE VALID ONES!!)
            //      invalidating those that are no longer present among actual files (identified by derived identifier!)
            //      re-validating those that are present (identified by derived identifier!), but set as invalid

            foreach(ORM.File curDbFileRecord in this.AllFilesInDb)
            {
                FileInfo actualFileMatchingDbRecord = this.GetMatchingFileInfoByIdentifier(filesInLibraryByIdentifier, curDbFileRecord.Identifier);

                bool dbFileRecordIsOnEvaluatedDirPath = isOnDirPath(curDbFileRecord);

                bool invalidateRecord =
                    (curDbFileRecord.Valid) && // nemá cenu znevalidňovat, co není validní
                    (
                        // Chci znevalidňovat pouze záznamy, pro které se nenašly skutečné soubory.
                        // Při omezování cesty je však možné zvažovat pouze záznamy, které se na této cestě měly nacházet!
                        (dbFileRecordIsOnEvaluatedDirPath) &&
                        (actualFileMatchingDbRecord == null)
                    );

                if (invalidateRecord)
                {
                    ORM.File updateFile = new ORM.File()
                    {
                        Id = curDbFileRecord.Id,
                        Identifier = curDbFileRecord.Identifier,
                        Valid = false,
                        CurrentFilePath = "",
                        UserFilename = curDbFileRecord.UserFilename
                    };

                    results.Add(updateFile);
                }
                else
                {
                    if (actualFileMatchingDbRecord != null)
                    {
                        string dirPath = actualFileMatchingDbRecord.Directory.FullName.Replace(Manager.Instance.OpenedLibraryUserDataDirPath, "");
                        string fileName = FileRenamer.ExtractFileName(actualFileMatchingDbRecord, false, false, this.IdentifierManager.IdentifierStrategy);

                        ORM.File updateFile = new ORM.File()
                        {
                            Id = curDbFileRecord.Id,
                            Identifier = curDbFileRecord.Identifier,
                            Valid = true,
                            CurrentFilePath = dirPath,
                            UserFilename = fileName,
                            FileExtension = FileRenamer.ExtractFileExtension(actualFileMatchingDbRecord, this.IdentifierManager.IdentifierStrategy)
                        };

                        results.Add(updateFile);
                    }
                }
            }

            return results.ToArray();
        }

        #region Supportive methods

        private FileInfo GetMatchingFileInfoByIdentifier(Dictionary<string, FileInfo> filesInLibraryByIdentifiers, string identifier)
        {
            FileInfo actualFileMatchingDbRecord = null;
            if (filesInLibraryByIdentifiers.ContainsKey(identifier))
            {
                actualFileMatchingDbRecord = filesInLibraryByIdentifiers[identifier];
            }

            return actualFileMatchingDbRecord;
        }

        #endregion

    }
}
